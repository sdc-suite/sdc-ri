/**
 * Any utilities that cannot sensibly be dropped into other packages.
 */
@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.common.util;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
