package org.somda.sdc.common.util

import org.jvnet.jaxb.lang.CopyTo

/**
 * Copies an object using the [CopyTo] interface, but returns the same type as the original object.
 */
fun <T : CopyTo> T.copyTyped(): T {
    @Suppress("UNCHECKED_CAST")
    return this.copyTo(null) as T
}
