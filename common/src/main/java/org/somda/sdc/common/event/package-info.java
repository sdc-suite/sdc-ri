/**
 * Base classes for events used in conjunction with {@linkplain com.google.common.eventbus.EventBus}.
 */
@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.common.event;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
