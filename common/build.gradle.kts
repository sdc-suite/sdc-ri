plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.lint-checkstyle")
    id("buildlogic.kotlin-conventions")
    id("buildlogic.deploy")
}

dependencies {
    api(libs.org.jvnet.jaxb.jaxb.plugins.runtime)
    api(libs.org.somda.sdc.common.model)
    testImplementation(libs.org.apache.logging.log4j.log4j.core.test)
    testImplementation(projects.test)
}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming with" +
    " the IEEE 11073 SDC specifications. This project implements common functionality used for SDCri."

checkstyle {
    // skip tests
    sourceSets = setOf(project.sourceSets.main.get())
}