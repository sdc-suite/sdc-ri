package org.somda.sdc.biceps.provider.preprocessing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.CommonConfig
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibTypeValidator
import org.somda.sdc.biceps.common.storage.MdibStorage
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.model.participant.MdibVersion
import org.somda.sdc.biceps.model.participant.VmdDescriptor
import org.somda.sdc.biceps.model.participant.VmdState
import org.somda.sdc.biceps.testutil.BaseTreeModificationsSet
import org.somda.sdc.biceps.testutil.Handles
import org.somda.sdc.biceps.testutil.MockEntryFactory
import org.somda.sdc.biceps.testutil.MockModelFactory
import test.org.somda.common.LoggingTestWatcher
import java.math.BigInteger

@ExtendWith(LoggingTestWatcher::class)
class DuplicateDescriptorCheckerTest {
    private val unitTestUtil = UnitTestUtil(object : DefaultBicepsConfigModule() {
        override fun customConfigure() {
            // Configure to avoid copying and make comparison easier
            bind(
                CommonConfig.COPY_MDIB_OUTPUT,
                Boolean::class.java,
                false
            )
        }
    })

    private lateinit var mdibStorage: MdibStorage
    private lateinit var duplicateDescriptorChecker: DuplicateDescriptorChecker

    private val mdibTypeValidator = unitTestUtil.injector.getInstance(MdibTypeValidator::class.java)
    private val mockEntryFactory = MockEntryFactory(mdibTypeValidator)

    @BeforeEach
    fun beforeEach() {
        // Given a version handler and sample input
        mdibStorage = unitTestUtil.injector.getInstance(MdibStorageFactory::class.java)
            .createMdibStorage()
        duplicateDescriptorChecker = unitTestUtil.injector.getInstance(DuplicateDescriptorChecker::class.java)

        mdibStorage.apply(
            MdibVersion.create(),
            BigInteger.ZERO,
            BigInteger.ZERO,
            BaseTreeModificationsSet(mockEntryFactory).createBaseTree()
        )
    }

    private fun apply(modifications: MdibDescriptionModifications) {
        duplicateDescriptorChecker.process(modifications.asList().toMutableList(), mdibStorage)
    }

    @Test
    fun `throw exception if handle is already present in a descriptor the mdib`() {
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Insert(
                    MockModelFactory.createPair(
                        Handles.MDS_0,
                        VmdDescriptor::class.java,
                        VmdState::class.java
                    ), Handles.MDS_1
                )
            )

        val thrown = Assertions.assertThrows(HandleDuplicatedException::class.java) { apply(modifications) }
        Assertions.assertTrue(thrown.message?.contains(Handles.MDS_0) ?: false)
    }

    @Test
    fun `throw exception if handle is already present in a context state the mdib`() {
        val collisionHandle = Handles.CONTEXT_0
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Insert(
                    MockModelFactory.createPair(
                        collisionHandle,
                        VmdDescriptor::class.java,
                        VmdState::class.java
                    ), Handles.MDS_1
                )
            )

        val thrown = Assertions.assertThrows(HandleDuplicatedException::class.java) { apply(modifications) }
        Assertions.assertTrue(
            thrown.message?.contains(collisionHandle) ?: false,
            "Unexpected message: ${thrown.message}"
        )
    }

    @Test
    fun `handle not present in the mdib passes checker`() {
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Insert(
                    MockModelFactory.createPair(
                        "MySickCustomHandle",
                        VmdDescriptor::class.java,
                        VmdState::class.java
                    ), Handles.MDS_1
                )
            )

        Assertions.assertDoesNotThrow { apply(modifications) }
    }
}
