package org.somda.sdc.biceps.provider.preprocessing;

import org.junit.jupiter.api.extension.ExtendWith;
import org.somda.sdc.biceps.UnitTestUtil;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.storage.MdibStorage;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelState;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MdsState;
import org.somda.sdc.biceps.model.participant.ScoDescriptor;
import org.somda.sdc.biceps.model.participant.ScoState;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;
import org.somda.sdc.biceps.model.participant.VmdState;
import org.somda.sdc.biceps.testutil.Handles;
import org.somda.sdc.biceps.testutil.MockModelFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.org.somda.common.LoggingTestWatcher;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(LoggingTestWatcher.class)
class CardinalityCheckerTest {
    private static final UnitTestUtil UT = new UnitTestUtil();

    private CardinalityChecker cardinalityChecker;
    private MdibStorage mdibStorage;

    @BeforeEach
    void beforeEach() {
        // Given a cardinality checker
        cardinalityChecker = UT.getInjector().getInstance(CardinalityChecker.class);
        mdibStorage = mock(MdibStorage.class);
    }

    @Test
    void noInsertions() throws Exception {
        // When there are no insertions in a modification set
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                .add(new MdibDescriptionModification.Update(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.MDS_0, MdsDescriptor.class), MockModelFactory.createState(Handles.MDS_0, MdsState.class))))
                .add(new MdibDescriptionModification.Update(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.VMD_0, VmdDescriptor.class), MockModelFactory.createState(Handles.VMD_0, VmdState.class))))
                .add(new MdibDescriptionModification.Update(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.METRIC_0, ChannelDescriptor.class), MockModelFactory.createState(Handles.METRIC_0, ChannelState.class))));

        // Then expect the cardinality checker to pass
        assertDoesNotThrow(() -> apply(modifications));
    }

    @Test
    void mdsInsertion() throws Exception {
        // When there is an MDS being inserted
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.MDS_0, MdsDescriptor.class), MockModelFactory.createState(Handles.MDS_0, MdsState.class)), null));

        // Then expect the cardinality checker to pass
        assertDoesNotThrow(() -> apply(modifications));
    }

    @Test
    void sameTypeInModificationsIsOk() throws Exception {
        when(mdibStorage.getEntity(Handles.MDS_0)).thenReturn(Optional.empty());

        {
            // When there are two VMDs to be inserted below an MDS
            final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.VMD_0, VmdDescriptor.class), MockModelFactory.createState(Handles.VMD_0, VmdState.class)), Handles.MDS_0))
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.VMD_1, VmdDescriptor.class), MockModelFactory.createState(Handles.VMD_1, VmdState.class)), Handles.MDS_0));

            // Then expect the cardinality checker to pass
            assertDoesNotThrow(() -> apply(modifications));
        }

        {
            // When there are two SCOs to be inserted below two MDSs
            final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_0, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_0, ScoState.class)), Handles.MDS_0))
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_1, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_1, ScoState.class)), Handles.MDS_1));

            // Then expect the cardinality checker to pass
            assertDoesNotThrow(() -> apply(modifications));
        }
    }

    @Test
    void sameTypeInModificationsIsBad() throws Exception {
        when(mdibStorage.getEntity(Handles.MDS_0)).thenReturn(Optional.empty());

        // When there are two SCOs to be inserted below an MDS
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_0, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_0, ScoState.class)), Handles.MDS_0))
                .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_1, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_1, ScoState.class)), Handles.MDS_0));

        // Then expect the cardinality checker to throw an exception
        assertThrows(CardinalityException.class, () -> apply(modifications));
    }

    @Test
    void insertWithExistingChildInMdibStorageIsOk() throws Exception {
        var mockMdsEntity = mock(MdibEntity.class);
        var mockMdsDescriptor = mock(MdsDescriptor.class);
        when(mockMdsEntity.getDescriptor()).thenReturn(mockMdsDescriptor);

        when(mdibStorage.getEntity(Handles.MDS_0)).thenReturn(Optional.of(mockMdsEntity));

        var mockVmdEntity = mock(MdibEntity.class);
        var mockVmdDescriptor = mock(VmdDescriptor.class);
        when(mockVmdEntity.getDescriptor()).thenReturn(mockVmdDescriptor);
        when(mockVmdEntity.getParent()).thenReturn(Optional.of(Handles.MDS_0));

        {
            when(mdibStorage.getChildrenByType(Handles.MDS_0, VmdDescriptor.class))
                    .thenReturn(Arrays.asList(mockVmdEntity));

            // When there are two VMDs to be inserted below an MDS that exists in the MdibStorage
            // and possesses one VMD already
            final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.VMD_0, VmdDescriptor.class), MockModelFactory.createState(Handles.VMD_0, VmdState.class)), Handles.MDS_0))
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.VMD_1, VmdDescriptor.class), MockModelFactory.createState(Handles.VMD_1, VmdState.class)), Handles.MDS_0));

            // Then expect the cardinality checker to pass
            assertDoesNotThrow(() -> apply(modifications));
        }

        {
            when(mdibStorage.getChildrenByType(Handles.MDS_0, VmdDescriptor.class))
                    .thenReturn(Collections.emptyList());

            // When there is one SCO to be inserted below an MDS that exists in the MdibStorage
            // and does not possess an SCO already
            final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                    .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_0, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_0, ScoState.class)), Handles.MDS_0));

            // Then expect the cardinality checker to pass
            assertDoesNotThrow(() -> apply(modifications));
        }
    }

    @Test
    void insertWithExistingChildInMdibStorageIsBad() throws Exception {
        var mockMdsEntity = mock(MdibEntity.class);
        var mockMdsDescriptor = mock(MdsDescriptor.class);
        when(mockMdsEntity.getDescriptor()).thenReturn(mockMdsDescriptor);

        when(mdibStorage.getEntity(Handles.MDS_0)).thenReturn(Optional.of(mockMdsEntity));

        var mockScoEntity = mock(MdibEntity.class);
        var mockScoDescriptor = mock(ScoDescriptor.class);
        when(mockScoEntity.getDescriptor()).thenReturn(mockScoDescriptor);

        when(mdibStorage.getChildrenByType(Handles.MDS_0, ScoDescriptor.class))
                .thenReturn(Arrays.asList(mockScoEntity));

        // When there is one SCO to be inserted below an MDS that exists in the MdibStorage
        // and possesses an SCO already
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications()
                .add(new MdibDescriptionModification.Insert(Pair.tryFromThrowing(MockModelFactory.createDescriptor(Handles.SCO_0, ScoDescriptor.class), MockModelFactory.createState(Handles.SCO_0, ScoState.class)), Handles.MDS_0));

        // Then expect the cardinality checker to throw an exception
        assertThrows(CardinalityException.class, () -> apply(modifications));
    }

    private void apply(MdibDescriptionModifications modifications) throws CardinalityException {
        cardinalityChecker.process(modifications.asList(), mdibStorage);
    }
}