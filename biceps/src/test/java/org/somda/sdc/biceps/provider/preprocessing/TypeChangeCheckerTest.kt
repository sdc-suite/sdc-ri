package org.somda.sdc.biceps.provider.preprocessing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.MdibStorage
import org.somda.sdc.biceps.model.participant.LocationContextDescriptor
import org.somda.sdc.biceps.model.participant.LocationContextState
import org.somda.sdc.biceps.model.participant.MdsDescriptor
import org.somda.sdc.biceps.model.participant.MdsState
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor
import org.somda.sdc.biceps.model.participant.PatientContextState
import org.somda.sdc.biceps.model.participant.VmdDescriptor
import org.somda.sdc.biceps.model.participant.VmdState
import org.somda.sdc.biceps.testutil.Handles
import org.somda.sdc.biceps.testutil.MockModelFactory
import test.org.somda.common.LoggingTestWatcher
import java.math.BigInteger

@ExtendWith(LoggingTestWatcher::class)
class TypeChangeCheckerTest {

    private lateinit var typeChangeChecker: TypeChangeChecker
    private lateinit var mdibStorage: MdibStorage

    @BeforeEach
    fun beforeEach() {
        // Given a type change checker
        typeChangeChecker =
            UT.injector.getInstance(TypeChangeChecker::class.java)
        mdibStorage = Mockito.mock(MdibStorage::class.java)
    }

    private fun apply(modifications: MdibDescriptionModifications) {
        typeChangeChecker.process(modifications.asList().toMutableList(), mdibStorage)
    }

    private fun apply(modifications: MdibStateModifications) {
        typeChangeChecker.process(modifications, mdibStorage)
    }

    @Test
    @Throws(Exception::class)
    fun `try to insert vmd with already present mds handle`() {
        val mdsHandle = "mdsHandle"

        // create mds type
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createPair(
                            mdsHandle,
                            MdsDescriptor::class.java,
                            MdsState::class.java
                        ), null
                    )
                )

            // Then expect the type change checker to pass, even upon second insertion
            Assertions.assertDoesNotThrow { apply(modifications) }
            Assertions.assertDoesNotThrow { apply(modifications) }
        }

        // create vmd type with same handle
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createPair(
                            mdsHandle,
                            VmdDescriptor::class.java,
                            VmdState::class.java
                        ), mdsHandle
                    )
                )
            val thrown = Assertions.assertThrows(TypeConsistencyException::class.java) { apply(modifications) }

            Assertions.assertTrue(thrown.message?.contains(mdsHandle) ?: false)
        }
    }

    @Test
    @Throws(Exception::class)
    fun `update handle used for mds with vmd descriptor`() {
        val mdsHandle = "mdsHandle"

        // create mds type
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createPair(
                            mdsHandle,
                            MdsDescriptor::class.java,
                            MdsState::class.java
                        ), null
                    )
                )

            // Then expect the type change checker to pass, even upon second insertion
            Assertions.assertDoesNotThrow { apply(modifications) }
            Assertions.assertDoesNotThrow { apply(modifications) }
        }

        // create update mds to vmd with same handle
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Update(
                        MockModelFactory.createPair(
                            mdsHandle,
                            VmdDescriptor::class.java,
                            VmdState::class.java
                        )
                    )
                )
            val thrown = Assertions.assertThrows(TypeConsistencyException::class.java) { apply(modifications) }

            Assertions.assertTrue(thrown.message?.contains(mdsHandle) ?: false)
        }
    }

    @Test
    fun `update context state from patient to location using description modification`() {
        val stateHandle = Handles.CONTEXT_0

        // create patient context type
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createContextStatePair(
                            Handles.CONTEXTDESCRIPTOR_0,
                            BigInteger.ZERO,
                            PatientContextDescriptor::class.java,
                            PatientContextState::class.java,
                            stateHandle,
                        ), null
                    )
                )

            // Then expect the type change checker to pass, even upon second insertion
            Assertions.assertDoesNotThrow { apply(modifications) }
            Assertions.assertDoesNotThrow { apply(modifications) }
        }

        // use handle for location context state
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createContextStatePair(
                            Handles.CONTEXTDESCRIPTOR_1,
                            BigInteger.ZERO,
                            LocationContextDescriptor::class.java,
                            LocationContextState::class.java,
                            stateHandle,
                        ), null
                    )
                )

            val thrown = Assertions.assertThrows(TypeConsistencyException::class.java) { apply(modifications) }
            Assertions.assertTrue(thrown.message?.contains(stateHandle) ?: false)
        }
    }

    @Test
    fun `update context state from patient to location using state change`() {
        val stateHandle = Handles.CONTEXT_0

        // create patient context type
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createContextStatePair(
                            Handles.CONTEXTDESCRIPTOR_0,
                            BigInteger.ZERO,
                            PatientContextDescriptor::class.java,
                            PatientContextState::class.java,
                            stateHandle,
                        ), null
                    )
                )

            // Then expect the type change checker to pass, even upon second insertion
            Assertions.assertDoesNotThrow { apply(modifications) }
            Assertions.assertDoesNotThrow { apply(modifications) }
        }

        // use handle for location context state
        run {
            val modifications = MdibStateModifications.Context(
                listOf(
                    MockModelFactory.createContextState(
                        stateHandle, Handles.CONTEXTDESCRIPTOR_1,
                        LocationContextState::class.java
                    )
                )
            )

            val thrown = Assertions.assertThrows(TypeConsistencyException::class.java) { apply(modifications) }
            Assertions.assertTrue(thrown.message?.contains(stateHandle) ?: false)
        }
    }

    @Test
    fun `new descriptor collides with present context state`() {
        val stateHandle = Handles.CONTEXT_0

        // create patient context type
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createContextStatePair(
                            Handles.CONTEXTDESCRIPTOR_0,
                            BigInteger.ZERO,
                            PatientContextDescriptor::class.java,
                            PatientContextState::class.java,
                            stateHandle,
                        ), null
                    )
                )

            // Then expect the type change checker to pass, even upon second insertion
            Assertions.assertDoesNotThrow { apply(modifications) }
            Assertions.assertDoesNotThrow { apply(modifications) }
        }

        // use handle for location context state
        run {
            val modifications = MdibDescriptionModifications()
                .add(
                    MdibDescriptionModification.Insert(
                        MockModelFactory.createPair(
                            stateHandle,
                            BigInteger.ZERO,
                            VmdDescriptor::class.java,
                            VmdState::class.java,
                        ), Handles.MDS_0
                    )
                )

            val thrown = Assertions.assertThrows(TypeConsistencyException::class.java) { apply(modifications) }
            Assertions.assertTrue(thrown.message?.contains(stateHandle) ?: false)
        }
    }


    companion object {
        private val UT = UnitTestUtil()
    }
}
