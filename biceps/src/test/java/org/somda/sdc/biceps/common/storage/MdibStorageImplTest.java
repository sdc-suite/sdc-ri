package org.somda.sdc.biceps.common.storage;

import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.somda.sdc.biceps.UnitTestUtil;
import org.somda.sdc.biceps.common.CommonConfig;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.PairException;
import org.somda.sdc.biceps.common.WrittenMdibStateModifications;
import org.somda.sdc.biceps.common.preprocessing.DescriptorChildRemover;
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory;
import org.somda.sdc.biceps.consumer.preprocessing.DuplicateContextStateHandleHandler;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AlertConditionDescriptor;
import org.somda.sdc.biceps.model.participant.AlertConditionState;
import org.somda.sdc.biceps.model.participant.AlertSystemDescriptor;
import org.somda.sdc.biceps.model.participant.AlertSystemState;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelState;
import org.somda.sdc.biceps.model.participant.ContextAssociation;
import org.somda.sdc.biceps.model.participant.EnsembleContextDescriptor;
import org.somda.sdc.biceps.model.participant.EnsembleContextState;
import org.somda.sdc.biceps.model.participant.LocationContextDescriptor;
import org.somda.sdc.biceps.model.participant.LocationContextState;
import org.somda.sdc.biceps.model.participant.MdibVersion;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MdsState;
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor;
import org.somda.sdc.biceps.model.participant.PatientContextState;
import org.somda.sdc.biceps.model.participant.SystemContextDescriptor;
import org.somda.sdc.biceps.model.participant.SystemContextState;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;
import org.somda.sdc.biceps.model.participant.VmdState;
import org.somda.sdc.biceps.provider.preprocessing.ContextHandleDuplicateChecker;
import org.somda.sdc.biceps.provider.preprocessing.DuplicateChecker;
import org.somda.sdc.biceps.provider.preprocessing.DuplicateDescriptorChecker;
import org.somda.sdc.biceps.provider.preprocessing.EmptyWriteChecker;
import org.somda.sdc.biceps.provider.preprocessing.HandleReferenceChecker;
import org.somda.sdc.biceps.provider.preprocessing.TypeChangeChecker;
import org.somda.sdc.biceps.provider.preprocessing.TypeConsistencyChecker;
import org.somda.sdc.biceps.provider.preprocessing.VersionHandler;
import org.somda.sdc.biceps.testutil.Handles;
import org.somda.sdc.biceps.testutil.MockModelFactory;
import org.somda.sdc.common.guice.AbstractConfigurationModule;
import test.org.somda.common.LoggingTestWatcher;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

@ExtendWith(LoggingTestWatcher.class)
class MdibStorageImplTest {
    private static final UnitTestUtil UT = new UnitTestUtil();

    private MdibStorage mdibStorage;

    @BeforeEach
    void setUp() {
        Injector injector = UT.getInjector();
        mdibStorage = injector.getInstance(MdibStorageFactory.class).createMdibStorage();
    }

    private void applyDescriptionInsertWithVersion(
        MdibStorage storage,
        BigInteger version
    ) throws PairException {
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        for (var entry: generateDescriptionWithVersion(version)) {
            modifications.insert(entry.getLeft(), entry.getRight().orElse(null));
        }
        storage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);
    }

    private void applyDescriptionUpdateWithVersion(
        MdibStorage storage,
        BigInteger version
    ) throws PairException {
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        for (var entry: generateDescriptionWithVersion(version)) {
            modifications.update(entry.getLeft());
        }
        storage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);
    }

    private void applyDescriptionDeleteWithVersion(
        MdibStorage storage,
        BigInteger version
    ) throws PairException {
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        for (var entry: generateDescriptionWithVersion(version)) {
            modifications.delete(entry.getLeft().getHandle());
        }
        storage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);
    }

    private List<Pair<org.somda.sdc.biceps.common.Pair, Optional<String>>> generateDescriptionWithVersion(BigInteger version) throws PairException {
        var modifications = new ArrayList<Pair<org.somda.sdc.biceps.common.Pair, Optional<String>>>();

        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.MDS_0, version, MdsDescriptor.class, MdsState.class),
            Optional.empty()
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.MDS_1, version, MdsDescriptor.class, MdsState.class),
            Optional.empty()
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.SYSTEMCONTEXT_0, version, SystemContextDescriptor.class, SystemContextState.class),
            Optional.of(Handles.MDS_0)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.SYSTEMCONTEXT_1, version, SystemContextDescriptor.class, SystemContextState.class),
            Optional.of(Handles.MDS_1)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.VMD_0, version, VmdDescriptor.class, VmdState.class),
            Optional.of(Handles.MDS_0)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.VMD_1, version, VmdDescriptor.class, VmdState.class),
            Optional.of(Handles.MDS_1)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.CHANNEL_0, version, ChannelDescriptor.class, ChannelState.class),
            Optional.of(Handles.VMD_0)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.CHANNEL_1, version, ChannelDescriptor.class, ChannelState.class),
            Optional.of(Handles.VMD_0)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createPair(Handles.CHANNEL_2, version, ChannelDescriptor.class, ChannelState.class),
            Optional.of(Handles.VMD_1)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createContextStatePair(
                Handles.CONTEXTDESCRIPTOR_0,
                version,
                PatientContextDescriptor.class,
                PatientContextState.class,
                Handles.CONTEXT_0, Handles.CONTEXT_1
            ),
            Optional.of(Handles.SYSTEMCONTEXT_0)
        ));
        modifications.add(new ImmutablePair<>(
            MockModelFactory.createContextStatePair(
                Handles.CONTEXTDESCRIPTOR_1,
                version,
                LocationContextDescriptor.class,
                LocationContextState.class,
                Handles.CONTEXT_2
            ),
            Optional.of(Handles.SYSTEMCONTEXT_0)
        ));

        return modifications;
    }

    private void testWithVersion(List<String> testedHandles, BigInteger version) {
        testWithVersion(testedHandles, version, version);
    }

    private void testWithVersion(List<String> testedHandles, BigInteger descrVersion, BigInteger stateVersion) {
        testedHandles.forEach(handle -> {
            assertThat(mdibStorage.getEntity(handle).isPresent(), is(true));
            assertThat(mdibStorage.getEntity(handle).orElseThrow().getDescriptor().getDescriptorVersion(), is(descrVersion));
            mdibStorage.getEntity(handle).orElseThrow().getStates().forEach(state ->
                    assertThat(state.getStateVersion(), is(stateVersion)));
        });
    }

    @Test
    void writeDescription() throws PairException {
        List<String> testedHandles = Arrays.asList(
                Handles.MDS_0,
                Handles.MDS_1,
                Handles.SYSTEMCONTEXT_0,
                Handles.SYSTEMCONTEXT_1,
                Handles.VMD_0,
                Handles.VMD_1,
                Handles.CHANNEL_0,
                Handles.CHANNEL_1,
                Handles.CHANNEL_2,
                Handles.CONTEXTDESCRIPTOR_0);

        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);
        testWithVersion(testedHandles, BigInteger.ZERO);
        applyDescriptionUpdateWithVersion(mdibStorage, BigInteger.ONE);
        testWithVersion(testedHandles, BigInteger.ONE);
        applyDescriptionUpdateWithVersion(mdibStorage, BigInteger.TEN);
        testWithVersion(testedHandles, BigInteger.TEN);

        applyDescriptionDeleteWithVersion(mdibStorage, BigInteger.ZERO);
        for (String handle : testedHandles) {
            assertThat(mdibStorage.getEntity(handle).isPresent(), is(false));
        }
    }

    @Test
    void deleteChildUpdateParent() throws PairException {
        // create content
        List<String> testedHandles = Arrays.asList(
                Handles.MDS_0,
                Handles.SYSTEMCONTEXT_0,
                Handles.VMD_0,
                Handles.CHANNEL_0,
                Handles.CHANNEL_1,
                Handles.CONTEXTDESCRIPTOR_0);

        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);
        testWithVersion(testedHandles, BigInteger.ZERO);

        final var parentHandle = Handles.VMD_0;
        final var childHandle = Handles.CHANNEL_0;
        final var untouchedChildHandle = Handles.CHANNEL_1;

        assertTrue(mdibStorage.getEntity(parentHandle).orElseThrow().getChildren().contains(childHandle));
        assertTrue(mdibStorage.getEntity(parentHandle).orElseThrow().getChildren().contains(untouchedChildHandle));
        assertEquals(parentHandle, mdibStorage.getEntity(childHandle).orElseThrow().getParent().orElseThrow());


        // delete the child
        final MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.delete(Handles.CHANNEL_0);
        var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

        assertFalse(mdibStorage.getEntity(parentHandle).orElseThrow().getChildren().contains(childHandle));
        assertTrue(mdibStorage.getEntity(parentHandle).orElseThrow().getChildren().contains(untouchedChildHandle));
        assertEquals(1, result.getDeletedEntities().size());

        // is is up to the version handler preprocessor to adjust the updated entities
        // therefore 0 is expected
        assertEquals(0, result.getUpdatedEntities().size());
    }

    @Test
    void mdibAccess() throws PairException {
        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);
        assertThat(mdibStorage.getEntity(Handles.UNKNOWN).isPresent(), is(false));
        assertThat(mdibStorage.getEntity(Handles.MDS_0).isPresent(), is(true));

        assertThat(mdibStorage.getDescriptor(Handles.UNKNOWN, MdsDescriptor.class).isPresent(), is(false));
        assertThat(mdibStorage.getDescriptor(Handles.VMD_0, MdsDescriptor.class).isPresent(), is(false));
        assertThat(mdibStorage.getDescriptor(Handles.VMD_0, VmdDescriptor.class).isPresent(), is(true));

        assertThat(mdibStorage.getContextStates(Handles.UNKNOWN, PatientContextState.class).isEmpty(), is(true));
        assertThat(mdibStorage.getContextStates(Handles.VMD_0, PatientContextState.class).isEmpty(), is(true));
        assertThat(mdibStorage.getContextStates(Handles.CONTEXTDESCRIPTOR_0, LocationContextState.class).isEmpty(), is(true));
        assertThat(mdibStorage.getContextStates(Handles.CONTEXTDESCRIPTOR_0, PatientContextState.class).size(), is(2));

        assertThat(mdibStorage.findContextStatesByType(PatientContextState.class).size(), is(2));
        assertThat(mdibStorage.findContextStatesByType(LocationContextState.class).size(), is(1));
    }

    @Test
    void writeStates() throws PairException {
        List<String> testedHandles = Arrays.asList(
                Handles.ALERTSYSTEM_0,
                Handles.ALERTCONDITION_0,
                Handles.ALERTCONDITION_1);

        final MdibDescriptionModifications descriptionModifications = new MdibDescriptionModifications();
        descriptionModifications.insert(
            MockModelFactory.createPair(Handles.MDS_0, MdsDescriptor.class, MdsState.class),
            null
        );
        descriptionModifications.insert(
            MockModelFactory.createPair(Handles.ALERTSYSTEM_0, AlertSystemDescriptor.class, AlertSystemState.class),
            Handles.MDS_0
        );
        descriptionModifications.insert(
            MockModelFactory.createPair(Handles.ALERTCONDITION_0, AlertConditionDescriptor.class, AlertConditionState.class),
            Handles.ALERTSYSTEM_0
        );
        descriptionModifications.insert(
            MockModelFactory.createPair(Handles.ALERTCONDITION_1, AlertConditionDescriptor.class, AlertConditionState.class),
            Handles.ALERTSYSTEM_0
        );
        descriptionModifications.insert(
            MockModelFactory.createPair(Handles.SYSTEMCONTEXT_0, SystemContextDescriptor.class, SystemContextState.class),
            Handles.MDS_0
        );
        descriptionModifications.insert(
            MockModelFactory.createContextStatePair(
                Handles.CONTEXTDESCRIPTOR_0,
                BigInteger.ZERO,
                PatientContextDescriptor.class,
                PatientContextState.class,
                Handles.CONTEXT_0,
                Handles.CONTEXT_1
            ),
            Handles.SYSTEMCONTEXT_0
        );
        descriptionModifications.insert(
            MockModelFactory.createContextStatePair(
                Handles.CONTEXTDESCRIPTOR_1,
                BigInteger.ZERO,
                LocationContextDescriptor.class,
                LocationContextState.class
            ),
            Handles.SYSTEMCONTEXT_0
        );

        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), descriptionModifications);

        testWithVersion(testedHandles, BigInteger.ZERO);

        var stateModificationsAlert = new MdibStateModifications.Alert(new ArrayList<>());
        stateModificationsAlert.getAlertStates().add(MockModelFactory.createState(Handles.ALERTSYSTEM_0, BigInteger.ONE, AlertSystemState.class));
        stateModificationsAlert.getAlertStates().add(MockModelFactory.createState(Handles.ALERTCONDITION_0, BigInteger.ONE, AlertConditionState.class));
        stateModificationsAlert.getAlertStates().add(MockModelFactory.createState(Handles.ALERTCONDITION_1, BigInteger.ONE, AlertConditionState.class));
        try {
            stateModificationsAlert.getStates().add(MockModelFactory.createState(Handles.MDS_0, BigInteger.ONE, MdsState.class));
            Assertions.fail("Could add MDS to alert state change set");
        } catch (Exception ignored) {
        }

        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), stateModificationsAlert);
        testWithVersion(testedHandles, BigInteger.ZERO, BigInteger.ONE);

        testedHandles = List.of(Handles.CONTEXTDESCRIPTOR_0);

        var stateModificationsContext = new MdibStateModifications.Context(new ArrayList<>());

        stateModificationsContext.getContextStates().add(MockModelFactory.createContextState(Handles.CONTEXT_0, Handles.CONTEXTDESCRIPTOR_0, BigInteger.ONE, PatientContextState.class));
        stateModificationsContext.getContextStates().add(MockModelFactory.createContextState(Handles.CONTEXT_1, Handles.CONTEXTDESCRIPTOR_0, BigInteger.ONE, PatientContextState.class));
        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), stateModificationsContext);

        testWithVersion(testedHandles, BigInteger.ZERO, BigInteger.ONE);

        // add a MultiState to a descriptor without any previous states
        stateModificationsContext = new MdibStateModifications.Context(new ArrayList<>());
        stateModificationsContext.getContextStates().add(MockModelFactory.createContextState(Handles.CONTEXT_2, Handles.CONTEXTDESCRIPTOR_1, BigInteger.ONE, LocationContextState.class));
        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), stateModificationsContext);
        assertThat(mdibStorage.getContextStates(Handles.CONTEXTDESCRIPTOR_1, LocationContextState.class).size(), is(1));

        // add a second MultiState
        stateModificationsContext = new MdibStateModifications.Context(new ArrayList<>());
        stateModificationsContext.getContextStates().add(MockModelFactory.createContextState(Handles.CONTEXT_3, Handles.CONTEXTDESCRIPTOR_1, BigInteger.ONE, LocationContextState.class));
        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), stateModificationsContext);
        assertThat(mdibStorage.getContextStates(Handles.CONTEXTDESCRIPTOR_1, LocationContextState.class).size(), is(2));
    }

    @Test
    void writeNotAssociatedContextStatesThroughDescriptionUpdate() throws PairException {
        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);

        // Make sure there are two states in the MDIB for tested patient context
        var states = mdibStorage.findContextStatesByType(PatientContextState.class);
        assertEquals(2, states.size());

        // Get states and set the first one not-associated
        var entity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(PatientContextState.class);
        assertEquals(2, states.size());
        states.get(0).setContextAssociation(ContextAssociation.NO);
        var modifications = new MdibDescriptionModifications()
                .update(org.somda.sdc.biceps.common.Pair.tryFromThrowing(entity.orElseThrow().getDescriptor(), states));
        var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

        // After write check that there was one updated entity with two updated states where one state is not associated
        assertEquals(1, result.getUpdatedEntities().size());
        assertEquals(2, result.getUpdatedEntities().get(0).getStates().size());
        states = result.getUpdatedEntities().get(0).getStates(PatientContextState.class);
        assertEquals(Handles.CONTEXT_0, states.get(0).getHandle());
        assertEquals(ContextAssociation.NO, states.get(0).getContextAssociation());

        // Check that the state is missing from the MDIB
        entity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(PatientContextState.class);
        assertEquals(1, states.size());
        assertEquals(Handles.CONTEXT_1, states.get(0).getHandle());

        // Whitebox: triggers internal access to contextStates map
        // That one shall be updated, too
        states = mdibStorage.findContextStatesByType(PatientContextState.class);
        assertEquals(1, states.size());
    }

    @Test
    void writeNotAssociatedContextStatesThroughDescriptionInsert() throws PairException {
        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);

        // Make sure there are two states in the MDIB for tested patient context
        var states = mdibStorage.findContextStatesByType(EnsembleContextState.class);
        assertEquals(0, states.size());

        // Get states and add another one as not associated
        var newDescriptor = new EnsembleContextDescriptor();
        newDescriptor.setHandle(Handles.CONTEXTDESCRIPTOR_2);
        var newState = new EnsembleContextState();
        newState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_2);
        newState.setHandle(Handles.CONTEXT_3);
        newState.setContextAssociation(ContextAssociation.NO);
        states.add(newState);
        var modifications = new MdibDescriptionModifications()
                .insert(org.somda.sdc.biceps.common.Pair.tryFromThrowing(newDescriptor, states), Handles.SYSTEMCONTEXT_0);
        var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

        // After write check that there was one inserted entity with one inserted state where that one state is not associated
        assertEquals(1, result.getInsertedEntities().size());
        assertEquals(1, result.getInsertedEntities().get(0).getStates().size());
        states = result.getInsertedEntities().get(0).getStates(EnsembleContextState.class);
        assertEquals(Handles.CONTEXT_3, states.get(0).getHandle());
        assertEquals(ContextAssociation.NO, states.get(0).getContextAssociation());

        // Check that the state is missing from the MDIB as a not-associated context state is never written to the MDIB
        var entity = mdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_2);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(EnsembleContextState.class);
        assertEquals(0, states.size());

        // Whitebox: triggers internal access to contextStates map
        states = mdibStorage.findContextStatesByType(EnsembleContextState.class);
        assertEquals(0, states.size());
    }

    @Test
    void writeNotAssociatedContextStatesThroughStateUpdate() throws PairException {
        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);

        {
            // Read disassociated context, write to associated
            var state = mdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            assertEquals(ContextAssociation.DIS, state.orElseThrow().getContextAssociation());
            state.orElseThrow().setContextAssociation(ContextAssociation.ASSOC);
            mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(state.orElseThrow()))
            );
            state = mdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            assertEquals(ContextAssociation.ASSOC, state.orElseThrow().getContextAssociation());
        }
        {
            // Read associated context, write to not-associated (i.e., remove)
            var state = mdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            state.orElseThrow().setContextAssociation(ContextAssociation.NO);
            var applyResult = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(state.orElseThrow()))
            );
            // Expect the state to be part of the report
            assertInstanceOf(WrittenMdibStateModifications.Context.class, applyResult.getStates());
            final var written = (WrittenMdibStateModifications.Context) applyResult.getStates();
            // one per mds
            assertEquals(1, written.getContextStates().size());
            assertEquals(1, written.getContextStates().values().stream().findFirst().orElseThrow().size());
            var writtenState = written.getContextStates().values().stream().findFirst().orElseThrow().get(0);
            assertNotNull(writtenState);
            assertEquals(ContextAssociation.NO, writtenState.getContextAssociation());
            state = mdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            // Expect the state not to be in the MDIB anymore
            assertTrue(state.isEmpty());
        }
        {
            // Write a new context state as not-associated
            var contextState = new PatientContextState();
            contextState.setHandle(Handles.CONTEXT_3);
            contextState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_0);

            var applyResult = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(contextState))
            );
            // Expect the state to be part of the report
            assertInstanceOf(WrittenMdibStateModifications.Context.class, applyResult.getStates());
            final var written = (WrittenMdibStateModifications.Context) applyResult.getStates();

            assertEquals(1, written.getContextStates().size());
            assertEquals(1, written.getContextStates().values().stream().findFirst().orElseThrow().size());
            var writtenState = written.getContextStates().values().stream().findFirst().orElseThrow().get(0);
            assertNotNull(writtenState);
            assertNull(writtenState.getContextAssociation());
            var state = mdibStorage.getState(Handles.CONTEXT_3, AbstractContextState.class);
            // Expect the state not to be in the MDIB
            assertTrue(state.isEmpty());
        }
    }

    @Test
    void testParentOfInsertedEntitiesDoesNotAppearInUpdatedList() throws PairException {
        applyDescriptionInsertWithVersion(mdibStorage, BigInteger.ZERO);

        {
            // Check that parent MDS appears in updated list
            var modifications = new MdibDescriptionModifications()
                .insert(
                    MockModelFactory.createPair(Handles.VMD_1, BigInteger.ZERO, VmdDescriptor.class, VmdState.class),
                    Handles.MDS_0
                );
            var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

            assertEquals(1, result.getInsertedEntities().size());
            assertEquals(Handles.VMD_1, result.getInsertedEntities().get(0).getHandle());
            assertEquals(0, result.getUpdatedEntities().size());
        }

        {
            // Check that parent VMD does not appear twice in updated list
            var modifications = new MdibDescriptionModifications()
                    .insert(
                        MockModelFactory.createPair(Handles.CHANNEL_2, BigInteger.ZERO, ChannelDescriptor.class, ChannelState.class),
                        Handles.VMD_0
                    )
                    .update(
                        MockModelFactory.createPair(Handles.VMD_0, BigInteger.ZERO, MdsDescriptor.class, MdsState.class)
                    );
            var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

            assertEquals(1, result.getInsertedEntities().size());
            assertEquals(Handles.CHANNEL_2, result.getInsertedEntities().get(0).getHandle());
            assertEquals(1, result.getUpdatedEntities().size());
        }

        {
            // Check that existing parent VMD does not appear twice in updated list
            var modifications = new MdibDescriptionModifications()
                    .insert(
                        MockModelFactory.createPair(Handles.CHANNEL_2, BigInteger.ZERO, ChannelDescriptor.class, ChannelState.class),
                        Handles.VMD_0
                    )
                    .update(
                        MockModelFactory.createPair(Handles.VMD_0, BigInteger.ZERO, MdsDescriptor.class, MdsState.class)
                    );
            var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

            assertEquals(1, result.getInsertedEntities().size());
            assertEquals(Handles.CHANNEL_2, result.getInsertedEntities().get(0).getHandle());
            assertEquals(1, result.getUpdatedEntities().size());
        }

        {
            // Check that inserted parent VMD does not appear in updated list (it is sufficient to add it to the
            // insertedEntities list
            var modifications = new MdibDescriptionModifications()
                    .insert(
                        MockModelFactory.createPair(Handles.VMD_2, BigInteger.ZERO, VmdDescriptor.class, VmdState.class),
                        Handles.MDS_0
                    )
                    .insert(
                        MockModelFactory.createPair(Handles.CHANNEL_3, BigInteger.ZERO, ChannelDescriptor.class, ChannelState.class),
                        Handles.VMD_2
                    );
            var result = mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

            assertEquals(2, result.getInsertedEntities().size());
            assertEquals(Handles.VMD_2, result.getInsertedEntities().get(0).getHandle());
            assertEquals(Handles.CHANNEL_3, result.getInsertedEntities().get(1).getHandle());
            assertEquals(0, result.getUpdatedEntities().size());
        }
    }

    MdibStorage mdibStorageWithKeepContextStates() {
        Injector injector = new UnitTestUtil(new AbstractConfigurationModule() {
            @Override
            protected void defaultConfigure() {
                bind(CommonConfig.COPY_MDIB_INPUT, Boolean.class, true);
                bind(CommonConfig.COPY_MDIB_OUTPUT, Boolean.class, true);
                bind(CommonConfig.STORE_NOT_ASSOCIATED_CONTEXT_STATES, Boolean.class, true);
                bind(CommonConfig.CONSUMER_STATE_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<List<Class<? extends StatePreprocessingSegment>>>() {
                    },
                        List.of(DuplicateContextStateHandleHandler.class));

                bind(CommonConfig.CONSUMER_DESCRIPTION_PREPROCESSING_SEGMENTS,
                        new TypeLiteral<List<Class<? extends DescriptionPreprocessingSegment>>>() {
                        },
                        List.of(DescriptorChildRemover.class));

                bind(CommonConfig.PROVIDER_STATE_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<>() {
                    },
                        List.of(
                            DuplicateContextStateHandleHandler.class, VersionHandler.class,
                            ContextHandleDuplicateChecker.class, TypeChangeChecker.class
                            )
                );

                bind(CommonConfig.PROVIDER_DESCRIPTION_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<>() {
                    },
                    List.of(DuplicateChecker.class, TypeConsistencyChecker.class, VersionHandler.class,
                        HandleReferenceChecker.class, DescriptorChildRemover.class, EmptyWriteChecker.class,
                        DuplicateDescriptorChecker.class, TypeChangeChecker.class, ContextHandleDuplicateChecker.class
                    )
                );
            }
        }).getInjector();
        return injector.getInstance(MdibStorageFactory.class).createMdibStorage();
    }

    /*
     Tests intentionally enabling storage of not associated context states.
     */
    @Test
    void keepNotAssociatedContextStatesThroughDescriptionUpdate() throws PairException {
        var localMdibStorage = mdibStorageWithKeepContextStates();
        applyDescriptionInsertWithVersion(localMdibStorage, BigInteger.ZERO);

        // Make sure there are two states in the MDIB for tested patient context
        var states = localMdibStorage.findContextStatesByType(PatientContextState.class);
        assertEquals(2, states.size());

        // Get states and set the first one not-associated
        var entity = localMdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(PatientContextState.class);
        assertEquals(2, states.size());
        states.get(0).setContextAssociation(ContextAssociation.NO);
        var modifications = new MdibDescriptionModifications()
                .update(org.somda.sdc.biceps.common.Pair.tryFromThrowing(entity.orElseThrow().getDescriptor(), states));
        var result = localMdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

        // After write check that there was one updated entity with two updated states where one state is not associated
        assertEquals(1, result.getUpdatedEntities().size());
        assertEquals(2, result.getUpdatedEntities().get(0).getStates().size());
        states = result.getUpdatedEntities().get(0).getStates(PatientContextState.class);
        assertEquals(Handles.CONTEXT_0, states.get(0).getHandle());
        assertEquals(ContextAssociation.NO, states.get(0).getContextAssociation());

        // Check that the state is present from the MDIB
        entity = localMdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_0);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(PatientContextState.class);
        assertEquals(2, states.size());
        assertEquals(Handles.CONTEXT_0, states.get(0).getHandle());
        assertEquals(Handles.CONTEXT_1, states.get(1).getHandle());

        // Whitebox: triggers internal access to contextStates map
        // That one shall be updated, too
        states = localMdibStorage.findContextStatesByType(PatientContextState.class);
        assertEquals(2, states.size());
    }

    @Test
    void keepNotAssociatedContextStatesThroughDescriptionInsert() throws PairException {
        var localMdibStorage = mdibStorageWithKeepContextStates();
        applyDescriptionInsertWithVersion(localMdibStorage, BigInteger.ZERO);

        // Make sure there are no states in the MDIB for tested patient context
        var states = localMdibStorage.findContextStatesByType(EnsembleContextState.class);
        assertEquals(0, states.size());

        // Get states and add another one as not associated
        var newDescriptor = new EnsembleContextDescriptor();
        newDescriptor.setHandle(Handles.CONTEXTDESCRIPTOR_2);
        var newState = new EnsembleContextState();
        newState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_2);
        newState.setHandle(Handles.CONTEXT_3);
        newState.setContextAssociation(ContextAssociation.NO);
        states.add(newState);
        var modifications = new MdibDescriptionModifications()
                .insert(
                    org.somda.sdc.biceps.common.Pair.tryFromThrowing(newDescriptor, states),
                    Handles.SYSTEMCONTEXT_0
                );
        var result = localMdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);

        // After write check that there was one inserted entity with one inserted state where that one state is not associated
        assertEquals(1, result.getInsertedEntities().size());
        assertEquals(1, result.getInsertedEntities().get(0).getStates().size());
        states = result.getInsertedEntities().get(0).getStates(EnsembleContextState.class);
        assertEquals(Handles.CONTEXT_3, states.get(0).getHandle());
        assertEquals(ContextAssociation.NO, states.get(0).getContextAssociation());

        // Check that the state is not missing from the MDIB as a not-associated context state is written to the MDIB
        var entity = localMdibStorage.getEntity(Handles.CONTEXTDESCRIPTOR_2);
        assertTrue(entity.isPresent());
        states = entity.orElseThrow().getStates(EnsembleContextState.class);
        assertEquals(1, states.size());

        // Whitebox: triggers internal access to contextStates map
        states = localMdibStorage.findContextStatesByType(EnsembleContextState.class);
        assertEquals(1, states.size());
    }

    @Test
    void keepNotAssociatedContextStatesThroughStateUpdate() throws PairException {
        var localMdibStorage = mdibStorageWithKeepContextStates();
        applyDescriptionInsertWithVersion(localMdibStorage, BigInteger.ZERO);

        {
            // Read disassociated context, write to associated
            var state = localMdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            assertEquals(ContextAssociation.DIS, state.orElseThrow().getContextAssociation());
            state.orElseThrow().setContextAssociation(ContextAssociation.ASSOC);
            localMdibStorage.apply(
                mock(MdibVersion.class),
                mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(state.orElseThrow()))
            );
            state = localMdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            assertEquals(ContextAssociation.ASSOC, state.orElseThrow().getContextAssociation());
        }
        {
            // Read associated context, write to not-associated (i.e., remove)
            var state = localMdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            assertTrue(state.isPresent());
            state.orElseThrow().setContextAssociation(ContextAssociation.NO);
            var applyResult = localMdibStorage.apply(
                mock(MdibVersion.class),
                mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(state.orElseThrow()))
            );
            // Expect the state to be part of the report
            assertInstanceOf(WrittenMdibStateModifications.Context.class, applyResult.getStates());
            final var written = (WrittenMdibStateModifications.Context) applyResult.getStates();

            assertEquals(1, written.getContextStates().size());
            var statesList = written.getContextStates().values().stream().findFirst().orElseThrow();
            assertEquals(1, statesList.size());
            var writtenState = statesList.get(0);
            assertNotNull(writtenState);
            assertEquals(ContextAssociation.NO, writtenState.getContextAssociation());
            state = localMdibStorage.getState(Handles.CONTEXT_0, AbstractContextState.class);
            // Expect the state not to be in the MDIB anymore
            assertTrue(state.isPresent());
        }
        {
            // Write a new context state as not-associated
            var contextState = new PatientContextState();
            contextState.setHandle(Handles.CONTEXT_3);
            contextState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_0);

            var applyResult = localMdibStorage.apply(
                mock(MdibVersion.class),
                mock(BigInteger.class),
                new MdibStateModifications.Context(List.of(contextState))
            );
            // Expect the state to be part of the report
            assertInstanceOf(WrittenMdibStateModifications.Context.class, applyResult.getStates());
            final var written = (WrittenMdibStateModifications.Context) applyResult.getStates();

            assertEquals(1, written.getContextStates().size());
            var statesList = written.getContextStates().values().stream().findFirst().orElseThrow();
            assertEquals(1, statesList.size());
            var writtenState = statesList.get(0);
            assertNotNull(writtenState);
            assertNull(writtenState.getContextAssociation());
            var state = localMdibStorage.getState(Handles.CONTEXT_3, AbstractContextState.class);
            // Expect the state not to be in the MDIB
            assertTrue(state.isPresent());
        }
    }

    @Test
    void testStateUpdateWithoutDescriptor() throws PairException {
        Injector injector = new UnitTestUtil(new AbstractConfigurationModule() {
            @Override
            protected void defaultConfigure() {
                bind(CommonConfig.COPY_MDIB_INPUT, Boolean.class, true);
                bind(CommonConfig.COPY_MDIB_OUTPUT, Boolean.class, true);
                bind(CommonConfig.STORE_NOT_ASSOCIATED_CONTEXT_STATES, Boolean.class, true);
                bind(CommonConfig.CONSUMER_STATE_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<List<Class<? extends StatePreprocessingSegment>>>() {
                    },
                        List.of(DuplicateContextStateHandleHandler.class));

                bind(CommonConfig.CONSUMER_DESCRIPTION_PREPROCESSING_SEGMENTS,
                        new TypeLiteral<List<Class<? extends DescriptionPreprocessingSegment>>>() {
                        },
                        List.of(DescriptorChildRemover.class));

                bind(CommonConfig.PROVIDER_STATE_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<>() {
                    },
                        List.of(
                            DuplicateContextStateHandleHandler.class, VersionHandler.class,
                            ContextHandleDuplicateChecker.class, TypeChangeChecker.class
                        ));

                bind(CommonConfig.PROVIDER_DESCRIPTION_PREPROCESSING_SEGMENTS,
                    new TypeLiteral<>() {
                    },
                    List.of(DuplicateChecker.class, TypeConsistencyChecker.class, VersionHandler.class,
                        HandleReferenceChecker.class, DescriptorChildRemover.class, EmptyWriteChecker.class,
                        DuplicateDescriptorChecker.class, TypeChangeChecker.class, ContextHandleDuplicateChecker.class
                    )
                );
            }
        }).getInjector();
        var localMdibStorage = injector.getInstance(MdibStorageFactory.class).createMdibStorage();

        {
            // Write a new context state as not-associated
            var contextState = new PatientContextState();
            contextState.setHandle(Handles.CONTEXT_3);
            contextState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_0);

            assertThrows(
                RuntimeException.class,
                () -> localMdibStorage.apply(
                    mock(MdibVersion.class), mock(BigInteger.class),
                    new MdibStateModifications.Context(List.of(contextState))
                )
            );
        }
        {
            // Make sure there are no states in the MDIB for tested patient context
            var states = localMdibStorage.findContextStatesByType(EnsembleContextState.class);
            assertEquals(0, states.size());

            // Get states and add another one as not associated
            var newDescriptor = new EnsembleContextDescriptor();
            newDescriptor.setHandle(Handles.CONTEXTDESCRIPTOR_2);
            var newState = new EnsembleContextState();
            newState.setDescriptorHandle(Handles.CONTEXTDESCRIPTOR_2);
            newState.setHandle(Handles.CONTEXT_3);
            newState.setContextAssociation(ContextAssociation.NO);
            states.add(newState);
            var modifications = new MdibDescriptionModifications()
                    .insert(org.somda.sdc.biceps.common.Pair.tryFromThrowing(newDescriptor, states), Handles.SYSTEMCONTEXT_0);

            // throws an exception about system context 0 not being present
            var ex = assertThrows(RuntimeException.class, () -> localMdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications));
            assertTrue(ex.getMessage().contains(Handles.SYSTEMCONTEXT_0));
        }
    }

    void distinctEntities(MdibStorage localMdibStorage) {
        var entities = localMdibStorage.findEntitiesByType(AbstractDescriptor.class);

        var handles = entities.stream()
            .map(MdibEntity::getHandle)
            .toList();
        var distinctHandles = new HashSet<>(handles);
        assertEquals(distinctHandles.size(), handles.size());
    }

    @Test
    void testFindEntitiesByTypeNoDuplicate() throws PairException {
        var localMdibStorage = mdibStorageWithKeepContextStates();
        applyDescriptionInsertWithVersion(localMdibStorage, BigInteger.ZERO);
        distinctEntities(localMdibStorage);

        // modify context state in mdib
        {
            var stateModificationsContext = new MdibStateModifications.Context(new ArrayList<>());
            stateModificationsContext.getContextStates().add(MockModelFactory.createContextState(Handles.CONTEXT_0, Handles.CONTEXTDESCRIPTOR_0, BigInteger.ONE, PatientContextState.class));
            localMdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), stateModificationsContext);

            var updatedState = localMdibStorage.getState(Handles.CONTEXT_0).orElseThrow();
            assertEquals(BigInteger.ONE, updatedState.getStateVersion());
        }
        distinctEntities(localMdibStorage);
    }

}
