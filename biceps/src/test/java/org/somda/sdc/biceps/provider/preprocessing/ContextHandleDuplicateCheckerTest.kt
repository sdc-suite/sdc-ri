package org.somda.sdc.biceps.provider.preprocessing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.somda.sdc.biceps.UnitTestUtil
import org.somda.sdc.biceps.common.CommonConfig
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibDescriptionModifications
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.MdibTypeValidator
import org.somda.sdc.biceps.common.storage.MdibStorage
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory
import org.somda.sdc.biceps.guice.DefaultBicepsConfigModule
import org.somda.sdc.biceps.model.participant.MdibVersion
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor
import org.somda.sdc.biceps.model.participant.PatientContextState
import org.somda.sdc.biceps.testutil.BaseTreeModificationsSet
import org.somda.sdc.biceps.testutil.Handles
import org.somda.sdc.biceps.testutil.MockEntryFactory
import org.somda.sdc.biceps.testutil.MockModelFactory
import test.org.somda.common.LoggingTestWatcher
import java.math.BigInteger

@ExtendWith(LoggingTestWatcher::class)
class ContextHandleDuplicateCheckerTest {

    private val unitTestUtil = UnitTestUtil(object : DefaultBicepsConfigModule() {
        override fun customConfigure() {
            // Configure to avoid copying and make comparison easier
            bind(
                CommonConfig.COPY_MDIB_OUTPUT,
                Boolean::class.java,
                false
            )
        }
    })

    private lateinit var mdibStorage: MdibStorage
    private lateinit var contextHandleDuplicateChecker: ContextHandleDuplicateChecker

    private val mdibTypeValidator = unitTestUtil.injector.getInstance(MdibTypeValidator::class.java)
    private val mockEntryFactory = MockEntryFactory(mdibTypeValidator)


    @BeforeEach
    fun beforeEach() {
        // Given a version handler and sample input
        mdibStorage = unitTestUtil.injector.getInstance(MdibStorageFactory::class.java)
            .createMdibStorage()
        contextHandleDuplicateChecker = unitTestUtil.injector.getInstance(ContextHandleDuplicateChecker::class.java)

        mdibStorage.apply(
            MdibVersion.create(),
            BigInteger.ZERO,
            BigInteger.ZERO,
            BaseTreeModificationsSet(mockEntryFactory).createBaseTree()
        )
    }

    private fun apply(modifications: MdibDescriptionModifications) {
        contextHandleDuplicateChecker.process(modifications.asList().toMutableList(), mdibStorage)
    }

    private fun apply(modifications: MdibStateModifications) {
        contextHandleDuplicateChecker.process(modifications, mdibStorage)
    }

    @Test
    fun `throw exception if context state handle in descriptor insert collides with descriptor`() {
        val collisionHandle = Handles.MDS_0
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Insert(
                    MockModelFactory.createContextStatePair(
                        "definitelyNoCollisionHere",
                        BigInteger.ZERO,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        collisionHandle
                    ), Handles.MDS_1
                )
            )

        val thrown = Assertions.assertThrows(HandleDuplicatedException::class.java) { apply(modifications) }
        Assertions.assertTrue(thrown.message?.contains(collisionHandle) ?: false)
    }


    @Test
    fun `valid context state in descriptor insert passes`() {
        val collisionHandle = "alsoNoCollisionHere"
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Insert(
                    MockModelFactory.createContextStatePair(
                        "definitelyNoCollisionHere",
                        BigInteger.ZERO,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        collisionHandle
                    ), Handles.MDS_1
                )
            )

        Assertions.assertDoesNotThrow { apply(modifications) }
    }

    @Test
    fun `throw exception if context state handle in descriptor update collides with descriptor`() {
        val collisionHandle = Handles.MDS_0
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Update(
                    MockModelFactory.createContextStatePair(
                        "definitelyNoCollisionHere",
                        BigInteger.ZERO,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        collisionHandle
                    )
                )
            )

        val thrown = Assertions.assertThrows(HandleDuplicatedException::class.java) { apply(modifications) }
        Assertions.assertTrue(thrown.message?.contains(collisionHandle) ?: false)
    }


    @Test
    fun `valid context state in descriptor update passes`() {
        val collisionHandle = "alsoNoCollisionHere"
        val modifications = MdibDescriptionModifications()
            .add(
                MdibDescriptionModification.Update(
                    MockModelFactory.createContextStatePair(
                        "definitelyNoCollisionHere",
                        BigInteger.ZERO,
                        PatientContextDescriptor::class.java,
                        PatientContextState::class.java,
                        collisionHandle
                    )
                )
            )

        Assertions.assertDoesNotThrow { apply(modifications) }
    }

    @Test
    fun `throw exception if context state handle in context state update collides with descriptor`() {
        val collisionHandle = Handles.MDS_0
        val noCollisionCtx = MockModelFactory.createContextState(
            "noCollisionHere",
            Handles.CONTEXTDESCRIPTOR_0,
            PatientContextState::class.java
        )
        val ctx = MockModelFactory.createContextState(
            collisionHandle,
            Handles.CONTEXTDESCRIPTOR_0,
            PatientContextState::class.java
        )
        val modifications = MdibStateModifications.Context(mutableListOf(noCollisionCtx, ctx))

        val thrown = Assertions.assertThrows(HandleDuplicatedException::class.java) { apply(modifications) }
        Assertions.assertTrue(thrown.message?.contains(collisionHandle) ?: false)
    }

    @Test
    fun `valid context state handle in context state update passes`() {
        val collisionHandle = "alsoNoCollisionHere"
        val noCollisionCtx = MockModelFactory.createContextState(
            "noCollisionHere",
            Handles.CONTEXTDESCRIPTOR_0,
            PatientContextState::class.java
        )
        val ctx = MockModelFactory.createContextState(
            collisionHandle,
            Handles.CONTEXTDESCRIPTOR_0,
            PatientContextState::class.java
        )
        val modifications = MdibStateModifications.Context(mutableListOf(noCollisionCtx, ctx))

        Assertions.assertDoesNotThrow { apply(modifications) }
    }
}
