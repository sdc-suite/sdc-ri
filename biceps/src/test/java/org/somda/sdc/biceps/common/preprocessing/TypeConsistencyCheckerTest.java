package org.somda.sdc.biceps.common.preprocessing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.somda.sdc.biceps.UnitTestUtil;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.PairException;
import org.somda.sdc.biceps.common.storage.MdibStorage;
import org.somda.sdc.biceps.common.storage.factory.MdibStorageFactory;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelState;
import org.somda.sdc.biceps.model.participant.MdibVersion;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MdsState;
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor;
import org.somda.sdc.biceps.model.participant.NumericMetricState;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;
import org.somda.sdc.biceps.model.participant.VmdState;
import org.somda.sdc.biceps.provider.preprocessing.TypeConsistencyChecker;
import org.somda.sdc.biceps.testutil.MockModelFactory;
import test.org.somda.common.LoggingTestWatcher;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

@ExtendWith(LoggingTestWatcher.class)
class TypeConsistencyCheckerTest {
    private static final UnitTestUtil UT = new UnitTestUtil();

    private TypeConsistencyChecker consistencyHandler;
    private MdibStorage mdibStorage;

    private String mds;
    private String vmd1;
    private String vmd2;
    private String channel;

    @BeforeEach
    void beforeEach() throws PairException {
        consistencyHandler = UT.getInjector().getInstance(TypeConsistencyChecker.class);
        mdibStorage = UT.getInjector().getInstance(MdibStorageFactory.class).createMdibStorage();

        mds = "mds";
        vmd1 = "vmd1";
        vmd2 = "vmd2";
        channel = "channel";

        MdibDescriptionModifications modifications = new MdibDescriptionModifications();

        modifications.insert(MockModelFactory.createPair(mds, MdsDescriptor.class, MdsState.class), null);
        modifications.insert(MockModelFactory.createPair(vmd1, VmdDescriptor.class, VmdState.class), mds);
        modifications.insert(MockModelFactory.createPair(channel, ChannelDescriptor.class, ChannelState.class), vmd1);
        mdibStorage.apply(mock(MdibVersion.class), mock(BigInteger.class), mock(BigInteger.class), modifications);
    }

    @Test
    void missingParent() throws Exception {
        MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.insert(MockModelFactory.createPair("vmd3", VmdDescriptor.class, VmdState.class), null);
        assertThrows(Exception.class, () -> apply(modifications));
    }

    @Test
    void mdsWithParent() throws Exception {
        MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.insert(MockModelFactory.createPair("mds", MdsDescriptor.class, MdsState.class), "invalid");
        assertThrows(Exception.class, () -> apply(modifications));
    }

    @Test
    void invalidParent() throws Exception {
        MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.insert(MockModelFactory.createPair("vmd", VmdDescriptor.class, VmdState.class), "invalid");
        assertThrows(Exception.class, () -> apply(modifications));
    }

    @Test
    void validParentInMdibStorage() throws Exception {
        MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.insert(MockModelFactory.createPair("metric", NumericMetricDescriptor.class, NumericMetricState.class), channel);
        assertDoesNotThrow(() -> apply(modifications));
    }

    @Test
    void validParentInModifications() throws Exception {
        MdibDescriptionModifications modifications = new MdibDescriptionModifications();
        modifications.insert(MockModelFactory.createPair(vmd2, VmdDescriptor.class, VmdState.class), mds);
        modifications.insert(MockModelFactory.createPair("channel", ChannelDescriptor.class, ChannelState.class), vmd2);
        assertDoesNotThrow(() -> apply(modifications));
    }

    private void apply(MdibDescriptionModifications modifications) throws Exception {
        consistencyHandler.process(modifications.asList(), mdibStorage);
    }
}