package org.somda.sdc.biceps.provider.preprocessing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.storage.MdibStorage;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MdsState;
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor;
import org.somda.sdc.biceps.model.participant.PatientContextState;
import org.somda.sdc.biceps.testutil.MockModelFactory;
import test.org.somda.common.LoggingTestWatcher;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(LoggingTestWatcher.class)
class DuplicateCheckerTest {
    @Test
    void processDuplicate() throws Exception {
        // Given a duplicate detector
        final DuplicateChecker duplicateChecker = new DuplicateChecker();

        final String expectedNonExistingHandle = "nonExistingHandle";
        final String expectedExistingHandle = "existingHandle";
        final List<MdibDescriptionModification> modifications = List.of(
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedNonExistingHandle, MdsDescriptor.class, MdsState.class), null),
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedExistingHandle, MdsDescriptor.class, MdsState.class), null),
            new MdibDescriptionModification.Update(MockModelFactory.createPair(expectedExistingHandle, MdsDescriptor.class, MdsState.class))
        );

        final MdibStorage mdibStorage = Mockito.mock(MdibStorage.class);
        Mockito.when(mdibStorage.getEntity(expectedNonExistingHandle)).thenReturn(Optional.empty());
        Mockito.when(mdibStorage.getEntity(expectedExistingHandle)).thenReturn(Optional.of(Mockito.mock(MdibEntity.class)));

        // When there is a duplication detected
        // Then expect the detector to throw an exception
        final var thrown = assertThrows(HandleDuplicatedException.class, () ->
                duplicateChecker.process(modifications, mdibStorage));
        assertTrue(thrown.getMessage().contains(expectedExistingHandle));
    }

    @Test
    void processDuplicateInContextState() throws Exception {
        // Given a duplicate detector
        final DuplicateChecker duplicateChecker = new DuplicateChecker();

        final String expectedNonExistingHandle = "nonExistingHandle";
        final String expectedExistingHandle = "existingHandle";
        final List<MdibDescriptionModification> modifications = List.of(
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedNonExistingHandle, MdsDescriptor.class, MdsState.class), null),
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedExistingHandle, MdsDescriptor.class, MdsState.class), null),
            new MdibDescriptionModification.Update(MockModelFactory.createContextStatePair("Whatever", BigInteger.ZERO, PatientContextDescriptor.class, PatientContextState.class, expectedExistingHandle))
        );

        final MdibStorage mdibStorage = Mockito.mock(MdibStorage.class);
        Mockito.when(mdibStorage.getEntity(expectedNonExistingHandle)).thenReturn(Optional.empty());
        Mockito.when(mdibStorage.getEntity(expectedExistingHandle)).thenReturn(Optional.of(Mockito.mock(MdibEntity.class)));

        // When there is a duplication detected
        // Then expect the detector to throw an exception
        final var thrown = assertThrows(HandleDuplicatedException.class, () ->
            duplicateChecker.process(modifications, mdibStorage));

        assertTrue(thrown.getMessage().contains(expectedExistingHandle));
    }


    @Test
    void processNoDuplicate() throws Exception {
        // Given a duplicate detector
        final DuplicateChecker duplicateChecker = new DuplicateChecker();

        final String expectedNonExistingHandle = "nonExistingHandle";
        final String expectedExistingHandle = "existingHandle";

        final List<MdibDescriptionModification> modifications = List.of(
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedNonExistingHandle, MdsDescriptor.class, MdsState.class), null),
            new MdibDescriptionModification.Insert(MockModelFactory.createPair(expectedExistingHandle, MdsDescriptor.class, MdsState.class), null)
        );

        final MdibStorage mdibStorage = Mockito.mock(MdibStorage.class);
        Mockito.when(mdibStorage.getEntity(expectedNonExistingHandle)).thenReturn(Optional.empty());
        Mockito.when(mdibStorage.getEntity(expectedExistingHandle)).thenReturn(Optional.of(Mockito.mock(MdibEntity.class)));

        // When there is no duplication detected
        // Then expect the detector to continue
        assertDoesNotThrow(() ->
            duplicateChecker.process(modifications, mdibStorage));
    }
}