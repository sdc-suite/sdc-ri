package org.somda.sdc.biceps.testutil;

import org.somda.sdc.biceps.common.MdibTypeValidator;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractState;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class MockEntryFactory {
    private final MdibTypeValidator typeValidator;

    public MockEntryFactory(MdibTypeValidator typeValidator) {
        this.typeValidator = typeValidator;
    }

    public <T extends AbstractDescriptor, V extends AbstractState> Pair entry(
        String handle,
        Class<T> descrClass,
        Class<V> stateClass
    ) throws Exception {
        return Pair.tryFromThrowing(
                descriptor(handle, descrClass),
                state(handle, stateClass)
        );
    }

    public <T extends AbstractDescriptor, V extends AbstractState> Pair entry(
        String handle,
        Class<T> descrClass,
        Consumer<T> descrCustomizer,
        Consumer<V> stateCustomizer
    ) throws Exception {
        Class<V> stateClass = typeValidator.resolveStateType(descrClass);
        return Pair.tryFromThrowing(
            descriptor(handle, descrClass, descrCustomizer),
            state(handle, stateClass, stateCustomizer)
        );
    }

    public <T extends AbstractDescriptor, V extends AbstractContextState> Pair contextEntry(
        String handle,
        String stateHandle,
        Class<T> descrClass,
        Class<V> stateClass
    ) throws Exception {
        return Pair.tryFromThrowing(
                descriptor(handle, descrClass),
                List.of(MockModelFactory.createContextState(stateHandle, handle, stateClass))
        );
    }

    public <T extends AbstractDescriptor, V extends AbstractContextState> Pair contextEntry(
        String handle,
        List<String> stateHandles,
        Class<T> descrClass,
        Class<V> stateClass
    ) throws Exception {
        final List<V> states = stateHandles.stream()
            .map(s -> MockModelFactory.createContextState(s, handle, stateClass))
            .toList();
        return Pair.tryFromThrowing(
            descriptor(handle, descrClass),
            states
        );
    }

    public <T extends AbstractDescriptor> T descriptor(String handle, Class<T> theClass) {
        return descriptor(handle, theClass, t -> {});
    }

    public <T extends AbstractDescriptor> T descriptor(String handle, Class<T> theClass, Consumer<T> customizer) {
        var descr = MockModelFactory.createDescriptor(handle, theClass);
        customizer.accept(descr);
        return descr;
    }

    public <T extends AbstractState> T state(String handle, Class<T> theClass) {
        return state(handle, theClass, t -> {});
    }

    public <T extends AbstractState> T state(String handle, Class<T> theClass, Consumer<T> customizer) {
        var state = MockModelFactory.createState(handle, theClass);
        customizer.accept(state);
        return state;
    }

    public <T extends AbstractContextState> T state(String handle, String descrHandle, Class<T> theClass) {
        return MockModelFactory.createContextState(handle, descrHandle, theClass);
    }
}
