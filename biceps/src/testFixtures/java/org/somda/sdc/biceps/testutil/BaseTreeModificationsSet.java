package org.somda.sdc.biceps.testutil;

import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractOperationState;
import org.somda.sdc.biceps.model.participant.AbstractState;
import org.somda.sdc.biceps.model.participant.ActivateOperationDescriptor;
import org.somda.sdc.biceps.model.participant.AlertActivation;
import org.somda.sdc.biceps.model.participant.AlertSignalManifestation;
import org.somda.sdc.biceps.model.participant.AlertSystemDescriptor;
import org.somda.sdc.biceps.model.participant.AlertSystemState;
import org.somda.sdc.biceps.model.participant.BatteryDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.ClockDescriptor;
import org.somda.sdc.biceps.model.participant.DistributionSampleArrayMetricDescriptor;
import org.somda.sdc.biceps.model.participant.EnsembleContextDescriptor;
import org.somda.sdc.biceps.model.participant.EnsembleContextState;
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor;
import org.somda.sdc.biceps.model.participant.LocationContextDescriptor;
import org.somda.sdc.biceps.model.participant.LocationContextState;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MeansContextDescriptor;
import org.somda.sdc.biceps.model.participant.MeansContextState;
import org.somda.sdc.biceps.model.participant.MetricAvailability;
import org.somda.sdc.biceps.model.participant.MetricCategory;
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor;
import org.somda.sdc.biceps.model.participant.OperatingMode;
import org.somda.sdc.biceps.model.participant.OperatorContextDescriptor;
import org.somda.sdc.biceps.model.participant.OperatorContextState;
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor;
import org.somda.sdc.biceps.model.participant.PatientContextState;
import org.somda.sdc.biceps.model.participant.Range;
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricDescriptor;
import org.somda.sdc.biceps.model.participant.ScoDescriptor;
import org.somda.sdc.biceps.model.participant.SetAlertStateOperationDescriptor;
import org.somda.sdc.biceps.model.participant.SetComponentStateOperationDescriptor;
import org.somda.sdc.biceps.model.participant.SetContextStateOperationDescriptor;
import org.somda.sdc.biceps.model.participant.SetMetricStateOperationDescriptor;
import org.somda.sdc.biceps.model.participant.SetStringOperationDescriptor;
import org.somda.sdc.biceps.model.participant.SetValueOperationDescriptor;
import org.somda.sdc.biceps.model.participant.StringMetricDescriptor;
import org.somda.sdc.biceps.model.participant.SystemContextDescriptor;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;
import org.somda.sdc.biceps.model.participant.WorkflowContextDescriptor;
import org.somda.sdc.biceps.model.participant.WorkflowContextState;
import org.somda.sdc.biceps.model.participant.factory.CodedValueFactory;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class BaseTreeModificationsSet {
    private final MockEntryFactory entryFactory;
    private final DescriptorStateDataGenerator dataGenerator;

    public BaseTreeModificationsSet(MockEntryFactory entryFactory) {
        this.entryFactory = entryFactory;
        this.dataGenerator = new DescriptorStateDataGenerator();
    }

    public MdibDescriptionModifications createBaseTree() {
        try {
            return new MdibDescriptionModifications()
                    .insert(entry(Handles.MDS_0, t -> t.setType(CodedValueFactory.createIeeeCodedValue("70001")), t -> {}, MdsDescriptor.class), null)
                    .insert(entry(Handles.MDS_1, t -> t.setType(CodedValueFactory.createIeeeCodedValue("70002")), t -> {}, MdsDescriptor.class), null)

                    .insert(entry(Handles.VMD_0, VmdDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.VMD_1, VmdDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.VMD_2, VmdDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.BATTERY_0, BatteryDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.CLOCK_0, ClockDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.ALERTSYSTEM_0, AlertSystemDescriptor.class,
                        t -> {}, t -> ((AlertSystemState)t).setActivationState(AlertActivation.ON)),
                        Handles.MDS_0)
                    .insert(entry(Handles.SCO_0, ScoDescriptor.class), Handles.MDS_0)
                    .insert(entry(Handles.SCO_2, ScoDescriptor.class), Handles.MDS_1)
                    .insert(entry(Handles.SYSTEMCONTEXT_0, SystemContextDescriptor.class), Handles.MDS_0)

                    .insert(entry(Handles.CHANNEL_0, ChannelDescriptor.class), Handles.VMD_0)
                    .insert(entry(Handles.CHANNEL_1, ChannelDescriptor.class), Handles.VMD_0)

                    .insert(entry(Handles.METRIC_0, NumericMetricDescriptor.class, t -> {
                        t.setResolution(BigDecimal.ONE);
                        t.setMetricCategory(MetricCategory.UNSPEC);
                        t.setMetricAvailability(MetricAvailability.INTR);
                        t.setUnit(CodedValueFactory.createIeeeCodedValue("500"));
                    }, t -> {}), Handles.CHANNEL_0)
                    .insert(entry(Handles.METRIC_1, StringMetricDescriptor.class, t -> {
                        t.setMetricCategory(MetricCategory.UNSPEC);
                        t.setMetricAvailability(MetricAvailability.INTR);
                        t.setUnit(CodedValueFactory.createIeeeCodedValue("500"));
                    }, t -> {}), Handles.CHANNEL_0)
                    .insert(entry(Handles.METRIC_2, EnumStringMetricDescriptor.class, t -> {
                        t.setMetricCategory(MetricCategory.UNSPEC);
                        t.setMetricAvailability(MetricAvailability.INTR);
                        t.setUnit(CodedValueFactory.createIeeeCodedValue("500"));
                        var allowedValue = new EnumStringMetricDescriptor.AllowedValue();
                        allowedValue.setValue("sample");
                        t.setAllowedValue(List.of(allowedValue));
                    }, t -> {}), Handles.CHANNEL_0)
                    .insert(entry(Handles.METRIC_3, RealTimeSampleArrayMetricDescriptor.class, t -> {
                        t.setResolution(BigDecimal.ONE);
                        t.setMetricCategory(MetricCategory.UNSPEC);
                        t.setMetricAvailability(MetricAvailability.INTR);
                        t.setUnit(CodedValueFactory.createIeeeCodedValue("500"));
                        t.setSamplePeriod(Duration.ofSeconds(1));
                    }, t -> {}), Handles.CHANNEL_0)
                    .insert(entry(Handles.METRIC_4, DistributionSampleArrayMetricDescriptor.class, t -> {
                        t.setResolution(BigDecimal.ONE);
                        t.setMetricCategory(MetricCategory.UNSPEC);
                        t.setMetricAvailability(MetricAvailability.INTR);
                        t.setUnit(CodedValueFactory.createIeeeCodedValue("500"));
                        t.setDomainUnit(CodedValueFactory.createIeeeCodedValue("1000"));
                        t.setDistributionRange(new Range());
                    }, t -> {}), Handles.CHANNEL_0)

                    .insert(entry(Handles.OPERATION_0, ActivateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_1, SetStringOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_2, SetValueOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_3, SetComponentStateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_4, SetMetricStateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_5, SetAlertStateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_6, SetContextStateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_0);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }), Handles.SCO_0)
                    .insert(entry(Handles.OPERATION_7, SetContextStateOperationDescriptor.class, t -> {
                        t.setOperationTarget(Handles.MDS_1);
                    }, t -> {
                        ((AbstractOperationState)t).setOperatingMode(OperatingMode.EN);
                    }),Handles.SCO_2)

                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_0, Handles.CONTEXT_0, PatientContextDescriptor.class, PatientContextState.class), Handles.SYSTEMCONTEXT_0)
                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_1, Handles.CONTEXT_1, LocationContextDescriptor.class, LocationContextState.class), Handles.SYSTEMCONTEXT_0)
                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_2, Handles.CONTEXT_2, EnsembleContextDescriptor.class, EnsembleContextState.class), Handles.SYSTEMCONTEXT_0)
                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_3, Handles.CONTEXT_3, WorkflowContextDescriptor.class, WorkflowContextState.class), Handles.SYSTEMCONTEXT_0)
                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_4, Handles.CONTEXT_4, OperatorContextDescriptor.class, OperatorContextState.class), Handles.SYSTEMCONTEXT_0)
                    .insert(contextEntry(Handles.CONTEXTDESCRIPTOR_5, Handles.CONTEXT_5, MeansContextDescriptor.class, MeansContextState.class), Handles.SYSTEMCONTEXT_0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // includes all entities plus data plus exemplary extensions
    public MdibDescriptionModifications createFullyPopulatedTree() {
        try {
            return new MdibDescriptionModifications()
                .insert(Pair.tryFromThrowing(dataGenerator.mdsDescriptor(Handles.MDS_0), dataGenerator.mdsState(Handles.MDS_0)), null)
                .insert(Pair.tryFromThrowing(dataGenerator.clockDescriptor(Handles.CLOCK_0), dataGenerator.clockState(Handles.CLOCK_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.batteryDescriptor(Handles.BATTERY_0), dataGenerator.batteryState(Handles.BATTERY_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.systemContextDescriptor(Handles.SYSTEMCONTEXT_0), dataGenerator.systemContextState(Handles.SYSTEMCONTEXT_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.patientContextDescriptor(Handles.CONTEXTDESCRIPTOR_0), List.of(dataGenerator.patientContextState(Handles.CONTEXT_0, Handles.CONTEXTDESCRIPTOR_0))), Handles.SYSTEMCONTEXT_0)
                .insert(Pair.tryFromThrowing(dataGenerator.locationContextDescriptor(Handles.CONTEXTDESCRIPTOR_1), List.of(dataGenerator.locationContextState(Handles.CONTEXT_1, Handles.CONTEXTDESCRIPTOR_1))), Handles.SYSTEMCONTEXT_0)
                .insert(Pair.tryFromThrowing(dataGenerator.ensembleContextDescriptor(Handles.CONTEXTDESCRIPTOR_3), List.of(dataGenerator.ensembleContextState(Handles.CONTEXT_3, Handles.CONTEXTDESCRIPTOR_3))), Handles.SYSTEMCONTEXT_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSystemDescriptor(Handles.ALERTSYSTEM_0), dataGenerator.alertSystemState(Handles.ALERTSYSTEM_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertConditionDescriptor(Handles.ALERTCONDITION_0, Handles.MDS_0), dataGenerator.alertConditionState(Handles.ALERTCONDITION_0)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.limitAlertConditionDescriptor(Handles.ALERTCONDITION_1, Handles.MDS_0), dataGenerator.limitAlertConditionState(Handles.ALERTCONDITION_1)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_0, Handles.ALERTCONDITION_0, AlertSignalManifestation.VIS), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_0)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_1, Handles.ALERTCONDITION_0, AlertSignalManifestation.AUD), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_1)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_2, Handles.ALERTCONDITION_1, AlertSignalManifestation.VIS), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_2)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_3, Handles.ALERTCONDITION_1, AlertSignalManifestation.AUD), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_3)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.vmdDescriptor(Handles.VMD_0), dataGenerator.vmdState(Handles.VMD_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSystemDescriptor(Handles.ALERTSYSTEM_1), dataGenerator.alertSystemState(Handles.ALERTSYSTEM_1)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertConditionDescriptor(Handles.ALERTCONDITION_2, Handles.MDS_0), dataGenerator.alertConditionState(Handles.ALERTCONDITION_2)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.limitAlertConditionDescriptor(Handles.ALERTCONDITION_3, Handles.MDS_0), dataGenerator.limitAlertConditionState(Handles.ALERTCONDITION_3)), Handles.ALERTSYSTEM_0)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_4, Handles.ALERTCONDITION_2, AlertSignalManifestation.VIS), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_4)), Handles.ALERTSYSTEM_1)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_5, Handles.ALERTCONDITION_2, AlertSignalManifestation.AUD), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_5)), Handles.ALERTSYSTEM_1)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_6, Handles.ALERTCONDITION_3, AlertSignalManifestation.VIS), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_6)), Handles.ALERTSYSTEM_1)
                .insert(Pair.tryFromThrowing(dataGenerator.alertSignalDescriptor(Handles.ALERTSIGNAL_7, Handles.ALERTCONDITION_3, AlertSignalManifestation.AUD), dataGenerator.alertSignalState(Handles.ALERTSIGNAL_7)), Handles.ALERTSYSTEM_1)
                .insert(Pair.tryFromThrowing(dataGenerator.channelDescriptor(Handles.CHANNEL_0), dataGenerator.channelState(Handles.CHANNEL_0)), Handles.VMD_0)
                .insert(Pair.tryFromThrowing(dataGenerator.numericMetricDescriptor(Handles.METRIC_0), dataGenerator.numericMetricState(Handles.METRIC_0)), Handles.CHANNEL_0)
                .insert(Pair.tryFromThrowing(dataGenerator.stringMetricDescriptor(Handles.METRIC_1), dataGenerator.stringMetricState(Handles.METRIC_1)), Handles.CHANNEL_0)
                .insert(Pair.tryFromThrowing(dataGenerator.enumStringMetricDescriptor(Handles.METRIC_2), dataGenerator.enumStringMetricState(Handles.METRIC_2)), Handles.CHANNEL_0)
                .insert(Pair.tryFromThrowing(dataGenerator.realTimeSampleArrayMetricDescriptor(Handles.METRIC_3), dataGenerator.realTimeSampleArrayMetricState(Handles.METRIC_3)), Handles.CHANNEL_0)
                .insert(Pair.tryFromThrowing(dataGenerator.distributionSampleArrayMetricDescriptor(Handles.METRIC_4), dataGenerator.distributionSampleArrayMetricState(Handles.METRIC_4)), Handles.CHANNEL_0)
                .insert(Pair.tryFromThrowing(dataGenerator.scoDescriptor(Handles.SCO_0), dataGenerator.scoState(Handles.SCO_0)), Handles.MDS_0)
                .insert(Pair.tryFromThrowing(dataGenerator.activateOperationDescriptor(Handles.OPERATION_0, Handles.MDS_0), dataGenerator.activateOperationState(Handles.OPERATION_0)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setStringOperationDescriptor(Handles.OPERATION_1, Handles.METRIC_1), dataGenerator.setStringOperationState(Handles.OPERATION_1)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setValueOperationDescriptor(Handles.OPERATION_2, Handles.METRIC_0), dataGenerator.setValueOperationState(Handles.OPERATION_2)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setComponentStateOperationDescriptor(Handles.OPERATION_3, Handles.MDS_0), dataGenerator.setComponentStateOperationState(Handles.OPERATION_3)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setMetricStateOperationDescriptor(Handles.OPERATION_4, Handles.METRIC_2), dataGenerator.setMetricStateOperationState(Handles.OPERATION_4)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setAlertStateOperationDescriptor(Handles.OPERATION_5, Handles.ALERTCONDITION_0), dataGenerator.setAlertStateOperationState(Handles.OPERATION_5)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.setContextStateOperationDescriptor(Handles.OPERATION_6, Handles.CONTEXTDESCRIPTOR_0), dataGenerator.setContextStateOperationState(Handles.OPERATION_6)), Handles.SCO_0)
                .insert(Pair.tryFromThrowing(dataGenerator.scoDescriptor(Handles.SCO_1), dataGenerator.scoState(Handles.SCO_1)), Handles.VMD_0)
                .insert(Pair.tryFromThrowing(dataGenerator.activateOperationDescriptor(Handles.OPERATION_7, Handles.VMD_0), dataGenerator.activateOperationState(Handles.OPERATION_7)), Handles.SCO_1)
                .insert(Pair.tryFromThrowing(dataGenerator.setStringOperationDescriptor(Handles.OPERATION_8, Handles.METRIC_1), dataGenerator.setStringOperationState(Handles.OPERATION_8)), Handles.SCO_1)
                .insert(Pair.tryFromThrowing(dataGenerator.setValueOperationDescriptor(Handles.OPERATION_9, Handles.METRIC_0), dataGenerator.setValueOperationState(Handles.OPERATION_9)), Handles.SCO_1)
                .insert(Pair.tryFromThrowing(dataGenerator.setComponentStateOperationDescriptor(Handles.OPERATION_10, Handles.VMD_0), dataGenerator.setComponentStateOperationState(Handles.OPERATION_10)), Handles.SCO_1)
                .insert(Pair.tryFromThrowing(dataGenerator.setMetricStateOperationDescriptor(Handles.OPERATION_11, Handles.METRIC_2), dataGenerator.setMetricStateOperationState(Handles.OPERATION_11)), Handles.SCO_1)
                .insert(Pair.tryFromThrowing(dataGenerator.setAlertStateOperationDescriptor(Handles.OPERATION_12, Handles.ALERTSIGNAL_7), dataGenerator.setAlertStateOperationState(Handles.OPERATION_12)), Handles.SCO_1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private <T extends AbstractDescriptor, V extends AbstractState> Pair entry(
        String handle,
        Class<T> descrClass
    ) throws Exception {
        return entry(handle, descrClass, t -> {
        }, t -> {
        });
    }

    private <T extends AbstractDescriptor, V extends AbstractState> Pair entry(String handle,
                                                                               Consumer<T> descrConsumer,
                                                                               Consumer<V> stateConsumer,
                                                                               Class<T> descrClass) throws Exception {
        return entry(handle, descrClass, descrConsumer, stateConsumer);
    }

    private <T extends AbstractDescriptor, V extends AbstractState> Pair entry(
        String handle,
        Class<T> descrClass,
        Consumer<T> descrConsumer,
        Consumer<V> stateConsumer
    ) throws Exception {
        return entryFactory.entry(handle, descrClass, descrConsumer, stateConsumer);
    }

    private <T extends AbstractDescriptor, V extends AbstractContextState> Pair contextEntry(
        String handle,
        String stateHandle,
        Class<T> descrClass,
        Class<V> stateClass
    ) throws Exception {
        return entryFactory.contextEntry(handle, stateHandle, descrClass, stateClass);
    }

    private <T extends AbstractDescriptor> T descriptor(String handle, Class<T> theClass) {
        return MockModelFactory.createDescriptor(handle, theClass);
    }

    private <T extends AbstractState> T state(String handle, Class<T> theClass) {
        return MockModelFactory.createState(handle, theClass);
    }
}
