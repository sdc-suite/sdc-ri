package org.somda.sdc.biceps.provider.preprocessing;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.MdibTreeValidator;
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment;
import org.somda.sdc.biceps.common.storage.MdibStorageRead;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.logging.InstanceLogger;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Preprocessing segment that ensures correctness of child-parent type relationships.
 */
public class TypeConsistencyChecker implements DescriptionPreprocessingSegment {
    private static final Logger LOG = LogManager.getLogger(TypeConsistencyChecker.class);
    private final MdibTreeValidator treeValidator;
    private final Logger instanceLogger;

    @Inject
    TypeConsistencyChecker(
            MdibTreeValidator treeValidator,
            @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier) {
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.treeValidator = treeValidator;
    }

    @Override
    public List<MdibDescriptionModification> process(
            List<MdibDescriptionModification> modifications,
            MdibStorageRead mdibStorage
    ) throws Exception {
        var errors = modifications.stream()
                .filter(it -> it instanceof MdibDescriptionModification.Insert)
                .map(it -> (MdibDescriptionModification.Insert) it)
                // filter mds
                .filter(it -> !(it.getPair().getDescriptor() instanceof MdsDescriptor && it.getParentHandle() == null))
                .filter(modification -> {
                    instanceLogger.trace(
                        "Finding parent descriptor {} for {}",
                        modification.getDescriptorHandle(), modification.getParentHandle()
                    );
                    AbstractDescriptor parentDescriptor = mdibStorage.getEntity(modification.getParentHandle())
                        .map(MdibEntity::getDescriptor).orElse(null);
                    // find parent in insertions
                    if (parentDescriptor == null) {
                        parentDescriptor = modifications.stream()
                            .filter(it -> it instanceof MdibDescriptionModification.Insert)
                            .map(it -> (MdibDescriptionModification.Insert) it)
                            .filter(it -> Objects.equals(it.getDescriptorHandle(), modification.getParentHandle()))
                            .findAny()
                                .map(it -> it.getPair().getDescriptor())
                            .orElse(null);
                    }
                    if (parentDescriptor == null) {
                        return true;
                    }

                    return !treeValidator.isValidParent(parentDescriptor, modification.getPair().getDescriptor());
                })
                .toList();

        if (!errors.isEmpty()) {
            throw new RuntimeException(String.format(
                "Children do not have a valid parent-child relationship, either no parent is present" +
                    " or an incorrect one. Handles: %s",
                errors.stream()
                    .map(it -> String.format("Handle %s - Parent %s", it.getDescriptorHandle(), it.getParent()))
                    .collect(Collectors.joining(", ")))
            );
        }
        return modifications;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
