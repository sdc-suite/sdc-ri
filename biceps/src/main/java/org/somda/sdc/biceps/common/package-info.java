/**
 * Any classes that are shared between BICEPS provider and consumer.
 */
@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.biceps.common;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;