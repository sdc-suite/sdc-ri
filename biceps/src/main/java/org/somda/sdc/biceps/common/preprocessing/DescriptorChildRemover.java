package org.somda.sdc.biceps.common.preprocessing;

import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.SingleStatePair;
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment;
import org.somda.sdc.biceps.common.storage.MdibStorageRead;
import org.somda.sdc.biceps.model.participant.AlertSystemDescriptor;
import org.somda.sdc.biceps.model.participant.ChannelDescriptor;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.ScoDescriptor;
import org.somda.sdc.biceps.model.participant.SystemContextDescriptor;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Removes children from descriptors in order to avoid redundant information in the MDIB storage.
 */
public class DescriptorChildRemover implements DescriptionPreprocessingSegment {

    @Override
    public List<MdibDescriptionModification> process(
        List<MdibDescriptionModification> allModifications,
        MdibStorageRead storage
    ) throws Exception {
        return allModifications.stream()
            .map(it -> {
                if (it instanceof MdibDescriptionModification.Insert insert) {
                    if (insert.getPair() instanceof Pair.SingleStatePair singleStatePair) {
                        // process
                        return new MdibDescriptionModification.Insert(
                                new Pair.SingleStatePair(processSingleState(singleStatePair.getPair())),
                                insert.getParentHandle()
                        );
                    }
                    // keep same
                    return insert;
                } else if (it instanceof MdibDescriptionModification.Update update) {
                    if (update.getPair() instanceof Pair.SingleStatePair singleStatePair) {
                        // process
                        return new MdibDescriptionModification.Update(
                                new Pair.SingleStatePair(processSingleState(singleStatePair.getPair()))
                        );
                    }
                    // keep same
                    return update;
                } else {
                    return it;
                }
            })
            .collect(Collectors.toList());
    }

    private SingleStatePair processSingleState(SingleStatePair singleStatePair) {
        if (singleStatePair instanceof SingleStatePair.Mds mds) {
            return new SingleStatePair.Mds(
                removeChildren(mds.getDescriptor()),
                mds.getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Vmd vmd) {
            return new SingleStatePair.Vmd(
                removeChildren(vmd.getDescriptor()),
                vmd.getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Channel channel) {
            return new SingleStatePair.Channel(
                removeChildren(channel.getDescriptor()),
                channel.getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.Sco sco) {
            return new SingleStatePair.Sco(
                removeChildren(sco.getDescriptor()),
                sco.getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.SystemContext systemContext) {
            return new SingleStatePair.SystemContext(
                removeChildren(systemContext.getDescriptor()),
                systemContext.getState()
            );
        } else if (singleStatePair instanceof SingleStatePair.AlertSystem alertSystem) {
            return new SingleStatePair.AlertSystem(
                removeChildren(alertSystem.getDescriptor()),
                alertSystem.getState()
            );
        } else {
            return singleStatePair;
        }
    }

    private AlertSystemDescriptor removeChildren(AlertSystemDescriptor descriptor) {
        descriptor.setAlertSignal(null);
        descriptor.setAlertCondition(null);
        return descriptor;
    }

    private SystemContextDescriptor removeChildren(SystemContextDescriptor descriptor) {
        descriptor.setLocationContext(null);
        descriptor.setPatientContext(null);
        descriptor.setMeansContext(null);
        descriptor.setOperatorContext(null);
        descriptor.setWorkflowContext(null);
        descriptor.setEnsembleContext(null);
        return descriptor;
    }

    private ScoDescriptor removeChildren(ScoDescriptor descriptor) {
        descriptor.setOperation(null);
        return descriptor;
    }

    private ChannelDescriptor removeChildren(ChannelDescriptor descriptor) {
        descriptor.setMetric(null);
        return descriptor;
    }

    private VmdDescriptor removeChildren(VmdDescriptor descriptor) {
        descriptor.setChannel(null);
        descriptor.setAlertSystem(null);
        return descriptor;
    }

    /**
     * Removes the children from a provided mds.
     * <p>
     * Removes the battery, clock, system context, vmd, alert system and sco
     * @param mds without the children
     */
    private MdsDescriptor removeChildren(MdsDescriptor mds) {
        mds.setBattery(null);
        mds.setClock(null);
        mds.setSystemContext(null);
        mds.setVmd(null);
        mds.setAlertSystem(null);
        mds.setSco(null);
        return mds;
    }
}
