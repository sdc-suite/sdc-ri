package org.somda.sdc.biceps.provider.preprocessing

import com.google.inject.Inject
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.Pair
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead

/**
 * Preprocessing segment that checks for handle duplicates on inserted and updated entities during description
 * modifications.
 */
class DuplicateChecker @Inject internal constructor() : DescriptionPreprocessingSegment {
    @Throws(Exception::class)
    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        storage: MdibStorageRead
    ): List<MdibDescriptionModification> {
        val allHandles = modifications
            .mapNotNull {
                when (it) {
                    is MdibDescriptionModification.Insert -> it.pair
                    is MdibDescriptionModification.Update -> it.pair
                    else -> null
                }
            }
            .flatMap {
                when (it) {
                    is Pair.MultiStatePair ->
                        listOf(it.handle) + it.pair.abstractContextStates.map { state -> state.handle }
                    else -> listOf(it.handle)
                }

            }
            .toMutableList()

        val distinctHandles = allHandles.toSet()

        if (allHandles.size != distinctHandles.size) {
            for (handle in distinctHandles) {
                allHandles.remove(handle)
            }
            throw HandleDuplicatedException("Inserted handles are duplicates: ${allHandles.joinToString()}")
        }
        return modifications
    }

    override fun toString(): String {
        return javaClass.simpleName
    }
}
