package org.somda.sdc.biceps.provider.preprocessing

import com.google.inject.Inject
import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead
import org.somda.sdc.biceps.common.storage.StatePreprocessingSegment

/**
 * Preprocessing element that forbids writing empty changes into the mdib, which would in turn cause empty reports.
 */
class EmptyWriteChecker @Inject constructor() : DescriptionPreprocessingSegment, StatePreprocessingSegment {

    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {
        return modifications
    }

    override fun process(
        modifications: MdibStateModifications,
        storage: MdibStorageRead
    ): MdibStateModifications {
        return modifications
    }


    override fun afterLastModification(
        modifications: MdibStateModifications,
        mdibStorage: MdibStorageRead
    ): MdibStateModifications {
        require(!modifications.isEmpty) { "Empty writes are prohibited" }
        return modifications
    }

    override fun afterLastModification(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {
        require(modifications.isNotEmpty()) { "Empty writes are prohibited" }
        return modifications
    }
}
