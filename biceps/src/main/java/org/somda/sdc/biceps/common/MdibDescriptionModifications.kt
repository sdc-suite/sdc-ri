package org.somda.sdc.biceps.common

import org.jvnet.jaxb.lang.CopyStrategy
import org.jvnet.jaxb.lang.CopyTo
import org.jvnet.jaxb.locator.ObjectLocator
import org.somda.sdc.biceps.model.participant.AbstractContextState

/**
 * Container for a set of description modifications to be written to the MDIB.
 */
@Suppress("TooManyFunctions") // amount of functions is necessary
class MdibDescriptionModifications: CopyTo {
    private val modifications = mutableListOf<MdibDescriptionModification>()
    private val usedHandles = mutableSetOf<String>()

    /**
     * Adds a new [MdibDescriptionModification.Insert] to the change set.
     *
     * @param pair modification to insert
     * @param parent parent of the modification, null if [pair] is an MDS
     * @throws IllegalArgumentException if a duplicate is detected in the change set, e.g. deleting and inserting
     * the same descriptor
     */
    fun insert(pair: Pair, parent: String?) = apply {
        add(MdibDescriptionModification.Insert(pair, parent))
    }

    /**
     * Adds a new [MdibDescriptionModification.Update] to the change set.
     *
     * @param pair modification to insert
     * @throws IllegalArgumentException if a duplicate is detected in the change set, e.g. deleting and inserting
     * the same descriptor
     */
    fun update(pair: Pair) = apply {
        add(MdibDescriptionModification.Update(pair))
    }

    /**
     * Adds a new [MdibDescriptionModification.Delete] to the change set.
     *
     * @param handle descriptor handle of the entity to delete
     * @throws IllegalArgumentException if a duplicate is detected in the change set, e.g. deleting and inserting
     * the same descriptor
     */
    fun delete(handle: String) = apply {
        add(MdibDescriptionModification.Delete(handle))
    }

    /**
     * Adds a new description modification to the set of modifications.
     *
     * @throws IllegalArgumentException if a duplicate is detected in the change set, e.g. deleting and inserting
     * the same descriptor
     */
    fun add(modification: MdibDescriptionModification) = apply {
        duplicateDetection(modification)
        modifications.add(modification)
    }

    /**
     * Adds all new description modification to the set of modifications.
     *
     * @throws IllegalArgumentException if a duplicate is detected in the change set, e.g. deleting and inserting
     * the same descriptor
     */
    fun addAll(modifications: List<MdibDescriptionModification>): MdibDescriptionModifications {
        // check for input data collisions
        val descriptorHandles = modifications
            .map { it.descriptorHandle }
            .toList()
        val multiStateHandles = modifications
            .mapNotNull { extractMultiStates(it) }
            .flatMap { it.map { s -> s.handle } }
            .toList()
        val handles = descriptorHandles + multiStateHandles

        val uniqueHandles = handles
            .toMutableSet()

        if (handles.size != uniqueHandles.size) {
            val duplicates = handles
                .filter { !uniqueHandles.remove(it) }
                .toList()
            throw IllegalArgumentException(
                "Change set contains duplicate descriptor or multi-state handles." +
                    " Duplicates: ${duplicates.joinToString(", ")}"
            )
        }

        // check for present data collisions
        val collisions = modifications
            .filter { isDuplicated(it) }
            .toList()

        require(collisions.isEmpty()) {
            "Change set contains descriptor or multi-state handles colliding with already added handles." +
                " Collisions: ${collisions.joinToString(", ")}"
        }

        modifications.forEach { pushModification(it) }

        return this
    }

    private fun isDuplicated(modification: MdibDescriptionModification): Boolean {
        return usedHandles.contains(modification.descriptorHandle)
                && (extractMultiStates(modification)?.let { it.any { s -> usedHandles.contains(s.handle) } } ?: false)
    }

    private fun pushModification(modification: MdibDescriptionModification) {
        usedHandles.add(modification.descriptorHandle)
        extractMultiStates(modification)?.let {states ->
            states.forEach { usedHandles.add(it.handle) }
        }
        modifications.add(modification)
    }

    private fun extractMultiStates(modification: MdibDescriptionModification): List<AbstractContextState>? {
        val pair = when (modification) {
            is MdibDescriptionModification.Insert -> modification.pair
            is MdibDescriptionModification.Update -> modification.pair
            else -> return null
        }

        return when (pair) {
            is Pair.MultiStatePair -> pair.pair.abstractContextStates
            else -> null
        }
    }

    private fun duplicateDetection(modification: MdibDescriptionModification) {
        modification.descriptorHandle.also {
            require(!usedHandles.contains(it)) {
                "Handle '$it' has already been inserted into description change set"
            }
            usedHandles.add(it)
        }

        // context state handles check
        extractMultiStates(modification)?.let {
            it.forEach { state ->
                require(!usedHandles.contains(state.handle)) {
                    "MultiState handle '${state.handle}' has already been inserted into description change set"
                }
            }
        }
    }

    fun asList(): List<MdibDescriptionModification> = modifications

    override fun createNewInstance(): Any {
        return MdibDescriptionModifications()
    }

    override fun copyTo(p0: Any?): Any {
        return MdibDescriptionModifications()
            .also { it.addAll(this.modifications) }
    }

    override fun copyTo(p0: ObjectLocator?, p1: Any?, p2: CopyStrategy?): Any {
        return MdibDescriptionModifications()
            .also { it.addAll(this.modifications) }
    }
}
