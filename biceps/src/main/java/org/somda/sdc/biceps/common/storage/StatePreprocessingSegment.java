package org.somda.sdc.biceps.common.storage;

import org.somda.sdc.biceps.common.MdibStateModifications;

/**
 * A segment that is applied during state modifications.
 */
public interface StatePreprocessingSegment {
    /**
     * Function that is invoked before the first modification in the processing chain is applied.
     * <p>
     * Default behavior is <em>do nothing</em>.
     *
     * @param modifications all modifications for preprocessing.
     * @param mdibStorage   the MDIB storage to be used by the callback.
     * @return pre-processed state modification
     */
    default MdibStateModifications beforeFirstModification(
        MdibStateModifications modifications,
        MdibStorageRead mdibStorage
    ) {
        return modifications;
    }

    /**
     * Function that is invoked after the last modification in the processing chain has been applied.
     * <p>
     * Default behavior is <em>do nothing</em>.
     *
     * @param modifications all modifications for preprocessing.
     * @param mdibStorage   the MDIB storage to be used by the callback.
     * @return post-processed state modification
     */
    default MdibStateModifications afterLastModification(
        MdibStateModifications modifications,
        MdibStorageRead mdibStorage
    ) {
        return modifications;
    }

    /**
     * In a sequence of modifications this function processes one modification.
     *
     * @param modifications all modifications that are being processed.
     * @param storage       the MDIB storage for access.
     * @throws Exception an arbitrary exception if something goes wrong.
     * @return processed state modification
     */
    MdibStateModifications process(MdibStateModifications modifications, MdibStorageRead storage)
            throws Exception;
}
