package org.somda.sdc.biceps.common.storage;

import org.jspecify.annotations.Nullable;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.access.WriteDescriptionResult;
import org.somda.sdc.biceps.common.access.WriteStateResult;
import org.somda.sdc.biceps.model.participant.MdibVersion;

import java.math.BigInteger;

/**
 * Registry-based read-write access to {@linkplain MdibEntity} instances derived
 * from an {@linkplain org.somda.sdc.biceps.model.participant.Mdib}.
 * <p>
 * In this case "registry-based" means that there is fast (hash map) access to any {@link MdibEntity} instance if
 * not mentioned otherwise.
 */
public interface MdibStorage extends MdibStorageRead {
    /**
     * Applies description modifications on this object regardless of any consistency checks.
     * <p>
     * Versions are applied without being verified.
     *
     * @param mdibVersion              the MDIB version to apply.
     * @param mdDescriptionVersion     the MD description version to apply. Value null leaves version as is.
     * @param mdStateVersion           the MD state version to apply. Value null leaves version as is.
     * @param descriptionModifications the modifications to apply.
     * @return a result set with inserted, updated and deleted entities.
     */
    WriteDescriptionResult apply(MdibVersion mdibVersion,
                                 @Nullable BigInteger mdDescriptionVersion,
                                 @Nullable BigInteger mdStateVersion,
                                 MdibDescriptionModifications descriptionModifications);

    /**
     * Applies state modifications on this object regardless of any consistency checks.
     * <p>
     * Versions are applied without being verified.
     *
     * @param mdibVersion        the MDIB version to apply.
     * @param mdStateVersion     the MD state version to apply. Value null leaves version as is.
     * @param stateModifications the modifications to apply.
     * @return a result set with updated states.
     */
    WriteStateResult apply(MdibVersion mdibVersion,
                           @Nullable BigInteger mdStateVersion,
                           MdibStateModifications stateModifications);

}
