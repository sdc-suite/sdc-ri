package org.somda.sdc.biceps.common.storage;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.inject.name.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jspecify.annotations.Nullable;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibEntity;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.MdibTypeValidator;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.SingleStatePair;
import org.somda.sdc.biceps.common.WrittenMdibStateModifications;
import org.somda.sdc.biceps.common.access.WriteDescriptionResult;
import org.somda.sdc.biceps.common.access.WriteStateResult;
import org.somda.sdc.biceps.common.MdibEntityFactory;
import org.somda.sdc.biceps.common.storage.helper.MdibStorageUtil;
import org.somda.sdc.biceps.model.participant.AbstractContextDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractMultiState;
import org.somda.sdc.biceps.model.participant.AbstractState;
import org.somda.sdc.biceps.model.participant.ContextAssociation;
import org.somda.sdc.biceps.model.participant.MdibVersion;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.logging.InstanceLogger;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Default implementation of {@linkplain MdibStorage}.
 */
public class MdibStorageImpl implements MdibStorage {
    private static final Logger LOG = LogManager.getLogger(MdibStorageImpl.class);

    private final MdibEntityFactory entityFactory;
    private final MdibStorageUtil util;
    private final MdibTypeValidator typeValidator;
    private final Logger instanceLogger;
    private final Boolean storeNotAssociatedContextStates;

    private MdibVersion mdibVersion;
    private BigInteger mdDescriptionVersion;
    private BigInteger mdStateVersion;

    private final Map<String, MdibEntity> entities;
    private final ArrayList<String> rootEntities;
    private final Map<String, AbstractContextState> contextStates;


    @AssistedInject
    MdibStorageImpl(MdibEntityFactory entityFactory,
                    MdibStorageUtil util,
                    MdibTypeValidator typeValidator,
                    @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier,
                    @Named(org.somda.sdc.biceps.common.CommonConfig.STORE_NOT_ASSOCIATED_CONTEXT_STATES)
                            Boolean storeNotAssociatedContextStates) {
        this(
                MdibVersion.create(), BigInteger.valueOf(-1),
                BigInteger.valueOf(-1), entityFactory, util,
                typeValidator, frameworkIdentifier,
                storeNotAssociatedContextStates
        );
    }

    @AssistedInject
    MdibStorageImpl(@Assisted MdibVersion initialMdibVersion,
                    MdibEntityFactory entityFactory,
                    MdibStorageUtil util,
                    MdibTypeValidator typeValidator,
                    @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier,
                    @Named(org.somda.sdc.biceps.common.CommonConfig.STORE_NOT_ASSOCIATED_CONTEXT_STATES)
                            Boolean storeNotAssociatedContextStates) {
        this(
                initialMdibVersion, BigInteger.valueOf(-1),
                BigInteger.valueOf(-1), entityFactory, util,
                typeValidator, frameworkIdentifier,
                storeNotAssociatedContextStates
        );
    }

    @AssistedInject
    MdibStorageImpl(@Assisted MdibVersion initialMdibVersion,
                    @Assisted("mdDescriptionVersion") BigInteger mdDescriptionVersion,
                    @Assisted("mdStateVersion") BigInteger mdStateVersion,
                    MdibEntityFactory entityFactory,
                    MdibStorageUtil util,
                    MdibTypeValidator typeValidator,
                    @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier,
                    @Named(org.somda.sdc.biceps.common.CommonConfig.STORE_NOT_ASSOCIATED_CONTEXT_STATES)
                            Boolean storeNotAssociatedContextStates) {
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.mdibVersion = initialMdibVersion;
        this.mdDescriptionVersion = mdDescriptionVersion;
        this.mdStateVersion = mdStateVersion;
        this.entityFactory = entityFactory;
        this.util = util;
        this.typeValidator = typeValidator;
        this.storeNotAssociatedContextStates = storeNotAssociatedContextStates;

        this.entities = new HashMap<>();
        this.rootEntities = new ArrayList<>();
        this.contextStates = new HashMap<>();
    }

    public <T extends AbstractDescriptor> Optional<T> getDescriptor(String handle, Class<T> descrClass) {
        final MdibEntity mdibEntity = entities.get(handle);
        if (mdibEntity != null) {
            return util.exposeInstance(mdibEntity.getDescriptor(), descrClass);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AbstractDescriptor> getDescriptor(String handle) {
        return getDescriptor(handle, AbstractDescriptor.class);
    }

    public Optional<MdibEntity> getEntity(String handle) {
        return Optional.ofNullable(entities.get(handle));
    }

    @Override
    public <T extends AbstractDescriptor> Collection<MdibEntity> findEntitiesByType(Class<T> type) {
        List<MdibEntity> result = new ArrayList<>();
        for (MdibEntity entity : entities.values()) {
            if (type.isAssignableFrom(entity.getDescriptor().getClass())) {
                result.add(entity);
            }
        }
        return result;
    }

    @Override
    public <T extends AbstractDescriptor> List<MdibEntity> getChildrenByType(String handle, Class<T> type) {
        final List<MdibEntity> result = new ArrayList<>();
        final Optional<MdibEntity> entity = getEntity(handle);
        if (entity.isEmpty()) {
            return result;
        }

        for (String child : entity.get().getChildren()) {
            getEntity(child).ifPresent(childEntity ->
            {
                if (type.isAssignableFrom(childEntity.getDescriptor().getClass())) {
                    result.add(childEntity);
                }
            });
        }

        return result;
    }

    public List<MdibEntity> getRootEntities() {
        return util.exposeEntityList(entities, rootEntities);
    }

    public Optional<AbstractState> getState(String handle) {
        return getState(handle, AbstractState.class);
    }

    public <T extends AbstractState> Optional<T> getState(String handle, Class<T> stateClass) {
        final MdibEntity entity = entities.get(handle);
        if (entity != null && entity.getStates().size() == 1) {
            return util.exposeInstance(entity.getStates().get(0), stateClass);
        }
        final AbstractContextState contextState = contextStates.get(handle);
        return util.exposeInstance(contextState, stateClass);
    }

    @Override
    public <T extends AbstractState> List<T> getStatesByType(Class<T> stateClass) {
        var result = new ArrayList<T>();
        for (MdibEntity entity : entities.values()) {
            entity.getStates().forEach(state -> {
                if (stateClass.isAssignableFrom(state.getClass())) {
                    result.add(stateClass.cast(state));
                }
            });
        }
        return result;
    }

    @Override
    public <T extends AbstractContextState> List<T> getContextStates(String descriptorHandle, Class<T> stateClass) {
        final MdibEntity entity = entities.get(descriptorHandle);
        if (entity == null || entity.getStates().isEmpty()) {
            return Collections.emptyList();
        }
        return util.exposeListOfType(entity.getStates(), stateClass);
    }

    @Override
    public List<AbstractContextState> getContextStates(String descriptorHandle) {
        final MdibEntity entity = entities.get(descriptorHandle);
        if (entity == null || entity.getStates().isEmpty()) {
            return Collections.emptyList();
        }
        return util.exposeListOfType(entity.getStates(), AbstractContextState.class);
    }

    @Override
    public List<AbstractMultiState> getMultiStates(String descriptorHandle) {
        final MdibEntity entity = entities.get(descriptorHandle);
        if (entity == null || entity.getStates().isEmpty()) {
            return Collections.emptyList();
        }
        return util.exposeListOfType(entity.getStates(), AbstractMultiState.class);
    }

    @Override
    public List<AbstractContextState> getContextStates() {
        return new ArrayList<>(contextStates.values());
    }

    @Override
    public <T extends AbstractContextState> List<T> findContextStatesByType(Class<T> stateClass) {
        var result = new ArrayList<T>();
        contextStates.forEach((handle, state) -> {
            if (stateClass.isAssignableFrom(state.getClass())) {
                result.add(stateClass.cast(state));
            }
        });
        return result;
    }

    @Override
    public WriteDescriptionResult apply(MdibVersion mdibVersion,
                                        @Nullable BigInteger mdDescriptionVersion,
                                        @Nullable BigInteger mdStateVersion,
                                        MdibDescriptionModifications descriptionModifications) {
        this.mdibVersion = mdibVersion;
        // MdDescription/@DescriptionVersion: The implied value SHALL be "0".
        this.mdDescriptionVersion = mdDescriptionVersion != null ? mdDescriptionVersion : BigInteger.ZERO;
        // MdState/@StateVersion: The implied value SHALL be "0".
        this.mdStateVersion = mdStateVersion != null ? mdStateVersion : BigInteger.ZERO;

        final List<MdibEntity> insertedEntities = new ArrayList<>();
        final List<MdibEntity> updatedEntities = new ArrayList<>();
        final List<MdibEntity> deletedEntities = new ArrayList<>();

        final Map<String, String> parentMdsCache = new HashMap<>();

        for (var modification : descriptionModifications.asList()) {
            var sanitizedStates = removeNotAssociatedContextStates(modification);
            if (modification instanceof MdibDescriptionModification.Insert insert) {
                insertEntity(
                    insert,
                    sanitizedStates,
                    insertedEntities,
                    parentMdsCache
                );
            } else if (modification instanceof MdibDescriptionModification.Update update) {
                updateEntity(update, sanitizedStates, updatedEntities);
            } else if (modification instanceof MdibDescriptionModification.Delete delete) {
                deleteEntity(delete, deletedEntities);
            } else {
                instanceLogger.warn(
                    "Unknown modification type detected. Skip entry while description modification processing."
                );
            }
        }

        return new WriteDescriptionResult(mdibVersion, insertedEntities, updatedEntities, deletedEntities);
    }

    private List<AbstractState> handleRemove(Pair.MultiStatePair pair) {
        if (storeNotAssociatedContextStates) {
            // also return a copy in this case to have the same behavior afterward
            // we also need to cast through an intermediate wildcard type, which is kinda insane
            //noinspection unchecked
            return (List<AbstractState>) (List<?>) pair.getPair().getAbstractContextStates();
        }

        var result = new ArrayList<>(pair.getPair().getAbstractContextStates());
        result.removeIf(this::isNotAssociated);
        //noinspection unchecked
        return (List<AbstractState>) (List<?>) result;
    }

    private List<AbstractState> removeNotAssociatedContextStates(MdibDescriptionModification modification) {

        if (modification instanceof MdibDescriptionModification.Insert insert) {
            if (insert.getPair() instanceof Pair.SingleStatePair) {
                return insert.getPair().getStates();
            } else {
                Pair.MultiStatePair pair = (Pair.MultiStatePair) insert.getPair();
                return handleRemove(pair);
            }
        } else if (modification instanceof MdibDescriptionModification.Update update) {
            if (update.getPair() instanceof Pair.SingleStatePair) {
                return update.getPair().getStates();
            } else {
                Pair.MultiStatePair pair = (Pair.MultiStatePair) update.getPair();
                return handleRemove(pair);
            }
        } else {
            // deletes have no states
            return Collections.emptyList();
        }
    }

    private void deleteEntity(MdibDescriptionModification.Delete modification,
                              List<MdibEntity> deletedEntities) {
        Optional.ofNullable(entities.get(modification.getHandle())).ifPresent(mdibEntity -> {
            instanceLogger.debug(
                    "[{}] Delete entity: {}",
                    mdibVersion.getSequenceId(), modification.getHandle()
            );
            mdibEntity.getParent().flatMap(parentHandle ->
                    Optional.ofNullable(entities.get(parentHandle))).ifPresent(parentEntity -> {
                var updatedParent = entityFactory.replaceChildren(parentEntity,
                        parentEntity.getChildren().stream()
                                // filter out the removed entity only
                                .filter(s -> !s.equals(mdibEntity.getHandle()))
                                .collect(Collectors.toList()));
                entities.put(updatedParent.getHandle(), updatedParent);
            });
        });

        final var deletedEntity = entities.get(modification.getHandle());
        if (deletedEntity == null) {
            instanceLogger.warn(
                    "Possible inconsistency detected. Entity to delete was not found: {}",
                    modification.getHandle()
            );
            return;
        }

        rootEntities.remove(modification.getHandle());
        entities.remove(modification.getHandle());
        contextStates.entrySet().removeIf(state ->
                state.getValue().getDescriptorHandle().equals(modification.getHandle()));

        deletedEntities.add(deletedEntity);
    }

    private void updateEntity(MdibDescriptionModification.Update modification,
                              List<AbstractState> sanitizedStates,
                              List<MdibEntity> updatedEntities
    ) {
        Optional.ofNullable(entities.get(modification.getDescriptorHandle())).ifPresent(mdibEntity -> {
            instanceLogger.debug(
                "[{}] Update entity: {}",
                mdibVersion.getSequenceId(), modification.getPair().getDescriptor()
            );

            entities.put(mdibEntity.getHandle(), entityFactory.replaceDescriptorAndStates(
                    mdibEntity,
                    modification.getPair().getDescriptor(),
                    sanitizedStates));

            updatedEntities.add(entityFactory.replaceDescriptorAndStates(
                    mdibEntity,
                    modification.getPair().getDescriptor(),
                    modification.getPair().getStates()));

            updateContextStatesMap(modification.getPair().getStates());
        });
    }

    private void updateContextStatesMap(List<AbstractState> states) {
        for (AbstractState state : states) {
            var contextState = typeValidator.toContextState(state);
            if (contextState.isEmpty()) {
                continue;
            }

            if (!storeNotAssociatedContextStates && getNotAssociatedContextState(state).isPresent()) {
                contextStates.remove(contextState.get().getHandle());
            } else {
                contextStates.put(contextState.get().getHandle(), contextState.get());
            }
        }
    }

    private String findParentMds(
        List<MdibEntity> insertedEntities,
        String parentHandle,
        String newEntityHandle
    ) {
        // find parent entity in inserted
        var parentEntity = insertedEntities.stream()
            .filter(it -> parentHandle.equals(it.getHandle()))
            .findFirst();

        if (parentEntity.isEmpty()) {
            parentEntity = Optional.ofNullable(this.entities.get(parentHandle));
        }

        var parent = parentEntity.orElseThrow(
            () -> new RuntimeException(String.format(
                "Could not find parent entity for %s, %s was missing", newEntityHandle, parentHandle
            ))
        );

        if (parent.getDescriptor() instanceof MdsDescriptor) {
            return parent.getHandle();
        } else {
            var grandParent = parent.getParent().orElseThrow(
                () -> new RuntimeException(String.format("No mandatory parent found for %s", parentHandle))
            );
            return findParentMds(insertedEntities, grandParent, newEntityHandle);
        }
    }

    private void insertEntity(MdibDescriptionModification.Insert modification,
                              List<AbstractState> sanitizedStates,
                              List<MdibEntity> insertedEntities,
                              Map<String, String> parentMdsHandleCache) {
        // find parent MDS for entity
        String parentMdsHandle;
        var newEntityHandle = modification.getPair().getHandle();
        if (
            modification.getPair() instanceof Pair.SingleStatePair
                && ((Pair.SingleStatePair) modification.getPair()).getPair() instanceof SingleStatePair.Mds
        ) {
            parentMdsHandle = newEntityHandle;
        } else {
            var parentHandle = Optional.ofNullable(modification.getParentHandle()).orElseThrow(
                    () -> new RuntimeException(
                            String.format("Mandatory parent handle missing for %s", newEntityHandle)
                    )
            );
            parentMdsHandle = parentMdsHandleCache.get(parentHandle);
            if (parentMdsHandle == null) {
                parentMdsHandle = findParentMds(
                        insertedEntities,
                        parentHandle,
                        newEntityHandle
                );
            }
        }
        parentMdsHandleCache.put(newEntityHandle, parentMdsHandle);


        var mdibEntityForStorage = entityFactory.createMdibEntity(
            modification.getParentHandle(),
            new ArrayList<>(),
            modification.getPair().getDescriptor(),
            sanitizedStates,
            mdibVersion,
            parentMdsHandle
        );

        var mdibEntityForResultSet = entityFactory.createMdibEntity(
            modification.getParentHandle(),
            new ArrayList<>(),
            modification.getPair().getDescriptor(),
            modification.getPair().getStates(),
            mdibVersion,
            parentMdsHandle
        );

        // Either add entity as child of a parent or expect it to be a root entity
        if (modification.getParentHandle() != null) {
            Optional.ofNullable(entities.get(modification.getParentHandle())).ifPresent(parentEntity -> {
                var children = new ArrayList<>(parentEntity.getChildren());
                children.add(mdibEntityForStorage.getHandle());

                entities.put(parentEntity.getHandle(),
                        entityFactory.replaceChildren(parentEntity, Collections.unmodifiableList(children)));
            });
        } else {
            rootEntities.add(mdibEntityForStorage.getHandle());
        }

        // Add to entities list
        entities.put(mdibEntityForStorage.getHandle(), mdibEntityForStorage);

        instanceLogger.debug(
                "[{}] Insert entity: {}",
                mdibVersion.getInstanceId(), mdibEntityForStorage.getDescriptor()
        );

        // Add to context states if context entity
        if (mdibEntityForStorage.getDescriptor() instanceof AbstractContextDescriptor) {
            contextStates.putAll(mdibEntityForStorage.getStates().stream()
                    .map(AbstractContextState.class::cast)
                    .collect(Collectors.toMap(AbstractMultiState::getHandle, state -> state)));
        }

        insertedEntities.add(mdibEntityForResultSet);
    }

    private <T extends AbstractState> Map<String, List<T>> applySingleState(List<T> states) {
        final Map<String, List<T>> modifiedStates = new HashMap<>();
        for (T state : states) {
            if (instanceLogger.isDebugEnabled()) {
                instanceLogger.debug("[{}] Update single state: {}", mdibVersion.getSequenceId(), state);
            }
            final MdibEntity mdibEntity = entities.get(state.getDescriptorHandle());
            if (mdibEntity == null) {
                throw new RuntimeException(String.format(
                    "Unknown descriptor %s in state update", state.getDescriptorHandle()
                ));
            }
            modifiedStates.computeIfAbsent(mdibEntity.getParentMds(), s -> new ArrayList<>()).add(state);

            entities.put(mdibEntity.getHandle(),
                    entityFactory.replaceStates(mdibEntity, List.of(state)));
        }
        return modifiedStates;
    }

    private <T extends AbstractMultiState> Map<String, List<T>> applyMultiState(List<T> states) {
        final Map<String, List<T>> modifiedStates = new HashMap<>();
        for (T state : states) {
            if (instanceLogger.isDebugEnabled()) {
                instanceLogger.debug("[{}] Update multi state: {}", mdibVersion.getSequenceId(), state);
            }
            final MdibEntity mdibEntity = entities.get(state.getDescriptorHandle());
            if (mdibEntity == null) {
                throw new RuntimeException(String.format(
                    "Unknown descriptor %s in state update", state.getDescriptorHandle()
                ));
            }

            final var mdibEntityStates = mdibEntity.getStates()
                .stream()
                .map(s -> (AbstractMultiState) s)
                .collect(Collectors.toList());

            var doStore = storeNotAssociatedContextStates || getNotAssociatedContextState(state).isEmpty();

            var existingState = mdibEntityStates.stream()
                .filter(it -> state.getHandle().equals(it.getHandle()))
                .findFirst();
            var existingIndex = existingState.map(mdibEntityStates::indexOf);

            if (doStore) {
                if (instanceLogger.isDebugEnabled()) {
                    if (existingIndex.isPresent()) {
                        instanceLogger.debug("Updating existing multi-state {}", state.getHandle());
                    } else {
                        instanceLogger.debug("Adding new multi-state {}", state.getHandle());
                    }
                }
                modifiedStates.computeIfAbsent(mdibEntity.getParentMds(), s -> new ArrayList<>()).add(state);
                if (existingIndex.isPresent()) {
                    mdibEntityStates.set(existingIndex.orElseThrow(), state);
                } else {
                    mdibEntityStates.add(state);
                }
            } else {
                instanceLogger.debug(
                    "Not storing not associated context state {} for descriptor {}",
                    state.getHandle(), state.getDescriptorHandle()
                );
                if (existingState.isPresent()) {
                    // remove state
                    mdibEntityStates.remove(existingState.orElseThrow());
                }
                modifiedStates.computeIfAbsent(mdibEntity.getParentMds(), s -> new ArrayList<>()).add(state);
            }

            // update context map
            mdibEntity.getStates(AbstractMultiState.class)
                    .forEach(s -> {
                        contextStates.remove(s.getHandle());
                        entities.remove(s.getHandle());
                    });

            var updatedEntity = entityFactory.replaceStates(mdibEntity,
                Collections.unmodifiableList(mdibEntityStates));
            entities.put(mdibEntity.getHandle(), updatedEntity);

            mdibEntityStates.forEach(s -> contextStates.put(s.getHandle(), (AbstractContextState) s));
        }

        return modifiedStates;
    }

    @Override
    public WriteStateResult apply(MdibVersion mdibVersion,
                                  @Nullable BigInteger mdStateVersion,
                                  MdibStateModifications stateModifications) throws RuntimeException {
        this.mdibVersion = mdibVersion;
        Optional.ofNullable(mdStateVersion).ifPresent(version -> this.mdStateVersion = version);

        WrittenMdibStateModifications states;
        if (stateModifications instanceof MdibStateModifications.Context contextStateModifications) {
            states = new WrittenMdibStateModifications.Context(
                applyMultiState(contextStateModifications.getContextStates())
            );
        } else if (stateModifications instanceof MdibStateModifications.Alert alertStateModifications) {
            states = new WrittenMdibStateModifications.Alert(
                applySingleState(alertStateModifications.getAlertStates())
            );
        } else if (stateModifications instanceof MdibStateModifications.Component componentStateModifications) {
            states = new WrittenMdibStateModifications.Component(
                applySingleState(componentStateModifications.getComponentStates())
            );
        } else if (stateModifications instanceof MdibStateModifications.Metric metricStateModifications) {
            states = new WrittenMdibStateModifications.Metric(
                applySingleState(metricStateModifications.getMetricStates())
            );
        } else if (stateModifications instanceof MdibStateModifications.Operation operationStateModifications) {
            states = new WrittenMdibStateModifications.Operation(
                applySingleState(operationStateModifications.getOperationStates())
            );
        } else if (stateModifications instanceof MdibStateModifications.Waveform waveformStateModifications) {
            states = new WrittenMdibStateModifications.Waveform(
                applySingleState(waveformStateModifications.getWaveformStates())
            );
        } else {
            throw new RuntimeException(String.format(
                "Unexpected modification kind %s", stateModifications.getClass().getName()
            ));
        }

        return new WriteStateResult(mdibVersion, states);
    }

    @Override
    public MdibVersion getMdibVersion() {
        return mdibVersion;
    }

    @Override
    public BigInteger getMdDescriptionVersion() {
        return mdDescriptionVersion;
    }

    @Override
    public BigInteger getMdStateVersion() {
        return mdStateVersion;
    }

    private boolean isNotAssociated(AbstractContextState state) {
        return state.getContextAssociation() == null || ContextAssociation.NO == state.getContextAssociation();
    }

    private Optional<AbstractContextState> getNotAssociatedContextState(AbstractState state) {
        if (state instanceof AbstractContextState contextState && isNotAssociated(contextState)) {
            return Optional.of(contextState);
        }
        return Optional.empty();
    }
}
