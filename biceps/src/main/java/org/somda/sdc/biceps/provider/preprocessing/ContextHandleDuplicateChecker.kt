package org.somda.sdc.biceps.provider.preprocessing

import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.Pair
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead
import org.somda.sdc.biceps.common.storage.StatePreprocessingSegment

/**
 * Checker for collisions of context state handles with already present descriptor and context handles.
 */
class ContextHandleDuplicateChecker: DescriptionPreprocessingSegment, StatePreprocessingSegment {

    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {
        val collisions = modifications
            .mapNotNull {
                when (it) {
                    is MdibDescriptionModification.Insert -> handleInsert(it, mdibStorage)
                    is MdibDescriptionModification.Update -> handleUpdate(it, mdibStorage)
                    else -> null
                }
            }
            .flatten()

        if (collisions.isNotEmpty()) {
            throw HandleDuplicatedException("Descriptor change included colliding handles ${collisions.joinToString()}")
        }

        return modifications
    }

    /**
     * Check for collisions of both descriptor and all states
     */
    @Suppress("ReturnCount") // early exit requires extra return
    private fun handleInsert(insert: MdibDescriptionModification.Insert, mdibStorage: MdibStorageRead): List<String>? {
        val msp = when (insert.pair) {
            is Pair.MultiStatePair -> insert.pair.pair
            else -> return null
        }

        val descriptorCollision = isCollidingHandle(msp.handle, mdibStorage)
        val stateCollision = msp.abstractContextStates
            .filter { isCollidingHandle(it.handle, mdibStorage) }
            .map { it.handle }

        if (descriptorCollision || stateCollision.isNotEmpty()) {
            val collisions = when (descriptorCollision) {
                true -> listOf(msp.handle) + stateCollision
                false -> stateCollision
            }
            return collisions
        }
        return null
    }

    /**
     * Only check for collisions of context handles with present descriptors, updates likely contain already present
     * context state handles.
     */
    @Suppress("ReturnCount") // early exit requires extra return
    private fun handleUpdate(update: MdibDescriptionModification.Update, mdibStorage: MdibStorageRead): List<String>? {
        val msp = when (update.pair) {
            is Pair.MultiStatePair -> update.pair.pair
            else -> return null
        }

        val stateCollision = msp.abstractContextStates
            .filter { isCollidingDescriptor(it.handle, mdibStorage) }
            .map { it.handle }

        if (stateCollision.isNotEmpty()) {
            return stateCollision
        }
        return null
    }

    override fun process(modifications: MdibStateModifications, storage: MdibStorageRead): MdibStateModifications {
        when (modifications) {
            is MdibStateModifications.Context -> {
                val stateCollision = modifications.contextStates
                    .filter { isCollidingDescriptor(it.handle, storage) }
                    .map { it.handle }
                if (stateCollision.isNotEmpty()) {
                    throw HandleDuplicatedException(
                        "Context change included handles colliding with descriptors: ${stateCollision.joinToString()}"
                    )
                }
            }
            else -> {}
        }

        return modifications
    }

    private fun isCollidingDescriptor(handle: String, mdibStorage: MdibStorageRead): Boolean =
        mdibStorage.getDescriptor(handle).isPresent
    private fun isCollidingState(handle: String, mdibStorage: MdibStorageRead): Boolean =
        mdibStorage.getState(handle).isPresent
    private fun isCollidingHandle(handle: String, mdibStorage: MdibStorageRead): Boolean =
        isCollidingDescriptor(handle, mdibStorage) || isCollidingState(handle, mdibStorage)

}
