package org.somda.sdc.biceps.provider.preprocessing

import org.somda.sdc.biceps.common.MdibDescriptionModification
import org.somda.sdc.biceps.common.MdibStateModifications
import org.somda.sdc.biceps.common.storage.DescriptionPreprocessingSegment
import org.somda.sdc.biceps.common.storage.MdibStorageRead
import org.somda.sdc.biceps.common.storage.StatePreprocessingSegment
import org.somda.sdc.biceps.model.participant.AbstractContextState

/**
 * Checker which ensures that both descriptor handles and context state handles are only ever reused for the same type
 * of descriptor and context state.
 */
class TypeChangeChecker: DescriptionPreprocessingSegment, StatePreprocessingSegment {

    private val handleTypeMap = mutableMapOf<String, Class<*>>()
    override fun process(
        modifications: MutableList<MdibDescriptionModification>,
        mdibStorage: MdibStorageRead
    ): MutableList<MdibDescriptionModification> {

        modifications
            .mapNotNull {
                when (it) {
                    is MdibDescriptionModification.Insert -> Pair(it.descriptorHandle, it.pair)
                    is MdibDescriptionModification.Update -> Pair(it.descriptorHandle, it.pair)
                    else -> null
                }
            }
            .forEach { (descriptorHandle, pair) ->
                handleTypeMap[descriptorHandle]?.let {
                    if (it != pair.descriptor::class.java) {
                        throw TypeConsistencyException(
                            "Handle $descriptorHandle was previously used for $it," +
                                " but is now used for ${pair.descriptor::class.java}"
                        )
                    }
                }
                when (pair) {
                    is org.somda.sdc.biceps.common.Pair.MultiStatePair -> {
                        pair.pair.abstractContextStates.forEach {
                            processContextState(it)
                        }
                    }
                    else -> {}
                }

                handleTypeMap[descriptorHandle] = pair.descriptor::class.java
            }

        return modifications
    }

    override fun process(modifications: MdibStateModifications, storage: MdibStorageRead): MdibStateModifications {
        when (modifications) {
            is MdibStateModifications.Context -> {
                modifications.contextStates.forEach {
                    processContextState(it)
                }
            }
            else -> {}
        }

        return modifications
    }

    private fun processContextState(state: AbstractContextState) {
        handleTypeMap[state.handle]?.let { type ->
            if (type != state::class.java) {
                throw TypeConsistencyException(
                    "Handle ${state.handle} was previously used for $type, but is now used for ${state::class.java}"
                )
            }
        }

        handleTypeMap[state.handle] = state::class.java
    }

}
