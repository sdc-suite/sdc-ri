plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.lint-checkstyle")
    id("buildlogic.kotlin-conventions")
    id("buildlogic.deploy")
    `java-test-fixtures`
}

dependencies {
    api(projects.common)
    api(libs.org.somda.sdc.biceps.model)
    testImplementation(projects.test)
}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming" +
    " with the IEEE 11073 SDC specifications. This project implements the functionality described in" +
    " IEEE 11073-10207."

checkstyle {
    // skip tests
    sourceSets = setOf(project.sourceSets.main.get())
}