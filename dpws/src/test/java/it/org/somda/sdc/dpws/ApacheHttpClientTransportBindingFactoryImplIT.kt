package it.org.somda.sdc.dpws

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import it.org.somda.sdc.dpws.HttpServerUtil.GzipResponseHandler
import it.org.somda.sdc.dpws.soap.Ssl
import jakarta.xml.bind.JAXBElement
import org.apache.commons.lang3.reflect.FieldUtils
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.spy
import org.mockito.Mockito.verify
import org.mockito.kotlin.any
import org.somda.sdc.dpws.CommunicationLogContext
import org.somda.sdc.dpws.DpwsConfig
import org.somda.sdc.dpws.DpwsTest
import org.somda.sdc.dpws.crypto.CryptoConfig
import org.somda.sdc.dpws.crypto.CryptoSettings
import org.somda.sdc.dpws.factory.TransportBindingFactory
import org.somda.sdc.dpws.guice.DefaultDpwsConfigModule
import org.somda.sdc.dpws.helper.JaxbMarshalling
import org.somda.sdc.dpws.http.HttpConnectionInterceptor
import org.somda.sdc.dpws.http.apache.ClientTransportBinding
import org.somda.sdc.dpws.soap.SoapMarshalling
import org.somda.sdc.dpws.soap.SoapMessage
import org.somda.sdc.dpws.soap.factory.EnvelopeFactory
import org.somda.sdc.dpws.soap.factory.SoapMessageFactory
import java.io.ByteArrayOutputStream
import java.net.InetSocketAddress
import java.net.URI
import java.util.*
import javax.xml.namespace.QName

internal class ApacheHttpClientTransportBindingFactoryImplIT : DpwsTest() {
    private lateinit var transportBindingFactory: TransportBindingFactory
    private lateinit var soapMessageFactory: SoapMessageFactory
    private lateinit var envelopeFactory: EnvelopeFactory
    private lateinit var marshalling: SoapMarshalling
    private lateinit var spyingHttpConnectionInterceptor: SpyingHttpConnectionInterceptor

    // dummy interceptor for testing
    class SpyingHttpConnectionInterceptor(
        val interceptor: HttpConnectionInterceptor = spy(object: HttpConnectionInterceptor {})
    ): HttpConnectionInterceptor by interceptor

    @BeforeEach
    @Throws(Exception::class)
    public override fun setUp() {
        val clientCert = Ssl.setupClient()
        spyingHttpConnectionInterceptor = SpyingHttpConnectionInterceptor()
        val override: DefaultDpwsConfigModule = object : DefaultDpwsConfigModule() {
            public override fun customConfigure() {
                bind(
                    DpwsConfig.HTTP_GZIP_COMPRESSION,
                    Boolean::class.java, true
                )
                bind(
                    DpwsConfig.HTTP_CONNECTION_INTERCEPTOR,
                    HttpConnectionInterceptor::class.java,
                    spyingHttpConnectionInterceptor
                )
                bind(
                    CryptoConfig.CRYPTO_SETTINGS,
                    CryptoSettings::class.java,
                    clientCert
                )
                bind(
                    DpwsConfig.HTTPS_SUPPORT,
                    Boolean::class.java,
                    true
                )
                bind(
                    DpwsConfig.HTTP_SUPPORT,
                    Boolean::class.java,
                    true
                )
            }
        }
        this.overrideBindings(override)
        super.setUp()
        transportBindingFactory = injector.getInstance(TransportBindingFactory::class.java)
        soapMessageFactory = injector.getInstance(SoapMessageFactory::class.java)
        envelopeFactory = injector.getInstance(EnvelopeFactory::class.java)
        injector.getInstance(JaxbMarshalling::class.java).startAsync().awaitRunning()
        marshalling = injector.getInstance(SoapMarshalling::class.java).also {
            it.startAsync().awaitRunning()
        }
    }

    @Test
    @Throws(Exception::class)
    fun testGzipCompression() {
        var baseUri = URI.create("http://127.0.0.1:0/")

        val expectedResponse = """
            Sehr geehrter Kaliba, netter Versuch
            Kritische Texte, Weltverbesserer-Blues;
            """.trimIndent()

        val jaxbElement = JAXBElement(
            QName("root-element"),
            String::class.java, expectedResponse
        )

        val responseEnvelope = createASoapMessage()
        responseEnvelope.originalEnvelope.body.any.add(jaxbElement)

        // make bytes out of the expected response
        val expectedResponseStream = ByteArrayOutputStream()
        marshalling.marshal(responseEnvelope.envelopeWithMappedHeaders, expectedResponseStream)

        val responseBytes = expectedResponseStream.toByteArray()

        // spawn the http server
        val handler = GzipResponseHandler(responseBytes)
        val inetSocketAddress = InetSocketAddress(baseUri.host, baseUri.port)
        val server = HttpServerUtil.spawnHttpServer(inetSocketAddress, handler)

        // replace the port
        baseUri = baseUri.replacePort(server.address.port)

        // make request to our server
        val httpBinding1 = transportBindingFactory.createHttpBinding(baseUri.toString(), null)
        val response = httpBinding1.onRequestResponse(createASoapMessage())

        val actualResponseStream = ByteArrayOutputStream()
        marshalling.marshal(response.envelopeWithMappedHeaders, actualResponseStream)

        // response bytes should exactly match our expected bytes, transparently decompressed
        Assertions.assertArrayEquals(expectedResponseStream.toByteArray(), actualResponseStream.toByteArray())
    }

    @Test
    @DisplayName("test that factory uses same client for all bindings")
    fun testNoDuplicateClient() {
        val baseUri = URI.create("http://127.0.0.1:5000/")

        // create binding and retrieve http client
        val httpBinding1 = transportBindingFactory.createHttpBinding(
            baseUri.toString(),
            null
        ) as ClientTransportBinding
        val client1 = FieldUtils.readDeclaredField(httpBinding1, "client", true)

        // create another binding with a communication context, client must remain the same
        val ctx = CommunicationLogContext("dummy")
        val httpBinding2 = transportBindingFactory.createHttpBinding(
            baseUri.toString(),
            ctx
        ) as ClientTransportBinding
        val client2 = FieldUtils.readDeclaredField(httpBinding2, "client", true)

        Assertions.assertNotNull(client1)
        Assertions.assertSame(client1, client2)
    }

    inner class DummyHttpHandler: HttpHandler {
        override fun handle(exchange: HttpExchange) {
            // setting length to -1, i.e., without any content, triggers the behavior where the interceptor doesn't
            // see any requests
            exchange.sendResponseHeaders(200, -1)
            exchange.responseBody.close()
        }
    }

    @Test
    fun testHttpConnectionInterceptor() {
        val serverBaseUri = URI.create("https://127.0.0.1:0/")

        val sslServer = Ssl.setupServer()

        // setup http server
        val handler = DummyHttpHandler()
        val inetSocketAddress = InetSocketAddress(serverBaseUri.host, serverBaseUri.port)
        val server = HttpServerUtil.spawnHttpsServer(
            inetSocketAddress,
            handler,
            HttpServerUtil.fromCryptoSettings(sslServer)
        )

        // replace the port
        val clientBaseUri = serverBaseUri.replacePort(server.address.port)

        // make request to our server
        val httpBinding1 = transportBindingFactory.createHttpBinding(clientBaseUri.toString(), null)
        httpBinding1.onRequestResponse(createASoapMessage())

        // check if the interceptor was called, once for the first message and once for the message
        // without the custom HttpRequestExecutor these do not occur
        verify(
            spyingHttpConnectionInterceptor.interceptor,
            Mockito.times(1)
        ).onFirstIntercept(any(), any(), any())

        verify(
            spyingHttpConnectionInterceptor.interceptor,
            Mockito.times(1)
        ).onIntercept(any())
    }

    private fun createASoapMessage(): SoapMessage {
        return soapMessageFactory.createSoapMessage(envelopeFactory.createEnvelope())
    }

    private fun URI.replacePort(port: Int) = URI(
        this.scheme,
        this.userInfo,
        this.host,
        port,
        this.path,
        this.query,
        this.fragment
    )


    companion object: Logging
}
