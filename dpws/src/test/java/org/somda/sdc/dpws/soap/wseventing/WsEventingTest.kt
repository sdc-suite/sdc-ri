package org.somda.sdc.dpws.soap.wseventing

import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.Uninterruptibles
import com.google.inject.AbstractModule
import com.google.inject.Key
import com.google.inject.TypeLiteral
import com.google.inject.name.Names
import jakarta.xml.bind.UnmarshalException
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertInstanceOf
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.RETURNS_DEEP_STUBS
import org.mockito.Mockito.mock
import org.mockito.kotlin.mock
import org.somda.sdc.common.util.ExecutorWrapperService
import org.somda.sdc.dpws.DpwsConstants
import org.somda.sdc.dpws.DpwsTest
import org.somda.sdc.dpws.HttpServerRegistryMock
import org.somda.sdc.dpws.LocalAddressResolverMock
import org.somda.sdc.dpws.TransportBindingFactoryMock
import org.somda.sdc.dpws.client.ClientEventObserver
import org.somda.sdc.dpws.client.SubscriptionEvent
import org.somda.sdc.dpws.factory.TransportBindingFactory
import org.somda.sdc.dpws.guice.NetworkJobThreadPool
import org.somda.sdc.dpws.helper.JaxbMarshalling
import org.somda.sdc.dpws.http.HttpException
import org.somda.sdc.dpws.http.HttpServerRegistry
import org.somda.sdc.dpws.model.ObjectFactory
import org.somda.sdc.dpws.network.LocalAddressResolver
import org.somda.sdc.dpws.soap.CommunicationContext
import org.somda.sdc.dpws.soap.MarshallingService
import org.somda.sdc.dpws.soap.NotificationSink
import org.somda.sdc.dpws.soap.RequestResponseClient
import org.somda.sdc.dpws.soap.RequestResponseServer
import org.somda.sdc.dpws.soap.SoapConstants
import org.somda.sdc.dpws.soap.SoapMarshalling
import org.somda.sdc.dpws.soap.SoapMessage
import org.somda.sdc.dpws.soap.SoapUtil
import org.somda.sdc.dpws.soap.exception.MalformedSoapMessageException
import org.somda.sdc.dpws.soap.exception.SoapFaultException
import org.somda.sdc.dpws.soap.exception.TransportException
import org.somda.sdc.dpws.soap.factory.NotificationSinkFactory
import org.somda.sdc.dpws.soap.factory.RequestResponseClientFactory
import org.somda.sdc.dpws.soap.factory.SoapFaultFactory
import org.somda.sdc.dpws.soap.model.Fault
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingServerInterceptor
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wseventing.factory.EventSourceInterceptorDispatcherFactory
import org.somda.sdc.dpws.soap.wseventing.factory.WsEventingEventSinkFactory
import org.somda.sdc.dpws.soap.wseventing.model.WsEventingStatus
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Round trip test for WS-Eventing (Source+Sink).
 */
@Suppress("LargeClass") // many tests needed
internal class WsEventingTest : DpwsTest() {
    private lateinit var wseSink: EventSink
    private lateinit var notificationSink: NotificationSink
    private lateinit var wseSourceDispatcher: EventSourceInterceptorDispatcher
    private lateinit var wseSource: ActionBasedEventSource
    private lateinit var requestResponseClientSpy: RequestResponseClient
    private lateinit var executorService: ExecutorWrapperService<ListeningExecutorService>
    private val sinkClientSubscriptionEvents = mutableListOf<SubscriptionEvent>()
    private val sinkClientEventObserver: ClientEventObserver =
        ClientEventObserver { event -> sinkClientSubscriptionEvents.add(event) }

    @BeforeEach
    @Throws(Exception::class)
    public override fun setUp() {
        overrideBindings(listOf(DpwsModuleReplacements()))
        super.setUp()

        // reset registry
        HttpServerRegistryMock.getRegistry().clear()

        // start required thread pool(s)
        executorService = injector.getInstance(
            Key.get(
                object : TypeLiteral<ExecutorWrapperService<ListeningExecutorService>>() {},
                NetworkJobThreadPool::class.java
            )
        )
        executorService.startAsync().awaitRunning()

        injector.getInstance(JaxbMarshalling::class.java).startAsync().awaitRunning()
        injector.getInstance(SoapMarshalling::class.java).startAsync().awaitRunning()

        val wsaUtil = injector.getInstance(WsAddressingUtil::class.java)
        val dpwsFactory = injector.getInstance(
            ObjectFactory::class.java
        )

        wseSource = injector.getInstance(ActionBasedEventSource::class.java)
        wseSourceDispatcher = injector
            .getInstance(EventSourceInterceptorDispatcherFactory::class.java)
            .create(listOf(wseSource))
        val reqResSrv = injector.getInstance(RequestResponseServer::class.java)
        reqResSrv.register(wseSourceDispatcher)
        notificationSink = injector.getInstance(NotificationSinkFactory::class.java).createNotificationSink(
            injector.getInstance(WsAddressingServerInterceptor::class.java)
        )

        val httpSrvRegistry = injector.getInstance(HttpServerRegistry::class.java)

        val uri = "https://$HOST:$PORT"
        val hostedServiceUri = httpSrvRegistry.registerContext(
            uri, true, HOSTED_SERVICE_PATH, null, null
        ) { inStream, outStream, communicationContext ->
            MarshallingHelper.handleRequestResponse(
                injector,
                reqResSrv,
                inStream,
                outStream,
                communicationContext
            )
        }

        val hst = dpwsFactory.createHostedServiceType()
        hst.endpointReference.add(wsaUtil.createEprWithAddress(hostedServiceUri))

        val rrcFactory = injector.getInstance(RequestResponseClientFactory::class.java)
        val tbFactory = injector.getInstance(TransportBindingFactory::class.java)
        val rrc = rrcFactory.createRequestResponseClient(
            tbFactory.createTransportBinding(hostedServiceUri, null)
        )
        requestResponseClientSpy = Mockito.spy(rrc)

        wseSink = injector.getInstance(WsEventingEventSinkFactory::class.java)
            .createWsEventingEventSink(
                requestResponseClientSpy,
                "http://localhost:1234",
                null,
                sinkClientEventObserver
            )
    }

    @Test
    @Throws(Exception::class)
    fun subscribe() {
        val expectedExpires = MAX_EXPIRES

        var resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )
        MatcherAssert.assertThat("Subscription ID length", resInfo.get().subscriptionId.length, Matchers.greaterThan(0))
        MatcherAssert.assertThat(
            "Granted expires duration",
            resInfo.get().grantedExpires,
            Matchers.`is`(expectedExpires)
        )

        val tryExpires = MAX_EXPIRES.plusHours(1)
        resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            tryExpires,
            notificationSink
        )
        MatcherAssert.assertThat(
            "Second subscription ID length",
            resInfo.get().subscriptionId.length,
            Matchers.greaterThan(0)
        )
        MatcherAssert.assertThat(
            "Seconds granted expires duration", resInfo.get().grantedExpires, Matchers.`is`(
                MAX_EXPIRES
            )
        )

        resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            null,
            notificationSink
        )
        MatcherAssert.assertThat(
            "Second subscription ID length",
            resInfo.get().subscriptionId.length,
            Matchers.greaterThan(0)
        )
        MatcherAssert.assertThat(
            "Seconds granted expires duration", resInfo.get().grantedExpires, Matchers.`is`(
                MAX_EXPIRES
            )
        )
    }

    @Test
    @Throws(Exception::class)
    fun renew() {
        var expectedExpires = Duration.ofHours(1)
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )
        MatcherAssert.assertThat(
            "Granted expires duration",
            resInfo.get().grantedExpires,
            Matchers.`is`(expectedExpires)
        )

        expectedExpires = Duration.ofHours(2)
        val actualExpires = wseSink.renew(resInfo.get().subscriptionId, expectedExpires)
        MatcherAssert.assertThat("Renew granted expires duration", actualExpires.get(), Matchers.`is`(expectedExpires))
    }

    @Test
    @Throws(Exception::class)
    fun getStatus() {
        val expectedExpires = Duration.ofHours(1)
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )
        MatcherAssert.assertThat(
            "Granted expires duration",
            resInfo.get().grantedExpires,
            Matchers.`is`(expectedExpires)
        )

        Thread.sleep(1000)

        val actualExpires =
            wseSink.getStatus(resInfo.get().subscriptionId)
        MatcherAssert.assertThat(
            "GetStatus retrieved expires duration",
            actualExpires.get(),
            Matchers.lessThan(expectedExpires)
        )
        MatcherAssert.assertThat(
            "GetStatus retrieved expires duration",
            actualExpires.get(),
            Matchers.greaterThan(Duration.ZERO)
        )
    }


    @Test
    @Throws(Exception::class)
    fun unsubscribe() {
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        // Given an active subscription with two sinks
        val expectedExpires = Duration.ofHours(1)
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )
        MatcherAssert.assertThat(
            "Granted expires duration",
            resInfo.get().grantedExpires,
            Matchers.`is`(expectedExpires)
        )

        // NotifyTo and EndTo sinks
        assertEquals(2,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        wseSink.getStatus(resInfo.get().subscriptionId).get()

        // When unsubscribing
        wseSink.unsubscribe(resInfo.get().subscriptionId).get()

        // Then all sinks are removed from the registry
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        assertThrows<Exception> {
            wseSink.getStatus(resInfo.get().subscriptionId).get()
        }
    }

    @Test
    @Throws(Exception::class)
    fun failedUnsubscribeCleansUpSubscriptions() {
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        // Given an active subscription with two sinks
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            Duration.ofHours(1),
            notificationSink
        )
        resInfo.get()

        // NotifyTo and EndTo sinks
        assertEquals(2,
            HttpServerRegistryMock
                .getRegistry()
                .keys
                .count { it.contains("/EventSink/") }
        )

        // When an exception is thrown during handling of unsubscription
        Mockito.doThrow(TransportException()).`when`(requestResponseClientSpy)
            .sendRequestResponse(ArgumentMatchers.argThat { soapMessage: SoapMessage ->
                soapMessage.wsAddressingHeader.action.orElseThrow().toString().lowercase(Locale.getDefault())
                    .contains("unsubscribe")
            })
        wseSink.unsubscribe(resInfo.get().subscriptionId).get()


        // Then all sinks are removed from the registry
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        assertThrows<Exception> {
            wseSink.getStatus(resInfo.get().subscriptionId).get()
        }
    }

    @Test
    @Throws(Exception::class)
    fun cancelledUnsubscribeCleansUpSubscriptions() {
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        // Given an active subscription with two sinks
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            Duration.ofHours(1),
            notificationSink
        )
        resInfo.get()

        // NotifyTo and EndTo sinks
        assertEquals(2,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )

        // Ensure that executor is busy and cannot handle the unsubscribe job
        repeat(1000) {
            executorService.get().submit {
                Uninterruptibles.sleepUninterruptibly(
                    Duration.ofHours(1)
                )
            }
        }

        // When unsubscribing and cancelling the future before it has a chance to be completed
        wseSink.unsubscribe(resInfo.get().subscriptionId).cancel(true)


        // Then all sinks are removed from the registry
        assertEquals(0,
            HttpServerRegistryMock.getRegistry().keys.stream()
                .filter { it.contains("/EventSink/") }
                .count()
        )
    }

    @Test
    @Throws(Exception::class)
    fun subscriptionEndNoStale() {
        val expectedExpires = Duration.ofSeconds(1)
        val spySink = Mockito.spy(notificationSink)
        val resInfo = wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            spySink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]
        assertEquals(expectedExpires, resInfo.grantedExpires)

        // wait expiration time plus one second to make sure it is expired
        Thread.sleep(1000 + expectedExpires.toMillis())

        wseSource.subscriptionEndToAll(WsEventingStatus.STATUS_SOURCE_CANCELLING)

        // we must wait for the message to be sent asynchronously, otherwise we might miss it
        Thread.sleep(1000)

        Mockito.verify(
            spySink,
            Mockito.times(0)
        ).receiveNotification(ArgumentMatchers.any(), ArgumentMatchers.any())
    }

    @Test
    fun subscriptionMessageProcessingFailSendsMessage() {
        val expectedExpires = Duration.ofHours(1)
        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        // send the notification to the NotifyTo handler
        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("NotifyTo") }
            .value

        val handlerResponse = ByteArrayOutputStream()

        val handlerError = assertThrows<HttpException> {
            handler.handle(
                ByteArrayInputStream("test".toByteArray()),
                handlerResponse,
                mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
            )
        }

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        val subscriptionMessageProcessingFailedEvent = sinkClientSubscriptionEvents
            .filterIsInstance<SubscriptionEvent.MessageProcessingFailed>()
            .first()

        assertEquals(
            UnmarshalException::class.java,
            subscriptionMessageProcessingFailedEvent.cause.javaClass
        )

        val fault = extractFault(handlerResponse)

        assertEquals(SoapConstants.SENDER, fault.code.value)

        // Sender must be 400 according to Soap 1.2 Part 2 Table 20
        assertEquals(400, handlerError.statusCode)

        wseSink.unsubscribeAll()

        // we must wait for the message to be sent asynchronously, otherwise we might miss it
        Thread.sleep(1000)

        assertEquals(3, sinkClientSubscriptionEvents.size)

        val startEvent = assertInstanceOf(
            SubscriptionEvent.Start::class.java,
            sinkClientSubscriptionEvents[0]
        )
        val subscriptionId = startEvent.subscriptionId

        val errorEvent = assertInstanceOf(
            SubscriptionEvent.MessageProcessingFailed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertEquals(
            subscriptionId,
            errorEvent.subscriptionId
        )

        // we didn't have an unexpected end, like the unsubscribe task failing, so we expect a regular end event
        val endEvent = assertInstanceOf(
            SubscriptionEvent.End::class.java,
            sinkClientSubscriptionEvents[2]
        )
        assertEquals(subscriptionId, endEvent.subscriptionId)
    }

    @Test
    fun subscriptionMessageUnsubscribeFail() {
        val expectedExpires = Duration.ofMillis(1)
        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        // wait for the subscription to time out, so that unsubscribe can fail
        Thread.sleep(expectedExpires.toMillis())

        wseSink.unsubscribeAll()

        Thread.sleep(1000)

        assertEquals(
            2, sinkClientSubscriptionEvents.size,
            "Elements are: $sinkClientSubscriptionEvents"
        )

        val startEvent = assertInstanceOf(
            SubscriptionEvent.Start::class.java,
            sinkClientSubscriptionEvents[0]
        )
        val subscriptionId = startEvent.subscriptionId

        // we've timed out the subscription, unsubscribe must have failed
        val endEvent = assertInstanceOf(
            SubscriptionEvent.Failed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertEquals(subscriptionId, endEvent.subscriptionId)
    }

    @Test
    @DisplayName(
        "Subscribe multiple times and ensure that messages subscribe and" +
            " subscription end events are published for each subscription"
    )
    fun subscriptionMessageManySubscribesAndUnsubscribes() {
        val expectedExpires = Duration.ofHours(1)
        val expectedSubscribes = 100

        repeat(expectedSubscribes) {
            wseSink.subscribe(
                DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
                listOf(ACTION),
                expectedExpires,
                notificationSink
            )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]
        }

        wseSink.unsubscribeAll()

        // wait for async events happening
        Thread.sleep(1000)

        assertEquals(
            2 * expectedSubscribes, sinkClientSubscriptionEvents.size,
            "Elements are: $sinkClientSubscriptionEvents"
        )

        sinkClientSubscriptionEvents.subList(0, expectedSubscribes).forEach {
            assertInstanceOf(SubscriptionEvent.Start::class.java, it)
        }

        sinkClientSubscriptionEvents.subList(expectedSubscribes, sinkClientSubscriptionEvents.size).forEach {
            assertInstanceOf(SubscriptionEvent.End::class.java, it)
        }
    }

    @Test
    fun subscriptionMessageIoException() {
        val expectedExpires = Duration.ofHours(1)
        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            notificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        // send the notification to the NotifyTo handler
        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("NotifyTo") }
            .value

        val expectedExceptionText = "Expected IO error"

        val handlerResponse = ByteArrayOutputStream()
        val failingInputStream = object : ByteArrayInputStream(VALID_INBOUND_MESSAGE.toByteArray()) {
            override fun close() {
                throw IOException(expectedExceptionText)
            }
        }
        val handlerError = assertThrows<HttpException> {
            handler.handle(
                failingInputStream,
                handlerResponse,
                mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
            )
        }

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        val subscriptionMessageProcessingFailedEvent = sinkClientSubscriptionEvents
            .filterIsInstance<SubscriptionEvent.MessageProcessingFailed>()
            .first()

        assertEquals(
            UnmarshalException::class.java,
            subscriptionMessageProcessingFailedEvent.cause.javaClass
        )

        val fault = extractFault(handlerResponse)

        assertEquals(
            SoapConstants.SENDER,
            fault.code.value
        )

        // Sender must be 400 according to Soap 1.2 Part 2 Table 20
        assertEquals(400, handlerError.statusCode)

        assertDoesNotThrow("Reasons were ${fault.reason.text}") {
            fault
                .reason
                .text
                .first {
                    it.value.contains("An error occurred during the processing of the inbound message")
                }
        }

        // ensure processing failure contains the expected text
        val endEvent = assertInstanceOf(
            SubscriptionEvent.MessageProcessingFailed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertTrue(endEvent.toString().contains(expectedExceptionText))
    }

    @Test
    fun subscriptionProcessingErrorSoapException() {
        val expectedExpires = Duration.ofHours(1)

        val expectedFaultText = "Intended error processing request"
        val expectedFault = injector.getInstance(SoapFaultFactory::class.java)
            .createReceiverFault(expectedFaultText)

        val mockNotificationSink = mock<NotificationSink> {
            on { receiveNotification(ArgumentMatchers.any(), ArgumentMatchers.any()) }
                .thenThrow(SoapFaultException(expectedFault))
        }

        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            mockNotificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("NotifyTo") }
            .value
        val handlerResponse = ByteArrayOutputStream()
        val handlerError = assertThrows<HttpException> {
            handler.handle(
                ByteArrayInputStream(VALID_INBOUND_MESSAGE.toByteArray()),
                handlerResponse,
                mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
            )
        }

        val fault = extractFault(handlerResponse)

        assertEquals(
            SoapConstants.RECEIVER,
            fault.code.value
        )

        // Receiver must be 500 according to Soap 1.2 Part 2 Table 20
        assertEquals(500, handlerError.statusCode)

        assertDoesNotThrow("Reasons were ${fault.reason.text}") {
            fault
                .reason
                .text
                .first {
                    it.value.contains(expectedFaultText)
                }
        }

        // ensure processing failure contains the expected text
        val endEvent = assertInstanceOf(
            SubscriptionEvent.MessageProcessingFailed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertTrue(endEvent.cause.toString().contains(expectedFaultText))
    }

    @Test
    fun subscriptionProcessingRuntimeException() {
        val expectedExpires = Duration.ofHours(1)

        val expectedExceptionText = "Intended error processing request"

        val mockNotificationSink = mock<NotificationSink> {
            on { receiveNotification(ArgumentMatchers.any(), ArgumentMatchers.any()) }
                .thenThrow(RuntimeException(expectedExceptionText))
        }

        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            mockNotificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("NotifyTo") }
            .value
        val handlerResponse = ByteArrayOutputStream()
        val handlerError = assertThrows<HttpException> {
            handler.handle(
                ByteArrayInputStream(VALID_INBOUND_MESSAGE.toByteArray()),
                handlerResponse,
                mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
            )
        }

        assertEquals(500, handlerError.statusCode)
        assertTrue(handlerResponse.toByteArray().isEmpty())

        assertEquals(2, sinkClientSubscriptionEvents.size)

        // ensure processing failure contains the expected text
        val endEvent = assertInstanceOf(
            SubscriptionEvent.MessageProcessingFailed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertTrue(endEvent.cause.toString().contains(expectedExceptionText))
    }

    @Test
    fun subscriptionOutputStreamCloseException() {
        val expectedExpires = Duration.ofHours(1)

        val expectedExceptionText = "Intended error processing request"

        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            mock<NotificationSink>()
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("NotifyTo") }
            .value
        val handlerResponse = object : ByteArrayOutputStream() {
            override fun close() {
                throw IOException(expectedExceptionText)
            }
        }
        val handlerError = assertThrows<HttpException> {
            handler.handle(
                ByteArrayInputStream(VALID_INBOUND_MESSAGE.toByteArray()),
                handlerResponse,
                mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
            )
        }

        assertEquals(500, handlerError.statusCode)
        assertTrue(
            handlerResponse.toByteArray().isEmpty(),
            "Had content ${handlerResponse.toString(StandardCharsets.UTF_8)}"
        )

        assertEquals(2, sinkClientSubscriptionEvents.size)

        // ensure processing failure contains the expected text
        val endEvent = assertInstanceOf(
            SubscriptionEvent.MessageProcessingFailed::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertTrue(endEvent.cause.toString().contains(expectedExceptionText))
    }

    @Test
    fun subscriptionProcessingSubscriptionEnd() {
        val expectedExpires = Duration.ofHours(1)

        val mockNotificationSink = mock<NotificationSink>()

        wseSink.subscribe(
            DpwsConstants.WS_EVENTING_SUPPORTED_DIALECT,
            listOf(ACTION),
            expectedExpires,
            mockNotificationSink
        )[MAX_WAIT.toSeconds(), TimeUnit.SECONDS]

        assertFalse(sinkClientSubscriptionEvents.isEmpty())

        assertTrue(HttpServerRegistryMock.getRegistry().entries.any { it.key.contains("NotifyTo") })
        assertTrue(HttpServerRegistryMock.getRegistry().entries.any { it.key.contains("EndTo") })

        val handler = HttpServerRegistryMock
            .getRegistry()
            .entries
            .first { it.key.contains("EndTo") }
            .value
        val handlerResponse = ByteArrayOutputStream()
        handler.handle(
            ByteArrayInputStream(SUBSCRIPTION_END_CANCELLING.toByteArray()),
            handlerResponse,
            mock(CommunicationContext::class.java, RETURNS_DEEP_STUBS)
        )

        assertTrue(
            handlerResponse.toByteArray().isEmpty(),
            "Had content ${handlerResponse.toString(StandardCharsets.UTF_8)}"
        )

        // ensure event contains the expected status of the provider cancelling the subscription
        val endEvent = assertInstanceOf(
            SubscriptionEvent.End::class.java,
            sinkClientSubscriptionEvents[1]
        )
        assertEquals(WsEventingStatus.STATUS_SOURCE_CANCELLING, endEvent.wsEventingStatus)

        // ensure the contexts were unregistered
        assertFalse(HttpServerRegistryMock.getRegistry().entries.any { it.key.contains("NotifyTo") })
        assertFalse(HttpServerRegistryMock.getRegistry().entries.any { it.key.contains("EndTo") })
    }

    private fun extractFault(handlerResponse: ByteArrayOutputStream): Fault {

        // check for soap fault in the output stream
        val unmarshalledResponse = injector.getInstance(MarshallingService::class.java)
            .unmarshal(ByteArrayInputStream(handlerResponse.toByteArray()))

        assertTrue(unmarshalledResponse.isFault)

        val fault = injector.getInstance(SoapUtil::class.java)
            .getBody(
                unmarshalledResponse,
                Fault::class.java
            ).orElseThrow {
                MalformedSoapMessageException("Could not get Fault element from stream")
            }

        return fault
    }

    private class DpwsModuleReplacements : AbstractModule() {
        override fun configure() {
            TransportBindingFactoryMock.setHandlerRegistry(HttpServerRegistryMock.getRegistry())
            bind(Duration::class.java)
                .annotatedWith(Names.named(WsEventingConfig.SOURCE_MAX_EXPIRES))
                .toInstance(Duration.ofHours(3))
            bind(HttpServerRegistry::class.java)
                .to(HttpServerRegistryMock::class.java)
            bind(TransportBindingFactory::class.java)
                .to(TransportBindingFactoryMock::class.java)
            bind(LocalAddressResolver::class.java).to(LocalAddressResolverMock::class.java)
        }
    }

    companion object {
        private const val HOST = "mock-host"
        private const val PORT = 8080
        private const val HOSTED_SERVICE_PATH = "/hosted-service"
        private const val ACTION = "http://action"
        private val MAX_EXPIRES: Duration = Duration.ofHours(3)
        private val MAX_WAIT: Duration = Duration.ofSeconds(5)

        private const val VALID_INBOUND_MESSAGE = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<s:Envelope
        xmlns:a="http://www.w3.org/2005/08/addressing"
        xmlns:d="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01"
        xmlns:s="http://www.w3.org/2003/05/soap-envelope">
    <s:Header>
        <a:Action>http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Bye</a:Action>
        <a:MessageID>http://msg-id/Bye</a:MessageID>
        <a:To>urn:docs-oasis-open-org:ws-dd:ns:discovery:2009:01</a:To>
        <d:AppSequence InstanceId="1077004800" MessageNumber="4"/>
    </s:Header>
    <s:Body>
        <d:Bye>
            <a:EndpointReference>
                <a:Address>urn:uuid:98190dc2-0890-4ef8-ac9a-5940995e6119</a:Address>
            </a:EndpointReference>
        </d:Bye>
    </s:Body>
</s:Envelope>
        """
        private const val SUBSCRIPTION_END_CANCELLING = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<s:Envelope
        xmlns:a="http://www.w3.org/2005/08/addressing"
        xmlns:s="http://www.w3.org/2003/05/soap-envelope"
        xmlns:e="http://schemas.xmlsoap.org/ws/2004/08/eventing"
>
    <s:Header>
        <a:Action>http://schemas.xmlsoap.org/ws/2004/08/eventing/SubscriptionEnd</a:Action>
        <a:MessageID>http://msg-id/SubscriptionEnd</a:MessageID>
    </s:Header>
    <s:Body>
        <e:SubscriptionEnd>
            <e:SubscriptionManager>
                <a:Address>whatever</a:Address>
            </e:SubscriptionManager>
            <e:Status>http://schemas.xmlsoap.org/ws/2004/08/eventing/SourceCancelling</e:Status>
        </e:SubscriptionEnd>
    </s:Body>
</s:Envelope>"""

    }

}
