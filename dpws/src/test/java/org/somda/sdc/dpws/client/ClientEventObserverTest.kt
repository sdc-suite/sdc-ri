package org.somda.sdc.dpws.client

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertInstanceOf
import org.mockito.kotlin.mock
import org.somda.sdc.common.util.UriUtil
import org.somda.sdc.dpws.soap.wseventing.model.WsEventingStatus
import java.util.*

class ClientEventObserverTest {
    @Test
    @DisplayName("Verify conversion from SubscriptionEvent to Subscription is lossless")
    fun testEventConversion() {
        val eprAddress = UriUtil.createUuid(UUID.randomUUID())
        val subscriptionId = UriUtil.createUuid(UUID.randomUUID())
        run {
            val convertedStart = assertInstanceOf<Subscription.Start>(
                SubscriptionEvent.Start(subscriptionId).intoSubscription(eprAddress)
            )

            assertEquals(subscriptionId, convertedStart.subscriptionId)
            assertEquals(eprAddress, convertedStart.eprAddress)
        }

        run {
            val statusMessage = WsEventingStatus.STATUS_SOURCE_CANCELLING
            val convertedEnd = assertInstanceOf<Subscription.End>(
                SubscriptionEvent.End(subscriptionId, statusMessage).intoSubscription(eprAddress)
            )

            assertEquals(subscriptionId, convertedEnd.subscriptionId)
            assertEquals(eprAddress, convertedEnd.eprAddress)
            assertEquals(statusMessage, convertedEnd.wsEventingStatus)
        }

        run {
            val failMessage = "it failed intentionally"
            val failCause = mock<Exception>()
            val convertedFail = assertInstanceOf<Subscription.Failed>(
                SubscriptionEvent.Failed(subscriptionId, failMessage, failCause)
                    .intoSubscription(eprAddress)
            )

            assertEquals(subscriptionId, convertedFail.subscriptionId)
            assertEquals(eprAddress, convertedFail.eprAddress)
            assertEquals(failMessage, convertedFail.message)
            assertEquals(failCause, convertedFail.cause)
        }

        run {
            val failMessage = "it failed intentionally during processing"
            val failCause = mock<Exception>()
            val convertedMessageProcessingFailed = assertInstanceOf<Subscription.MessageProcessingFailed>(
                SubscriptionEvent.MessageProcessingFailed(subscriptionId, failMessage, failCause)
                    .intoSubscription(eprAddress)
            )

            assertEquals(subscriptionId, convertedMessageProcessingFailed.subscriptionId)
            assertEquals(eprAddress, convertedMessageProcessingFailed.eprAddress)
            assertEquals(failMessage, convertedMessageProcessingFailed.message)
            assertEquals(failCause, convertedMessageProcessingFailed.cause)
        }
    }
}
