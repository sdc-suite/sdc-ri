package org.somda.sdc.dpws.http

import java.net.SocketAddress
import java.security.cert.Certificate
import java.util.*
import javax.net.ssl.SSLSession

/**
 * Http connection interceptor interface.
 */
interface HttpConnectionInterceptor {
    /**
     * The source that established the connection.
     */
    enum class Source {
        CLIENT,
        SERVER
    }

    /**
     * Called once, when the first message is sent through a connection.
     *
     * @param connectionId the unique id of the connection
     * @param sslSession the SSL session of the connection
     * @param source the source, that established the connection
     */
    fun onFirstIntercept(connectionId: UUID, sslSession: SSLSession, source: Source) {
        // Do nothing by default
    }

    /**
     * Called on every message sent through a connection.
     *
     * @param connectionId the unique id of the connection
     */
    fun onIntercept(connectionId: UUID) {
        // Do nothing by default
    }

    /**
     * Called once, when the last message is sent through a connection.
     *
     * @param connectionId the unique id of the connection
     */
    fun onLastIntercept(connectionId: UUID) {
        // Do nothing by default
    }

    /**
     * Called when a client request fails.
     *
     * @param cause the cause of the failure
     * @param targetAddress the target address for which the attempted client connection failed or null, if the target
     * address is not available
     * @param retryCount the number of retries that have been attempted so far
     */
    fun onClientRequestFailure(cause: Throwable, targetAddress: SocketAddress?, retryCount: Int) {
        // Do nothing by default
    }

    /**
     * Called when a server SSL handshake fails.
     *
     * @param cause the cause of the failure
     * @param peerAddress the address of the peer that attempted to connect
     * @param peerCertificates the peer certificates if they can be extracted or empty otherwise
     */
    fun onServerSslHandshakeFailure(cause: Throwable, peerAddress: SocketAddress, peerCertificates: List<Certificate>) {
        // Do nothing by default
    }
}
