package org.somda.sdc.dpws.http.apache

import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler
import org.apache.http.protocol.HttpContext
import org.somda.sdc.dpws.http.HttpConnectionInterceptor
import java.io.IOException
import java.net.InetSocketAddress

internal class CustomHttpRetryHandler(
    retries: Int,
    retryNonIdempotent: Boolean,
    private val httpConnectionInterceptor: HttpConnectionInterceptor
) : DefaultHttpRequestRetryHandler(retries, retryNonIdempotent) {
    override fun retryRequest(exception: IOException, executionCount: Int, context: HttpContext): Boolean {
        val clientContext = HttpClientContext.adapt(context)
        val peerAddress: InetSocketAddress? = clientContext.targetHost?.let {
            InetSocketAddress(it.address, it.port)
        }

        httpConnectionInterceptor.onClientRequestFailure(exception, peerAddress, executionCount)

        return super.retryRequest(exception, executionCount, context)
    }
}
