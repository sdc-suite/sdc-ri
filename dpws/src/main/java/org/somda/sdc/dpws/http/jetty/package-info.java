/**
 * Jetty HTTP server adapter.
 */
@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.dpws.http.jetty;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
