package org.somda.sdc.dpws.http.apache

import org.apache.http.impl.conn.Wire
import java.io.IOException
import java.io.InputStream
import java.util.*

/**
 * This class is a Kotlin conversion of [org.apache.http.impl.conn.LoggingInputStream].
 */
internal class MyApacheLoggingInputStream(private val input: InputStream, private val wire: Wire) : InputStream() {
    @Throws(IOException::class)
    override fun read(): Int {
        try {
            val b = input.read()
            if (b == -1) {
                wire.input("end of stream")
            } else {
                wire.input(b)
            }
            return b
        } catch (ex: IOException) {
            wire.input("[read] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray): Int {
        try {
            val bytesRead = input.read(b)
            if (bytesRead == -1) {
                wire.input("end of stream")
            } else if (bytesRead > 0) {
                wire.input(b, 0, bytesRead)
            }
            return bytesRead
        } catch (ex: IOException) {
            wire.input("[read] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray, off: Int, len: Int): Int {
        try {
            val bytesRead = input.read(b, off, len)
            if (bytesRead == -1) {
                wire.input("end of stream")
            } else if (bytesRead > 0) {
                wire.input(b, off, bytesRead)
            }
            return bytesRead
        } catch (ex: IOException) {
            wire.input("[read] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun skip(n: Long): Long {
        try {
            return super.skip(n)
        } catch (ex: IOException) {
            wire.input("[skip] I/O error: " + ex.message)
            throw ex
        }
    }

    @Throws(IOException::class)
    override fun available(): Int {
        try {
            return input.available()
        } catch (ex: IOException) {
            wire.input("[available] I/O error : " + ex.message)
            throw ex
        }
    }

    override fun mark(readlimit: Int) {
        super.mark(readlimit)
    }

    @Throws(IOException::class)
    override fun reset() {
        super.reset()
    }

    override fun markSupported(): Boolean {
        return false
    }

    @Throws(IOException::class)
    override fun close() {
        try {
            input.close()
        } catch (ex: IOException) {
            wire.input("[close] I/O error: " + ex.message)
            throw ex
        }
    }
}
