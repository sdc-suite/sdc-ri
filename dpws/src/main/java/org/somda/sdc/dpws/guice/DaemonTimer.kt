package org.somda.sdc.dpws.guice

import com.google.inject.BindingAnnotation

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@BindingAnnotation
/**
 * Marks [Timer][java.util.Timer]s that need to use daemon threads.
 */
annotation class DaemonTimer
