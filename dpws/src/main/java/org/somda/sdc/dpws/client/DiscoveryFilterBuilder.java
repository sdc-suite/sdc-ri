package org.somda.sdc.dpws.client;

import org.jspecify.annotations.Nullable;
import org.somda.sdc.dpws.soap.wsdiscovery.MatchBy;

import javax.xml.namespace.QName;
import java.util.HashSet;
import java.util.Set;

/**
 * {@link DiscoveryFilter} convenience builder with method chaining.
 */
public class DiscoveryFilterBuilder {
    private final Set<QName> types;
    private final Set<String> scopes;
    private MatchBy matchBy = null;

    /**
     * Constructs a new object with empty types and scopes.
     */
    public DiscoveryFilterBuilder() {
        this.types = new HashSet<>();
        this.scopes = new HashSet<>();
    }

    /**
     * Adds a type.
     *
     * @param type the type as QName according to WS-Discovery
     * @return this object
     */
    public DiscoveryFilterBuilder addType(QName type) {
        types.add(type);
        return this;
    }

    /**
     * Adds a scope.
     *
     * @param scope the scope URI as string
     * @return this object
     */
    public DiscoveryFilterBuilder addScope(String scope) {
        scopes.add(scope);
        return this;
    }

    /**
     * Sets the scopes matching rule.
     *
     * @param matchBy the scopes matching rule, or null to use the default
     *                <code>http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/rfc3986</code>
     * @see <a href="http://docs.oasis-open.org/ws-dd/discovery/1.1/os/wsdd-discovery-1.1-spec-os.html#_Toc234231831">
     *      WS-Discovery Probe</a>
     * @return this object
     */
    public DiscoveryFilterBuilder setMatchBy(@Nullable MatchBy matchBy) {
        this.matchBy = matchBy;
        return this;
    }

    /**
     * Gets a discovery filter with all types and scopes added via {@link #addType(QName)} and
     * {@link #addScope(String)}.
     *
     * @return a {@linkplain DiscoveryFilter} instance
     */
    public DiscoveryFilter get() {
        return new DiscoveryFilter(types, scopes, matchBy);
    }
}
