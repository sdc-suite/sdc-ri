package org.somda.sdc.dpws.client

import com.google.common.eventbus.Subscribe
import org.somda.sdc.dpws.soap.wseventing.model.WsEventingStatus

/**
 * Callback used internally for events related to the client subscriptions.
 */
fun interface ClientEventObserver {
    fun onSubscriptionEvent(event: SubscriptionEvent)
}

/**
 * Observer for internal events related to the client.
 */
interface EventObserver {
    /**
     * Handles subscription events.
     */
    @Subscribe
    fun onSubscriptionEvent(event: Subscription)
}

/**
 * Events related to subscriptions in the [Client].
 *
 * If an event occurs which indicates a failure related to the subscription,
 * **the application** has to decide whether to stop its communication with the provider.
 *
 * @see ClientEventObserver
 */
sealed interface Subscription {

    val eprAddress: String
    val subscriptionId: String

    /**
     * Event indicating that a subscription has started.
     *
     * @property eprAddress endpoint reference address of the hosting service for this subscription
     * @property subscriptionId the internal identifier given to the subscription
     */
    data class Start(
        override val eprAddress: String,
        override val subscriptionId: String
    ) : Subscription

    /**
     * Event indicating that a subscription has ended.
     *
     * @property eprAddress endpoint reference address of the hosting service for this subscription
     * @property subscriptionId the internal identifier given to the subscription
     * @property wsEventingStatus the status of the SubscriptionEnd, if the end was caused by such a message
     */
    data class End(
        override val eprAddress: String,
        override val subscriptionId: String,
        val wsEventingStatus: WsEventingStatus? = null
    ) : Subscription

    /**
     * Event indicating that a subscription has failed to process a message
     *
     * This happens, for example, if the message itself is invalid XML, fails schema validation,
     * or caused an error during processing.
     *
     * @property eprAddress endpoint reference address of the hosting service for this subscription
     * @property subscriptionId the internal identifier given to the subscription
     * @property message a message describing the failure
     * @property cause the cause of the failure
     */
    data class MessageProcessingFailed(
        override val eprAddress: String,
        override val subscriptionId: String,
        val message: String,
        val cause: Throwable
    ) : Subscription

    /**
     * Event indicating that a subscription has failed in an unspecified manner.
     *
     * @property eprAddress endpoint reference address of the hosting service for this subscription
     * @property subscriptionId the internal identifier given to the subscription
     * @property message a message describing the failure
     * @property cause the cause of the failure
     */
    data class Failed(
        override val eprAddress: String,
        override val subscriptionId: String,
        val message: String,
        val cause: Throwable
    ) : Subscription

}

/**
 * Internal events related to subscriptions in the [Client].
 *
 * @see Subscription
 */
sealed interface SubscriptionEvent {

    val subscriptionId: String

    /**
     * @see [Subscription.Start]
     */
    data class Start(
        override val subscriptionId: String
    ) : SubscriptionEvent

    /**
     * @see [Subscription.End]
     */
    data class End(
        override val subscriptionId: String,
        val wsEventingStatus: WsEventingStatus? = null
    ) : SubscriptionEvent

    /**
     * @see [Subscription.MessageProcessingFailed]
     */
    data class MessageProcessingFailed(
        override val subscriptionId: String,
        val message: String,
        val cause: Throwable
    ) : SubscriptionEvent

    /**
     * @see [Subscription.Failed]
     */
    data class Failed(
        override val subscriptionId: String,
        val message: String,
        val cause: Throwable
    ) : SubscriptionEvent

    /**
     * Converts this event into a [Subscription] event using the endpoint reference address as additional information.
     */
    fun intoSubscription(eprAddress: String): Subscription {
        return when (this) {
            is Start -> Subscription.Start(eprAddress, subscriptionId)
            is End -> Subscription.End(eprAddress, subscriptionId, wsEventingStatus)
            is MessageProcessingFailed -> Subscription.MessageProcessingFailed(
                eprAddress,
                subscriptionId,
                message,
                cause
            )
            is Failed -> Subscription.Failed(eprAddress, subscriptionId, message, cause)
        }
    }
}
