package org.somda.sdc.dpws.soap.wseventing;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.inject.name.Named;
import jakarta.xml.bind.JAXBException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import org.jspecify.annotations.Nullable;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.logging.InstanceLogger;
import org.somda.sdc.common.util.ExecutorWrapperService;
import org.somda.sdc.dpws.CommunicationLogContext;
import org.somda.sdc.dpws.DpwsConfig;
import org.somda.sdc.dpws.TransportBinding;
import org.somda.sdc.dpws.client.ClientEventObserver;
import org.somda.sdc.dpws.client.SubscriptionEvent;
import org.somda.sdc.dpws.factory.TransportBindingFactory;
import org.somda.sdc.dpws.guice.NetworkJobThreadPool;
import org.somda.sdc.dpws.http.HttpException;
import org.somda.sdc.dpws.http.HttpServerRegistry;
import org.somda.sdc.dpws.http.HttpUriBuilder;
import org.somda.sdc.dpws.soap.CommunicationContext;
import org.somda.sdc.dpws.soap.NotificationSink;
import org.somda.sdc.dpws.soap.RequestResponseClient;
import org.somda.sdc.dpws.soap.SoapConstants;
import org.somda.sdc.dpws.soap.SoapFaultHttpStatusCodeMapping;
import org.somda.sdc.dpws.soap.SoapMarshalling;
import org.somda.sdc.dpws.soap.SoapMessage;
import org.somda.sdc.dpws.soap.SoapUtil;
import org.somda.sdc.dpws.soap.exception.MalformedSoapMessageException;
import org.somda.sdc.dpws.soap.exception.SoapFaultException;
import org.somda.sdc.dpws.soap.factory.RequestResponseClientFactory;
import org.somda.sdc.dpws.soap.factory.SoapFaultFactory;
import org.somda.sdc.dpws.soap.model.Fault;
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingConstants;
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil;
import org.somda.sdc.dpws.soap.wsaddressing.model.EndpointReferenceType;
import org.somda.sdc.dpws.soap.wseventing.exception.SubscriptionNotFoundException;
import org.somda.sdc.dpws.soap.wseventing.exception.SubscriptionRequestResponseClientNotFoundException;
import org.somda.sdc.dpws.soap.wseventing.factory.SubscriptionManagerFactory;
import org.somda.sdc.dpws.soap.wseventing.model.DeliveryType;
import org.somda.sdc.dpws.soap.wseventing.model.FilterType;
import org.somda.sdc.dpws.soap.wseventing.model.GetStatus;
import org.somda.sdc.dpws.soap.wseventing.model.GetStatusResponse;
import org.somda.sdc.dpws.soap.wseventing.model.ObjectFactory;
import org.somda.sdc.dpws.soap.wseventing.model.Renew;
import org.somda.sdc.dpws.soap.wseventing.model.RenewResponse;
import org.somda.sdc.dpws.soap.wseventing.model.Subscribe;
import org.somda.sdc.dpws.soap.wseventing.model.SubscribeResponse;
import org.somda.sdc.dpws.soap.wseventing.model.SubscriptionEnd;
import org.somda.sdc.dpws.soap.wseventing.model.Unsubscribe;
import org.somda.sdc.dpws.soap.wseventing.model.WsEventingStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Default implementation of {@link EventSink}.
 */
public class EventSinkImpl implements EventSink {
    private static final Logger LOG = LogManager.getLogger(EventSinkImpl.class);

    private static final String EVENT_SINK_CONTEXT_PREFIX = "/EventSink/";
    private static final String EVENT_SINK_NOTIFY_TO_CONTEXT_PREFIX = EVENT_SINK_CONTEXT_PREFIX + "NotifyTo/";
    private static final String EVENT_SINK_END_TO_CONTEXT_PREFIX = EVENT_SINK_CONTEXT_PREFIX + "EndTo/";
    private final RequestResponseClient requestResponseClient;
    private final TransportBindingFactory transportBindingFactory;
    private final RequestResponseClientFactory requestResponseClientFactory;
    private final String hostAddress;
    private final HttpServerRegistry httpServerRegistry;
    private final ObjectFactory wseFactory;
    private final WsAddressingUtil wsaUtil;
    private final SoapMarshalling marshalling;
    private final SoapUtil soapUtil;
    private final ExecutorWrapperService<ListeningExecutorService> executorService;
    private final SubscriptionManagerFactory subscriptionManagerFactory;
    private final Map<String, SubscriptionData> subscriptionData;
    private final Lock subscriptionsLock;
    private final Duration maxWaitForFutures;
    @Nullable
    private final CommunicationLogContext communicationLogContext;
    private final Logger instanceLogger;
    private final HttpUriBuilder httpUriBuilder;
    private final SoapFaultFactory soapFaultFactory;
    private final ClientEventObserver clientEventObserver;

    @AssistedInject
    EventSinkImpl(@Assisted RequestResponseClient requestResponseClient,
                  @Assisted String hostAddress,
                  @Assisted @Nullable CommunicationLogContext communicationLogContext,
                  @Assisted ClientEventObserver clientEventObserver,
                  @Named(DpwsConfig.MAX_WAIT_FOR_FUTURES) Duration maxWaitForFutures,
                  HttpServerRegistry httpServerRegistry,
                  ObjectFactory wseFactory,
                  WsAddressingUtil wsaUtil,
                  SoapMarshalling marshalling,
                  SoapUtil soapUtil,
                  @NetworkJobThreadPool ExecutorWrapperService<ListeningExecutorService> executorService,
                  SubscriptionManagerFactory subscriptionManagerFactory,
                  @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier,
                  TransportBindingFactory transportBindingFactory,
                  RequestResponseClientFactory requestResponseClientFactory,
                  SoapFaultFactory soapFaultFactory
    ) {
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.requestResponseClient = requestResponseClient;
        this.transportBindingFactory = transportBindingFactory;
        this.requestResponseClientFactory = requestResponseClientFactory;
        this.hostAddress = hostAddress;
        this.communicationLogContext = communicationLogContext;
        this.maxWaitForFutures = maxWaitForFutures;
        this.httpServerRegistry = httpServerRegistry;
        this.wseFactory = wseFactory;
        this.wsaUtil = wsaUtil;
        this.marshalling = marshalling;
        this.soapUtil = soapUtil;
        this.executorService = executorService;
        this.subscriptionManagerFactory = subscriptionManagerFactory;
        this.soapFaultFactory = soapFaultFactory;
        this.clientEventObserver = clientEventObserver;
        this.subscriptionData = new ConcurrentHashMap<>();
        this.subscriptionsLock = new ReentrantLock();
        this.httpUriBuilder = new HttpUriBuilder();
    }

    private record SubscriptionData(
        SinkSubscriptionManager subscriptionManager,
        RequestResponseClient requestResponseClient
    ) {}

    @Override
    public ListenableFuture<SubscribeResult> subscribe(String filterDialect,
                                                       List<Object> filters,
                                                       @Nullable Duration expires,
                                                       NotificationSink notificationSink) {
        return executorService.get().submit(() -> {
            // Create unique context path suffix
            String subscriptionId = UUID.randomUUID().toString();

            // Create unique end-to context path and create proper handler
            String endToContext = EVENT_SINK_END_TO_CONTEXT_PREFIX + subscriptionId;
            var endToUri = httpServerRegistry.registerContext(
                hostAddress,
                true,
                endToContext,
                null,
                communicationLogContext,
                (inStream, outStream, communicationContext) ->
                    processIncomingEndTo(
                        inStream,
                        outStream,
                        subscriptionId
                    )
            );

            // Create unique notify-to context path and create proper handler
            String notifyToContext = EVENT_SINK_NOTIFY_TO_CONTEXT_PREFIX + subscriptionId;
            var notifyToUri = httpServerRegistry.registerContext(
                hostAddress,
                true,
                notifyToContext,
                null,
                communicationLogContext,
                (inStream, outStream, communicationContext) ->
                    processIncomingNotification(
                        notificationSink,
                        inStream,
                        outStream,
                        communicationContext,
                        subscriptionId
                    )
            );

            // Create subscribe body, include formerly created end-to and notify-to endpoint addresses
            // Populate rest of the request
            Subscribe subscribeBody = wseFactory.createSubscribe();

            DeliveryType deliveryType = wseFactory.createDeliveryType();
            deliveryType.setMode(WsEventingConstants.SUPPORTED_DELIVERY_MODE);

            EndpointReferenceType notifyToEpr = wsaUtil.createEprWithAddress(notifyToUri);
            deliveryType.setContent(List.of(wseFactory.createNotifyTo(notifyToEpr)));
            subscribeBody.setDelivery(deliveryType);

            EndpointReferenceType endToEpr = wsaUtil.createEprWithAddress(endToUri);
            subscribeBody.setEndTo(endToEpr);

            FilterType filterType = wseFactory.createFilterType();
            filterType.setDialect(filterDialect);
            filterType.setContent(filters);

            subscribeBody.setExpires(expires);

            subscribeBody.setFilter(filterType);

            SoapMessage subscribeRequest = soapUtil.createMessage(WsEventingConstants.WSA_ACTION_SUBSCRIBE,
                    subscribeBody);

            SoapMessage soapResponse = requestResponseClient.sendRequestResponse(subscribeRequest);
            SubscribeResponse responseBody = soapUtil.getBody(soapResponse, SubscribeResponse.class).orElseThrow(() ->
                    new MalformedSoapMessageException("Cannot read WS-Eventing Subscribe response"));

            //  Create subscription manager proxy from response
            SinkSubscriptionManager sinkSubMan = subscriptionManagerFactory.createSinkSubscriptionManager(
                responseBody.getSubscriptionManager(),
                responseBody.getExpires(),
                notifyToEpr,
                endToEpr,
                filters,
                filterDialect,
                subscriptionId
            );

            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.Start(
                subscriptionId
            ));

            TransportBinding tBinding = transportBindingFactory.createTransportBinding(
                    responseBody.getSubscriptionManager().getAddress().getValue(),
                    null);
            RequestResponseClient rrClient = requestResponseClientFactory.createRequestResponseClient(tBinding);

            // Add sink subscription manager and httpclient for handling getStatus/renew/unsubscribe messages
            // to internal registry
            subscriptionsLock.lock();
            try {
                subscriptionData.put(subscriptionId, new SubscriptionData(sinkSubMan, rrClient));
            } finally {
                subscriptionsLock.unlock();
            }

            // Return id for addressing purposes
            return new SubscribeResult(subscriptionId, sinkSubMan.getExpires());
        });
    }

    @Override
    public ListenableFuture<Duration> renew(String subscriptionId,
                                            Duration expires) {
        return executorService.get().submit(() -> {
            // Search for subscription to renew
            SinkSubscriptionManager subMan = getSubscriptionManagerProxy(subscriptionId);
            RequestResponseClient subscriptionRequestResponseClient =
                    getSubscriptionRequestResponseClient(subscriptionId);

            // Create new request body
            Renew renew = wseFactory.createRenew();
            renew.setExpires(expires);
            String subManAddress = wsaUtil.getAddressUri(subMan.getSubscriptionManagerEpr()).orElseThrow(() ->
                    new RuntimeException("No subscription manager EPR found"));

            // Create new message, put subscription manager EPR address as wsa:To
            SoapMessage renewMsg = soapUtil.createMessage(
                    WsEventingConstants.WSA_ACTION_RENEW,
                    subManAddress,
                    renew,
                    subMan.getSubscriptionManagerEpr().getReferenceParameters()
            );

            // Invoke request-response
            SoapMessage renewResMsg = subscriptionRequestResponseClient.sendRequestResponse(renewMsg);
            RenewResponse renewResponse = soapUtil.getBody(renewResMsg, RenewResponse.class).orElseThrow(() ->
                    new MalformedSoapMessageException("WS-Eventing RenewResponse message is malformed"));

            // Parse expires in response message, renew at subscription manager and return
            Duration newExpires = renewResponse.getExpires();
            subMan.renew(newExpires);
            return newExpires;
        });
    }

    @Override
    public ListenableFuture<Duration> getStatus(String subscriptionId) {

        return executorService.get().submit(() -> {
            // Search for subscription to get status from
            SinkSubscriptionManager subMan = getSubscriptionManagerProxy(subscriptionId);
            RequestResponseClient subscriptionRequestResponseClient =
                    getSubscriptionRequestResponseClient(subscriptionId);

            GetStatus getStatus = wseFactory.createGetStatus();
            String subManAddress = wsaUtil.getAddressUri(subMan.getSubscriptionManagerEpr()).orElseThrow(() ->
                    new RuntimeException("No subscription manager EPR found"));

            // Create new message, put subscription manager EPR address as wsa:To
            SoapMessage getStatusMsg = soapUtil.createMessage(
                    WsEventingConstants.WSA_ACTION_GET_STATUS,
                    subManAddress,
                    getStatus,
                    subMan.getSubscriptionManagerEpr().getReferenceParameters()
            );

            // Invoke request-response
            SoapMessage getStatusResMsg = subscriptionRequestResponseClient.sendRequestResponse(getStatusMsg);
            GetStatusResponse getStatusResponse = soapUtil.getBody(getStatusResMsg, GetStatusResponse.class)
                    .orElseThrow(() ->
                            new MalformedSoapMessageException("WS-Eventing GetStatusResponse message is malformed"));

            // Parse expires in response message and return
            return getStatusResponse.getExpires();
        });
    }

    private SubscriptionData removeSubscriptionData(String subscriptionId) {
        final var subscription = removeSubscription(subscriptionId);
        if (subscription == null) {
            throw new SubscriptionNotFoundException("Subscription with id " + subscriptionId +  " not found");
        }
        return subscription;
    }

    @Override
    public ListenableFuture<?> unsubscribe(String subscriptionId) {
        LOG.debug("Unsubscribing for subscription {}", subscriptionId);

        final var removedData = removeSubscriptionData(subscriptionId);
        final SinkSubscriptionManager subMan = removedData.subscriptionManager;
        final RequestResponseClient subscriptionRequestResponseClient = removedData.requestResponseClient;

        var unsubscribeFuture = executorService.get().submit(() -> {
            Unsubscribe unsubscribe = wseFactory.createUnsubscribe();
            String subManAddress = wsaUtil.getAddressUri(subMan.getSubscriptionManagerEpr()).orElseThrow(() ->
                    new RuntimeException("No subscription manager EPR found"));

            // Create new message, put subscription manager EPR address as wsa:To
            SoapMessage unsubscribeMsg = soapUtil.createMessage(
                    WsEventingConstants.WSA_ACTION_UNSUBSCRIBE,
                    subManAddress,
                    unsubscribe,
                    subMan.getSubscriptionManagerEpr().getReferenceParameters()
            );

            // Invoke request-response and ignore result
            subscriptionRequestResponseClient.sendRequestResponse(unsubscribeMsg);

            // Remove context paths after unsubscribe response
            unregisterContextPaths(subMan);

            return new Object();
        });

        Futures.addCallback(unsubscribeFuture, new FutureCallback<>() {
            @Override
            public void onSuccess(Object ignored) {
                clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.End(
                    subscriptionId, null
                ));
            }

            @Override
            public void onFailure(Throwable t) {
                // Ensure context paths are removed after unsubscribe has failed
                unregisterContextPaths(subMan);

                clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.Failed(
                    subscriptionId,
                    "Error unsubscribing",
                    t
                ));

            }
        }, MoreExecutors.directExecutor());


        return unsubscribeFuture;
    }

    @Override
    public void unsubscribeAll() {
        for (SubscriptionData subData : new ArrayList<>(this.subscriptionData.values())) {
            final var subscriptionId = subData.subscriptionManager.getSubscriptionId();
            final ListenableFuture<?> future = unsubscribe(subscriptionId);
            try {
                future.get(maxWaitForFutures.toSeconds(), TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException e) {
                instanceLogger.warn("Subscription {} could not be unsubscribed. Ignore.",
                    subscriptionId);
                instanceLogger.trace("Subscription {} could not be unsubscribed",
                        subscriptionId, e);
            } catch (TimeoutException e) {
                instanceLogger.warn("Subscription {} could not be unsubscribed, timeout after {}s. Ignore.",
                    subscriptionId, maxWaitForFutures.toSeconds());
                instanceLogger.trace("Subscription {} could not be unsubscribed, timeout after {}s",
                    subscriptionId, maxWaitForFutures.toSeconds(), e);
                future.cancel(true);
            }
        }
    }

    private void unregisterUri(URI fullUri) {
        var uriWithoutPath = httpUriBuilder.buildUri(fullUri.getScheme(), fullUri.getHost(), fullUri.getPort());
        httpServerRegistry.unregisterContext(uriWithoutPath, fullUri.getPath());
    }

    private void processIncomingEndTo(
        InputStream inputStream,
        OutputStream outputStream,
        String subscriptionId
    ) throws HttpException {
        SoapMessage soapMsg;
        try {
            // unmarshal closes the input stream when it's done, no need to do it manually again
            // it only pollutes our catch with an IOException
            soapMsg = soapUtil.createMessage(marshalling.unmarshal(inputStream));
        } catch (JAXBException exception) {
            handleInboundException(
                exception,
                outputStream,
                "An error occurred during the processing of the inbound message",
                subscriptionId
            );
            return;
        }

        instanceLogger.debug("Received message to EndTo address: {}", soapMsg);

        // check if this is an EndTo
        final SubscriptionEnd subscriptionEnd;

        // try-catch so we have a cause for the event.
        try {
            subscriptionEnd = soapUtil.getBody(soapMsg, SubscriptionEnd.class).orElseThrow();
        } catch (NoSuchElementException exception) {
            handleInboundException(
                exception,
                outputStream,
                "Inbound message to EndTo endpoint did not represent a SubscriptionEnd message",
                subscriptionId
            );
            throw new HttpException(
                HttpStatus.BAD_REQUEST_400,
                "Received message to EndTo address that is not a SubscriptionEnd"
            );
        }

        processSubscriptionEnd(subscriptionEnd, subscriptionId, outputStream);
    }

    private void processSubscriptionEnd(
        SubscriptionEnd subscriptionEnd,
        String subscriptionId,
        OutputStream outputStream
    ) throws HttpException {

        final WsEventingStatus status;
        try {
            status = WsEventingStatus.fromString(subscriptionEnd.getStatus());
        } catch (IllegalArgumentException exception) {
            instanceLogger.error(
                "Incoming SubscriptionEnd presented an unknown status: {}",
                subscriptionEnd.getStatus(),
                exception
            );
            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.Failed(
                subscriptionId,
                "Processing incoming message on EndTo found an unknown status",
                exception
            ));
            return;
        }

        final SubscriptionData subData;
        try {
            subData = removeSubscriptionData(subscriptionId);
        } catch (SubscriptionNotFoundException exception) {
            instanceLogger.error("Error processing incoming message", exception);
            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.Failed(
                subscriptionId,
                "Processing incoming message on EndTo led to unknown subscription",
                exception
            ));
            return;
        }

        unregisterContextPaths(subData.subscriptionManager);

        clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.End(
            subscriptionId, status
        ));

        closeOutputStream(outputStream, subscriptionId);
    }

    private void unregisterContextPaths(SinkSubscriptionManager subscriptionManager) {

        var endToUri = subscriptionManager.getEndTo()
            .map(it -> it.getAddress().getValue())
            .map(URI::create);

        var notifyToUri = URI.create(
            subscriptionManager
                .getNotifyTo()
                .getAddress()
                .getValue()
        );

        endToUri.ifPresent(this::unregisterUri);
        unregisterUri(notifyToUri);
    }

    private void closeOutputStream(OutputStream outputStream, String subscriptionId) throws HttpException {
        try {
            outputStream.close();
        } catch (IOException exception) {
            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.MessageProcessingFailed(
                subscriptionId,
                "Closing the output stream for the response triggered an IOException",
                exception
            ));
            throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR_500, "Error closing output stream");
        }
    }

    private void processIncomingNotification(
        NotificationSink notificationSink,
        InputStream inputStream,
        OutputStream outputStream,
        CommunicationContext communicationContext,
        String subscriptionId
    )
            throws HttpException {
        SoapMessage soapMsg;
        try {
            // unmarshal closes the input stream when it's done, no need to do it manually again
            // it only pollutes our catch with an IOException
            soapMsg = soapUtil.createMessage(marshalling.unmarshal(inputStream));
        } catch (JAXBException exception) {
            handleInboundException(
                exception,
                outputStream,
                "An error occurred during the processing of the inbound message",
                subscriptionId
            );
            return;
        }

        instanceLogger.debug("Received incoming notification {}", soapMsg);

        try {
            notificationSink.receiveNotification(soapMsg, communicationContext);
        } catch (SoapFaultException e) {
            instanceLogger.error("Error processing incoming message", e);
            sendFaultResponse(e.getFaultMessage(), outputStream);
            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.MessageProcessingFailed(
                subscriptionId,
                "Processing incoming message triggered SOAP fault",
                e
            ));
            throw new HttpException(SoapFaultHttpStatusCodeMapping.get(e.getFault()));
            // CHECKSTYLE.OFF: IllegalCatch
        } catch (Exception e) {
            // CHECKSTYLE.ON: IllegalCatch
            clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.MessageProcessingFailed(
                subscriptionId,
                "Processing incoming message triggered exception",
                e
            ));
            throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR_500, extractExceptionMessage(e));
        }

        // Only close the output stream when the notification has been processed
        // as closing allows the server to dispatch the next request, which will cause concurrency problems
        // for the ultimate receiver of the notifications
        closeOutputStream(outputStream, subscriptionId);
    }

    // in case of JAXBException, e.getMessage() is null and the actual message is hidden in the linkedException
    private String extractExceptionMessage(Exception e) {
        return e.getMessage() != null ? e.getMessage() : e.toString();
    }

    private void handleInboundException(
        Exception exception,
        OutputStream outputStream,
        String rationale,
        String subscriptionId
    ) throws HttpException {
        LOG.error("Error processing incoming message for subscription {}", subscriptionId, exception);
        clientEventObserver.onSubscriptionEvent(new SubscriptionEvent.MessageProcessingFailed(
            subscriptionId,
            rationale,
            exception
        ));
        final var faultMessageToSent = soapFaultFactory.createFault(
            WsAddressingConstants.FAULT_ACTION,
            SoapConstants.SENDER,
            SoapConstants.DEFAULT_SUBCODE,
            rationale
        );
        sendFaultResponse(
            faultMessageToSent,
            outputStream
        );

        final var fault = soapUtil.getBody(faultMessageToSent, Fault.class)
            .orElseThrow(); // must be available

        throw new HttpException(SoapFaultHttpStatusCodeMapping.get(fault));
    }

    private void sendFaultResponse(SoapMessage soapFault, OutputStream outputStream) throws HttpException {
        try {
            marshalling.marshal(soapFault.getEnvelopeWithMappedHeaders(), outputStream);
        } catch (JAXBException exception) {
            // At this point, we've been unable to produce a proper SOAP fault to send to the client.
            // We need to accept our own shortcomings and send a generic 500 error.
            throw new HttpException(HttpStatus.INTERNAL_SERVER_ERROR_500, extractExceptionMessage(exception));
        } finally {
            try {
                // if we can't close it, there is nothing for us to do, who are we gonna tell? the server?
                outputStream.close();
            } catch (IOException e) {
                instanceLogger.error("Error closing output stream while sending SOAP fault", e);
            }
        }
    }


    private SinkSubscriptionManager getSubscriptionManagerProxy(String subscriptionId) {
        subscriptionsLock.lock();
        try {
            return Optional.ofNullable(subscriptionData.get(subscriptionId))
                    .orElseThrow(SubscriptionNotFoundException::new).subscriptionManager;
        } finally {
            subscriptionsLock.unlock();
        }
    }

    private RequestResponseClient getSubscriptionRequestResponseClient(String subscriptionId) {
        subscriptionsLock.lock();
        try {
            return Optional.ofNullable(subscriptionData.get(subscriptionId))
                .orElseThrow(SubscriptionRequestResponseClientNotFoundException::new).requestResponseClient;
        } finally {
            subscriptionsLock.unlock();
        }
    }

    private @Nullable SubscriptionData removeSubscription(
        String subscriptionId
    ) {
        subscriptionsLock.lock();
        try {
            return subscriptionData.remove(subscriptionId);
        } finally {
            subscriptionsLock.unlock();
        }
    }
}
