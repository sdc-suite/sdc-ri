package org.somda.sdc.dpws.http.apache

import org.apache.commons.logging.Log
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.config.MessageConstraints
import org.apache.http.entity.ContentLengthStrategy
import org.apache.http.impl.conn.DefaultManagedHttpClientConnection
import org.apache.http.impl.conn.Wire
import org.apache.http.io.HttpMessageParserFactory
import org.apache.http.io.HttpMessageWriterFactory
import org.apache.logging.log4j.kotlin.KotlinLogger
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.dpws.http.HttpConnectionInterceptor
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.nio.charset.CharsetDecoder
import java.nio.charset.CharsetEncoder
import java.util.*


/**
 * This class is an almost identical copy of [org.apache.http.impl.conn.LoggingManagedHttpClientConnection]. It extends
 * [org.apache.http.impl.conn.DefaultManagedHttpClientConnection] because LoggingManagedHttpClientConnection
 * is package-private.
 * Changes:
 * - calls httpConnectionInterceptor.onLastIntercept() in close()
 */
@Suppress("LongParameterList") // code adapted from Apache kept similar
internal class MyApacheDefaultManagedHttpClientConnection(
    id: String,
    private val headerLog: KotlinLogger,
    wireLog: Log,
    bufferSize: Int,
    fragmentSizeHint: Int,
    charDecoder: CharsetDecoder?,
    charEncoder: CharsetEncoder?,
    constraints: MessageConstraints?,
    incomingContentStrategy: ContentLengthStrategy,
    outgoingContentStrategy: ContentLengthStrategy,
    requestWriterFactory: HttpMessageWriterFactory<HttpRequest?>,
    responseParserFactory: HttpMessageParserFactory<HttpResponse?>,
    private val httpConnectionInterceptor: HttpConnectionInterceptor
) : DefaultManagedHttpClientConnection(
    id, bufferSize, fragmentSizeHint, charDecoder, charEncoder, constraints, incomingContentStrategy,
    outgoingContentStrategy, requestWriterFactory, responseParserFactory
), Logging {
    private val wire = Wire(wireLog, id)

    @Throws(IOException::class)
    override fun close() {
        httpConnectionInterceptor.onLastIntercept(UUID.fromString(id))

        if (super.isOpen()) {
            logger.debug { "$id: Close connection" }
            super.close()
        }
    }

    override fun setSocketTimeout(timeout: Int) {
        logger.debug { "$id: set socket timeout to $timeout" }
        super.setSocketTimeout(timeout)
    }

    @Throws(IOException::class)
    override fun shutdown() {
        logger.debug { "$id: Shutdown connection" }
        super.shutdown()
    }

    @Throws(IOException::class)
    override fun getSocketInputStream(socket: Socket?): InputStream {
        var input = super.getSocketInputStream(socket)
        if (wire.enabled()) {
            input = MyApacheLoggingInputStream(input, this.wire)
        }
        return input
    }

    @Throws(IOException::class)
    override fun getSocketOutputStream(socket: Socket?): OutputStream {
        var out = super.getSocketOutputStream(socket)
        if (wire.enabled()) {
            out = MyApacheLoggingOutputStream(out, this.wire)
        }
        return out
    }

    override fun onResponseReceived(response: HttpResponse?) {
        if (response != null && headerLog.delegate.isDebugEnabled) {
            headerLog.debug { "$id << ${response.statusLine}" }
            val headers = response.allHeaders
            for (header in headers) {
                headerLog.debug { "$id << $header" }
            }
        }
    }

    override fun onRequestSubmitted(request: HttpRequest?) {
        if (request != null && headerLog.delegate.isDebugEnabled) {
            headerLog.debug { "$id >> ${request.requestLine}" }
            val headers = request.allHeaders
            for (header in headers) {
                headerLog.debug { "$id >> $header" }
            }
        }
    }
}
