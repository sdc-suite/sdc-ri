package org.somda.sdc.dpws.http.apache;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpClientConnection;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ManagedHttpClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.ManagedHttpClientConnectionFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.entity.LaxContentLengthStrategy;
import org.apache.http.impl.entity.StrictContentLengthStrategy;
import org.apache.http.impl.io.DefaultHttpRequestWriterFactory;
import org.apache.http.impl.io.DefaultHttpResponseParserFactory;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.kotlin.KotlinLogger;
import org.apache.logging.log4j.kotlin.LoggingFactoryKt;
import org.jspecify.annotations.Nullable;
import org.somda.sdc.common.CommonConfig;
import org.somda.sdc.common.logging.InstanceLogger;
import org.somda.sdc.dpws.CommunicationLog;
import org.somda.sdc.dpws.CommunicationLogContext;
import org.somda.sdc.dpws.DpwsConfig;
import org.somda.sdc.dpws.DpwsConstants;
import org.somda.sdc.dpws.TransportBinding;
import org.somda.sdc.dpws.crypto.CryptoConfig;
import org.somda.sdc.dpws.crypto.CryptoConfigurator;
import org.somda.sdc.dpws.crypto.CryptoSettings;
import org.somda.sdc.dpws.factory.CommunicationLogFactory;
import org.somda.sdc.dpws.factory.TransportBindingFactory;
import org.somda.sdc.dpws.http.HttpConnectionInterceptor;
import org.somda.sdc.dpws.http.factory.HttpClientFactory;
import org.somda.sdc.dpws.soap.SoapMarshalling;
import org.somda.sdc.dpws.soap.SoapUtil;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Factory for creating apache http client backed transport bindings and clients.
 */
public class ApacheTransportBindingFactoryImpl implements TransportBindingFactory, HttpClientFactory {

    private static final Logger LOG = LogManager.getLogger(ApacheTransportBindingFactoryImpl.class);

    private static final String SCHEME_SOAP_OVER_UDP = DpwsConstants.URI_SCHEME_SOAP_OVER_UDP;
    private static final String SCHEME_HTTP = "http";
    private static final String SCHEME_HTTPS = "https";

    private final SoapMarshalling marshalling;
    private final SoapUtil soapUtil;
    private final boolean enableGzipCompression;
    private final Duration clientConnectTimeout;
    private final Duration clientReadTimeout;
    private final HttpClient client;
    private final String[] tlsProtocols;
    private final HostnameVerifier hostnameVerifier;
    private final String[] enabledCiphers;
    private final boolean enableHttps;
    private final boolean enableHttp;
    private final CryptoConfigurator cryptoConfigurator;
    @Nullable
    private final CryptoSettings cryptoSettings;
    private final Logger instanceLogger;
    private final String frameworkIdentifier;
    private final int clientPoolSize;
    private final boolean retryNonIdempotent;
    private final HttpConnectionInterceptor httpConnectionInterceptor;
    private final PoolCleaner poolCleaner;

    private final ClientTransportBindingFactory clientTransportBindingFactory;

    @Inject
    ApacheTransportBindingFactoryImpl(SoapMarshalling marshalling,
                                      SoapUtil soapUtil,
                                      CryptoConfigurator cryptoConfigurator,
                                      @Nullable @Named(CryptoConfig.CRYPTO_SETTINGS) CryptoSettings cryptoSettings,
                                      @Named(DpwsConfig.HTTP_CLIENT_CONNECT_TIMEOUT) Duration clientConnectTimeout,
                                      @Named(DpwsConfig.HTTP_CLIENT_READ_TIMEOUT) Duration clientReadTimeout,
                                      @Named(DpwsConfig.HTTP_CLIENT_POOL_SIZE) int clientPoolSize,
                                      @Named(DpwsConfig.HTTP_CLIENT_RETRY_POST) boolean retryNonIdempotent,
                                      @Named(DpwsConfig.HTTP_GZIP_COMPRESSION) boolean enableGzipCompression,
                                      ClientTransportBindingFactory clientTransportBindingFactory,
                                      CommunicationLogFactory communicationLogFactory,
                                      @Named(CryptoConfig.CRYPTO_TLS_ENABLED_VERSIONS) String[] tlsProtocols,
                                      @Named(CryptoConfig.CRYPTO_TLS_ENABLED_CIPHERS) String[] enabledCiphers,
                                      @Named(CryptoConfig.CRYPTO_CLIENT_HOSTNAME_VERIFIER)
                                      HostnameVerifier hostnameVerifier,
                                      @Named(DpwsConfig.HTTPS_SUPPORT) boolean enableHttps,
                                      @Named(DpwsConfig.HTTP_SUPPORT) boolean enableHttp,
                                      @Named(CommonConfig.INSTANCE_IDENTIFIER) String frameworkIdentifier,
                                      @Named(DpwsConfig.HTTP_CONNECTION_INTERCEPTOR)
                                      HttpConnectionInterceptor httpConnectionInterceptor,
                                      PoolCleaner poolCleaner
    ) {
        this.cryptoConfigurator = cryptoConfigurator;
        this.cryptoSettings = cryptoSettings;
        this.instanceLogger = InstanceLogger.wrapLogger(LOG, frameworkIdentifier);
        this.frameworkIdentifier = frameworkIdentifier;
        this.marshalling = marshalling;
        this.soapUtil = soapUtil;
        this.clientConnectTimeout = clientConnectTimeout;
        this.clientReadTimeout = clientReadTimeout;
        this.enableGzipCompression = enableGzipCompression;
        this.clientTransportBindingFactory = clientTransportBindingFactory;
        this.tlsProtocols = tlsProtocols;
        this.enabledCiphers = enabledCiphers;
        this.hostnameVerifier = hostnameVerifier;
        this.enableHttps = enableHttps;
        this.enableHttp = enableHttp;
        this.clientPoolSize = clientPoolSize;
        this.retryNonIdempotent = retryNonIdempotent;
        this.httpConnectionInterceptor = httpConnectionInterceptor;
        this.poolCleaner = poolCleaner;

        if (!this.enableHttp && !this.enableHttps) {
            throw new RuntimeException("Http and https are disabled, cannot continue");
        }

        this.client = buildClient(cryptoConfigurator, cryptoSettings, communicationLogFactory.createCommunicationLog());
    }

    private HttpClient buildClient(CryptoConfigurator cryptoConfigurator,
                                   @Nullable CryptoSettings cryptoSettings,
                                   CommunicationLog communicationLog) {
        var socketConfig = SocketConfig.custom().setTcpNoDelay(true).build();

        // set the timeout for all requests
        var requestConfig = RequestConfig.custom().setConnectionRequestTimeout((int) clientReadTimeout.toMillis())
                .setConnectTimeout((int) clientConnectTimeout.toMillis())
                .setSocketTimeout((int) clientConnectTimeout.toMillis()).build();

        var clientBuilder = HttpClients.custom().setDefaultSocketConfig(socketConfig)
                // attach interceptors to enable communication log capabilities including message headers
                .addInterceptorLast(new CommunicationLogHttpRequestInterceptor(communicationLog, frameworkIdentifier,
                        cryptoConfigurator.getCertificates(cryptoSettings)))
                .addInterceptorLast(new CommunicationLogHttpResponseInterceptor(communicationLog, frameworkIdentifier))
                .addInterceptorLast((HttpResponse response, HttpContext context) -> {
                    HttpClientContext clientContext = HttpClientContext.adapt(context);
                    ManagedHttpClientConnection connection =
                            clientContext.getConnection(ManagedHttpClientConnection.class);
                    if (connection != null && connection.isOpen()) {
                        var connectionId = UUID.fromString(connection.getId());
                        if (connection.getMetrics() != null && connection.getMetrics().getRequestCount() == 1
                                && connection.getSSLSession() != null) {
                            httpConnectionInterceptor.onFirstIntercept(connectionId, connection.getSSLSession(),
                                    HttpConnectionInterceptor.Source.CLIENT);
                        }
                        httpConnectionInterceptor.onIntercept(connectionId);
                    }
                })
                .setDefaultRequestConfig(requestConfig)
                // allow reusing ssl connections in the pool
                .disableConnectionState()
                // retry every request just once in case the socket has died
                .setRetryHandler(new CustomHttpRetryHandler(1, retryNonIdempotent, httpConnectionInterceptor));
        if (!enableGzipCompression) {
            // disable gzip compression
            clientBuilder.disableContentCompression();
        }

        var registryBuilder = RegistryBuilder.<ConnectionSocketFactory>create();

        if (enableHttps) {
            SSLContext sslContext;
            try {
                sslContext = cryptoConfigurator.createSslContextFromCryptoConfig(cryptoSettings);
                // CHECKSTYLE.OFF: IllegalCatch
            } catch (Exception e) {
                // CHECKSTYLE.ON: IllegalCatch
                instanceLogger.error("Could not read client crypto config, fallback to system properties", e);
                sslContext = cryptoConfigurator.createSslContextFromSystemProperties();
            }

            instanceLogger.debug("Enabled protocols: {}", () -> List.of(tlsProtocols));
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
                    sslContext,
                    tlsProtocols,
                    enabledCiphers,
                    hostnameVerifier
            );

            registryBuilder.register(SCHEME_HTTPS, socketFactory);
        }

        if (enableHttp) {
            registryBuilder.register(SCHEME_HTTP, PlainConnectionSocketFactory.getSocketFactory());
        }

        final PoolingHttpClientConnectionManager poolingConnectionManager = new PoolingHttpClientConnectionManager(
                registryBuilder.build(),
                getConnectionFactory(),
                null,
                null,
                -1,
                TimeUnit.MILLISECONDS
        );

        // make pool cleaner aware of the new pool
        poolCleaner.addPool(poolingConnectionManager);

        // only allow one connection per host
        poolingConnectionManager.setDefaultMaxPerRoute(1);
        poolingConnectionManager.setMaxTotal(clientPoolSize);

        // Wrap the response entity to enforce streaming behavior.
        // This allows the HttpConnectionInterceptor to receive a proper connection, not a "detached" connection,
        // which do not allow requesting the remote certificates or other connection information.
        // See also sdc-suite/sdc-ri#319
        var requestExecutor = new HttpRequestExecutor() {
            @Override
            public HttpResponse execute(HttpRequest request, HttpClientConnection conn, HttpContext context)
                throws IOException, HttpException {
                var response = super.execute(request, conn, context);

                if (response.getEntity() != null && !response.getEntity().isStreaming()) {
                    response.setEntity(new HttpEntityWrapper(response.getEntity()) {
                        @Override
                        public boolean isStreaming() {
                            return true;
                        }
                    });
                }
                return response;
            }
        };

        return clientBuilder
            .setRequestExecutor(requestExecutor)
            .setConnectionManager(poolingConnectionManager)
            .build();
    }

    private ManagedHttpClientConnectionFactory getConnectionFactory() {
        return new ManagedHttpClientConnectionFactory() {
            private final KotlinLogger headerLog = LoggingFactoryKt.logger("org.apache.http.headers");
            private final Log wireLog = LogFactory.getLog("org.apache.http.wire");

            /*
             * An almost identical copy of ManagedHttpClientConnectionFactory#create().
             * Changes:
             * - id is now a UUID
             * - returns MyApacheDefaultManagedHttpClientConnection
             */
            @Override
            public ManagedHttpClientConnection create(HttpRoute route, ConnectionConfig config) {
                final ConnectionConfig cconfig = config != null ? config : ConnectionConfig.DEFAULT;
                CharsetDecoder charDecoder = null;
                CharsetEncoder charEncoder = null;
                final Charset charset = cconfig.getCharset();
                final CodingErrorAction malformedInputAction = cconfig.getMalformedInputAction() != null ?
                        cconfig.getMalformedInputAction() : CodingErrorAction.REPORT;
                final CodingErrorAction unmappableInputAction = cconfig.getUnmappableInputAction() != null ?
                        cconfig.getUnmappableInputAction() : CodingErrorAction.REPORT;
                if (charset != null) {
                    charDecoder = charset.newDecoder();
                    charDecoder.onMalformedInput(malformedInputAction);
                    charDecoder.onUnmappableCharacter(unmappableInputAction);
                    charEncoder = charset.newEncoder();
                    charEncoder.onMalformedInput(malformedInputAction);
                    charEncoder.onUnmappableCharacter(unmappableInputAction);
                }
                final String id = UUID.randomUUID().toString();

                return new MyApacheDefaultManagedHttpClientConnection(
                        id,
                        headerLog,
                        wireLog,
                        cconfig.getBufferSize(),
                        cconfig.getFragmentSizeHint(),
                        charDecoder,
                        charEncoder,
                        cconfig.getMessageConstraints(),
                        LaxContentLengthStrategy.INSTANCE,
                        StrictContentLengthStrategy.INSTANCE,
                        DefaultHttpRequestWriterFactory.INSTANCE,
                        DefaultHttpResponseParserFactory.INSTANCE,
                        httpConnectionInterceptor);
            }
        };
    }

    @Override
    public TransportBinding createTransportBinding(
            String endpointUri,
            @Nullable CommunicationLogContext communicationLogContext
    ) throws UnsupportedOperationException {
        // To keep things simple, this method directly checks if there is a SOAP-UDP or
        // HTTP(S) binding
        // No plug-and-play feature is implemented that dispatches, based on the URI
        // scheme, to endpoint processor
        // factories

        String scheme = URI.create(endpointUri).getScheme();
        if (scheme.equalsIgnoreCase(SCHEME_SOAP_OVER_UDP)) {
            throw new UnsupportedOperationException(
                    "SOAP-over-UDP is currently not supported by the TransportBindingFactory");
        } else if (scheme.equalsIgnoreCase(SCHEME_HTTP)) {
            return createHttpBinding(endpointUri, communicationLogContext);
        } else if (scheme.equalsIgnoreCase(SCHEME_HTTPS)) {
            return createHttpBinding(endpointUri, communicationLogContext);
        } else {
            throw new UnsupportedOperationException(
                    String.format("Unsupported transport binding requested: %s", scheme));
        }
    }

    @Override
    public TransportBinding createHttpBinding(
            String endpointUri,
            @Nullable CommunicationLogContext communicationLogContext
    ) throws UnsupportedOperationException {
        var scheme = URI.create(endpointUri).getScheme();
        if (scheme.toLowerCase().startsWith("http")) {
            return this.clientTransportBindingFactory.create(
                    this.client,
                    endpointUri,
                    marshalling,
                    soapUtil,
                    communicationLogContext
            );
        }

        throw new UnsupportedOperationException(
                String.format("Binding with scheme %s is currently not supported", scheme));
    }

    @Override
    public org.somda.sdc.dpws.http.HttpClient createHttpClient() {
        return this.clientTransportBindingFactory.createHttpClient(client);
    }

    /**
     * Access the configured apache http client.
     * <p>
     * Note: <em>Do not</em> use this client for productive purposes, always use the {@linkplain TransportBinding}
     * instead. This is only useful if you want to send intentionally bad messages to a server, which you most likely
     * do not want.
     *
     * @return the configured apache http client
     */
    public HttpClient getClient() {
        return client;
    }
}
