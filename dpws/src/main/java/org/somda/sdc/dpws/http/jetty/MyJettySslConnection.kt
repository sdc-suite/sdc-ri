package org.somda.sdc.dpws.http.jetty

import org.eclipse.jetty.io.ByteBufferPool
import org.eclipse.jetty.io.EndPoint
import org.eclipse.jetty.io.RetainableByteBufferPool
import org.eclipse.jetty.io.ssl.SslConnection
import java.util.UUID
import java.util.concurrent.Executor
import javax.net.ssl.SSLEngine


@Suppress("LongParameterList") // code adapted from Jetty kept similar
internal class MyJettySslConnection(
    retainableByteBufferPool: RetainableByteBufferPool?,
    byteBufferPool: ByteBufferPool?,
    executor: Executor?,
    endPoint: EndPoint?,
    sslEngine: SSLEngine?,
    useDirectBuffersForEncryption: Boolean,
    useDirectBuffersForDecryption: Boolean
) : SslConnection(
    retainableByteBufferPool,
    byteBufferPool,
    executor,
    endPoint,
    sslEngine,
    useDirectBuffersForEncryption,
    useDirectBuffersForDecryption
) {
    val connectionId: UUID = UUID.randomUUID()
}
