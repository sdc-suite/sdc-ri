package org.somda.sdc.dpws.client

import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.Service
import com.google.common.util.concurrent.SettableFuture
import com.google.inject.Inject
import com.google.inject.name.Named
import org.apache.logging.log4j.kotlin.KotlinLogger
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.common.CommonConfig
import org.somda.sdc.common.event.EventBus
import org.somda.sdc.common.logging.InstanceLogger
import org.somda.sdc.common.util.ExecutorWrapperService
import org.somda.sdc.dpws.DpwsConfig
import org.somda.sdc.dpws.client.helper.DiscoveryClientUdpProcessor
import org.somda.sdc.dpws.client.helper.HelloByeAndProbeMatchesObserverImpl
import org.somda.sdc.dpws.client.helper.HostingServiceResolver
import org.somda.sdc.dpws.client.helper.factory.ClientHelperFactory
import org.somda.sdc.dpws.factory.TransportBindingFactory
import org.somda.sdc.dpws.guice.ClientSpecific
import org.somda.sdc.dpws.guice.DiscoveryUdpQueue
import org.somda.sdc.dpws.guice.NetworkJobThreadPool
import org.somda.sdc.dpws.helper.factory.DpwsHelperFactory
import org.somda.sdc.dpws.service.HostingServiceProxy
import org.somda.sdc.dpws.soap.exception.MarshallingException
import org.somda.sdc.dpws.soap.exception.TransportException
import org.somda.sdc.dpws.soap.factory.NotificationSinkFactory
import org.somda.sdc.dpws.soap.factory.NotificationSourceFactory
import org.somda.sdc.dpws.soap.factory.RequestResponseClientFactory
import org.somda.sdc.dpws.soap.interception.InterceptorException
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingServerInterceptor
import org.somda.sdc.dpws.soap.wsaddressing.WsAddressingUtil
import org.somda.sdc.dpws.soap.wsdiscovery.HelloByeAndProbeMatchesObserver
import org.somda.sdc.dpws.soap.wsdiscovery.WsDiscoveryClient
import org.somda.sdc.dpws.soap.wsdiscovery.factory.WsDiscoveryClientFactory
import org.somda.sdc.dpws.soap.wsdiscovery.model.ProbeMatchesType
import org.somda.sdc.dpws.soap.wsdiscovery.model.ResolveMatchesType
import org.somda.sdc.dpws.udp.UdpMessageQueueService
import java.time.Duration
import java.util.concurrent.CancellationException
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Default implementation of [Client].
 */
// parameters are injected
// functions are all from an interface
@Suppress(
    "LongParameterList",
    "TooManyFunctions"
)
class ClientImpl @Inject internal constructor(
    @param:Named(DpwsConfig.MAX_WAIT_FOR_FUTURES) private val maxWaitForFutures: Duration,
    discoveryClientFactory: WsDiscoveryClientFactory,
    notificationSourceFactory: NotificationSourceFactory,
    dpwsHelperFactory: DpwsHelperFactory,
    @param:DiscoveryUdpQueue private val discoveryMessageQueue: UdpMessageQueueService,
    notificationSinkFactory: NotificationSinkFactory,
    clientHelperFactory: ClientHelperFactory,
    @param:NetworkJobThreadPool private val executorService: ExecutorWrapperService<ListeningExecutorService>,
    private val wsAddressingUtil: WsAddressingUtil,
    private val transportBindingFactory: TransportBindingFactory,
    private val requestResponseClientFactory: RequestResponseClientFactory,
    private val hostingServiceResolver: HostingServiceResolver,
    private val clientEventBus: EventBus,
    @ClientSpecific wsAddressingServerInterceptor: WsAddressingServerInterceptor,
    @Named(CommonConfig.INSTANCE_IDENTIFIER) frameworkIdentifier: String?
) :
    AbstractIdleService(), Client, Service,
    HelloByeAndProbeMatchesObserver {
    private val msgProcessor: DiscoveryClientUdpProcessor
    private val helloByeAndProbeMatchesObserverImpl: HelloByeAndProbeMatchesObserverImpl
    private val wsDiscoveryClient: WsDiscoveryClient
    private val instanceLogger: KotlinLogger = InstanceLogger.wrapLogger(logger, frameworkIdentifier)

    // forwards client events through the event bus
    private val clientEventForwarder = object: EventObserver {
        override fun onSubscriptionEvent(event: Subscription) {
            this@ClientImpl.clientEventBus.post(event)
        }
    }

    init {

        // Create binding between a notification source and outgoing UDP messages to send probes and resolves
        val callback =
            dpwsHelperFactory.createNotificationSourceUdpCallback(discoveryMessageQueue)
        // Bind the callback to a notification source
        val notificationSource = notificationSourceFactory.createNotificationSource(callback)
        // Connect that notification source to a WS-Discovery client
        wsDiscoveryClient = discoveryClientFactory.createWsDiscoveryClient(notificationSource)

        // Create notification sink for WS-Discovery dedicated to the client side
        val notificationSink = notificationSinkFactory.createNotificationSink(wsAddressingServerInterceptor)

        // Create binding between a notification sink and incoming UDP messages to receive hello, bye, probeMatches and
        // resolveMatches
        msgProcessor = clientHelperFactory.createDiscoveryClientUdpProcessor(notificationSink)

        // Bind the notification sink to the WS-Discovery client that was created before
        notificationSink.register(wsDiscoveryClient)

        // Create device resolver proxy util object
        val discoveredDeviceResolver = clientHelperFactory
            .createDiscoveredDeviceResolver(wsDiscoveryClient)

        // Create observer for WS-Discovery messages
        helloByeAndProbeMatchesObserverImpl = clientHelperFactory.createDiscoveryObserver(discoveredDeviceResolver)
    }

    @Throws(TransportException::class, InterceptorException::class)
    override fun probe(discoveryFilter: DiscoveryFilter) {
        checkRunning()

        try {
            wsDiscoveryClient.sendProbe(
                discoveryFilter.discoveryId, discoveryFilter.types,
                discoveryFilter.scopes, discoveryFilter.matchBy
            )
        } catch (e: MarshallingException) {
            instanceLogger.error("Marshalling failed while probing for devices", e.cause)
        }
    }

    override fun directedProbe(xAddr: String): ListenableFuture<ProbeMatchesType> {
        checkRunning()

        val tBinding = transportBindingFactory.createTransportBinding(xAddr, null)
        val rrc = requestResponseClientFactory.createRequestResponseClient(tBinding)
        return wsDiscoveryClient.sendDirectedProbe(rrc, ArrayList(), ArrayList(), null)
    }

    @Throws(InterceptorException::class)
    override fun resolve(eprAddress: String): ListenableFuture<DiscoveredDevice> {
        checkRunning()

        val returnFuture = SettableFuture.create<DiscoveredDevice>()

        try {
            val resolveMatchesFuture = wsDiscoveryClient
                .sendResolve(wsAddressingUtil.createEprWithAddress(eprAddress))
            Futures.addCallback(resolveMatchesFuture, object : FutureCallback<ResolveMatchesType> {
                override fun onSuccess(resolveMatchesType: ResolveMatchesType?) {
                    if (resolveMatchesType == null) {
                        instanceLogger.warn("Received ResolveMatches with empty payload")
                    } else {
                        val rm = resolveMatchesType.resolveMatch
                        val scopes = rm.scopes?.value ?: emptyList()
                        returnFuture.set(
                            DiscoveredDevice(
                                rm.endpointReference.address.value,
                                rm.types,
                                scopes,
                                rm.xAddrs
                            )
                        )
                    }
                }

                override fun onFailure(throwable: Throwable) {
                    instanceLogger.trace(throwable)  { "Resolve failed." }
                    returnFuture.setException(throwable)
                }
            }, executorService.get())
        } catch (e: MarshallingException) {
            instanceLogger.warn(e.cause) { "Marshalling failed while probing for devices" }
            returnFuture.setException(e)
        } catch (e: TransportException) {
            instanceLogger.warn(e.cause) { "Sending failed on transport layer" }
            returnFuture.setException(e)
        }

        return returnFuture
    }

    override fun connect(discoveredDevice: DiscoveredDevice): ListenableFuture<HostingServiceProxy> {
        checkRunning()
        return hostingServiceResolver.resolveHostingService(discoveredDevice, clientEventForwarder)
    }

    @Throws(InterceptorException::class)
    override fun connect(eprAddress: String): ListenableFuture<HostingServiceProxy> {
        checkRunning()

        val resolveFuture = resolve(eprAddress)
        val hspFuture = SettableFuture.create<HostingServiceProxy>()

        Futures.addCallback(resolveFuture, object : FutureCallback<DiscoveredDevice?> {
            override fun onSuccess(discoveredDevice: DiscoveredDevice?) {
                checkNotNull(discoveredDevice) {
                    "Resolve of $eprAddress failed"
                }

                val connectFuture = connect(discoveredDevice)
                try {
                    hspFuture.set(connectFuture[maxWaitForFutures.toMillis(), TimeUnit.MILLISECONDS])
                } catch (e: TimeoutException) {
                    // cancel task on timeout
                    connectFuture.cancel(true)
                    val errorMessage = "Connecting to $eprAddress timed out " +
                        "after $maxWaitForFutures.toSeconds() seconds"
                    instanceLogger.debug(e) { errorMessage }
                    throw e
                } catch (e: InterruptedException) {
                    instanceLogger.debug(e) { "Connecting to $eprAddress failed" }
                    throw e
                } catch (e: ExecutionException) {
                    instanceLogger.debug(e) { "Connecting to $eprAddress failed" }
                    throw e
                } catch (e: CancellationException) {
                    instanceLogger.debug(e) { "Connecting to $eprAddress failed" }
                    throw e
                }
            }

            override fun onFailure(throwable: Throwable) {
                instanceLogger.trace(throwable) { "Connecting to endpoint $eprAddress failed" }
                hspFuture.setException(throwable)
            }
        }, executorService.get())

        return hspFuture
    }

    override fun startUp() {
        discoveryMessageQueue.registerUdpMessageQueueObserver(msgProcessor)
        wsDiscoveryClient.registerHelloByeAndProbeMatchesObserver(helloByeAndProbeMatchesObserverImpl)
    }

    override fun shutDown() {
        wsDiscoveryClient.unregisterHelloByeAndProbeMatchesObserver(helloByeAndProbeMatchesObserverImpl)
        discoveryMessageQueue.unregisterUdpMessageQueueObserver(msgProcessor)
    }


    override fun registerDiscoveryObserver(observer: DiscoveryObserver) {
        helloByeAndProbeMatchesObserverImpl.registerDiscoveryObserver(observer)
    }

    override fun unregisterDiscoveryObserver(observer: DiscoveryObserver) {
        helloByeAndProbeMatchesObserverImpl.unregisterDiscoveryObserver(observer)
    }

    private fun checkRunning() {
        check(isRunning) {
            instanceLogger.warn { "Try to invoke method on non-running client" }
        }
    }

    override fun registerEventObserver(eventObserver: EventObserver) {
        clientEventBus.register(eventObserver)
    }

    override fun unregisterEventObserver(eventObserver: EventObserver) {
        clientEventBus.unregister(eventObserver)
    }

    companion object: Logging
}
