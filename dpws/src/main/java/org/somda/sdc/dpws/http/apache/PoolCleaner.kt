package org.somda.sdc.dpws.http.apache

import com.google.common.util.concurrent.AbstractIdleService
import com.google.inject.Inject
import com.google.inject.Singleton
import com.google.inject.name.Named
import org.apache.http.conn.HttpClientConnectionManager
import org.apache.logging.log4j.kotlin.Logging
import org.somda.sdc.dpws.DpwsConfig
import org.somda.sdc.dpws.guice.DaemonTimer
import java.lang.ref.WeakReference
import java.util.Timer
import java.util.TimerTask
import java.util.concurrent.TimeUnit
import kotlin.concurrent.scheduleAtFixedRate
import kotlin.time.Duration
import kotlin.time.measureTime
import kotlin.time.toKotlinDuration

/**
 * Cleans the pools of [HttpClientConnectionManager]s with configured periods and timeouts.
 *
 * This is necessary to ensure we always close our clients at some point, triggering the
 * [onLastIntercept][org.somda.sdc.dpws.http.HttpConnectionInterceptor.onLastIntercept] callback.
 */
@Singleton
class PoolCleaner @Inject internal constructor(
    @Named(DpwsConfig.HTTP_CLIENT_POOL_IDLE_TIMEOUT) private val poolIdleTimeout: java.time.Duration,
    @Named(DpwsConfig.HTTP_CLIENT_POOL_CLEANUP_PERIOD) private val cleanupPeriod: java.time.Duration,
    @DaemonTimer private val timer: Timer,
) : AbstractIdleService() {

    private val knownPools = mutableSetOf<WeakReference<HttpClientConnectionManager>>()
    private lateinit var timerTask: TimerTask

    fun addPool(pool: HttpClientConnectionManager) {
        synchronized(this) {
            knownPools.add(WeakReference(pool))
        }
    }

    override fun startUp() {
        logger.info {
            "Starting pool cleaner with a cleanup period of $cleanupPeriod and an idle timeout of $poolIdleTimeout."
        }
        scheduledClean(cleanupPeriod.toKotlinDuration(), poolIdleTimeout.toKotlinDuration())
    }

    override fun shutDown() {
        logger.info { "Stopping pool cleaner." }
        timerTask.cancel()
    }

    private fun scheduledClean(period: Duration, idleTimeout: Duration) {
        timerTask = timer.scheduleAtFixedRate(
            // delay first eviction by period, there won't be any pools to clean
            period.inWholeMilliseconds,
            period.inWholeMilliseconds,
        ) {
            clean(idleTimeout)
        }
    }

    private fun clean(idleTimeout: Duration) {
        logger.debug { "Cleaning the pool." }
        val elapsed = measureTime {
            synchronized(this) {
                // filter dead references
                cleanDeadPools()
                // clean pools
                knownPools.forEach {
                    it.get()?.let { pool ->
                        pool.closeExpiredConnections()
                        pool.closeIdleConnections(idleTimeout.inWholeMilliseconds, TimeUnit.MILLISECONDS)
                    }
                }
            }
        }
        logger.debug { "Pool cleaning took '$elapsed'." }
    }


    private fun cleanDeadPools() {
        knownPools.removeIf { it.get() == null }
    }


    companion object : Logging
}
