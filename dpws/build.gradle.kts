plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.lint-checkstyle")
    id("buildlogic.kotlin-conventions")
    id("buildlogic.deploy")
    `java-test-fixtures`
}

dependencies {
    api(libs.org.somda.sdc.dpws.model)
    api(projects.common)
    api(libs.org.apache.httpcomponents.httpclient)
    api(libs.commons.codec.commons.codec)
    api(libs.commons.io.commons.io)
    api(libs.org.eclipse.jetty.jetty.server)
    api(libs.org.eclipse.jetty.jetty.servlet)
    api(libs.org.eclipse.jetty.jetty.jmx)
    api(libs.net.sourceforge.jregex.jregex)
    implementation(libs.org.jetbrains.kotlin.kotlin.reflect)
    testImplementation(projects.test)
    testApi(libs.org.mockito.mockito.kotlin)
    testFixturesImplementation(projects.test)
}

description = "Implements a Devices Profile for Web-Services (DPWS) 1.1 framework."

checkstyle {
    // skip tests
    sourceSets = setOf(project.sourceSets.main.get())
}