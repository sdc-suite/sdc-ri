pluginManagement {
    // Include 'plugins build' to define convention plugins.
    includeBuild("build-logic")
}

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "sdc-ri"
include(":glue-examples")
include(":dpws")
include(":biceps")
include(":glue")
include(":common")
include(":test")
