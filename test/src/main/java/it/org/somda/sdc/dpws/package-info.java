@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package it.org.somda.sdc.dpws;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;