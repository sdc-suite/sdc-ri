@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package test.org.somda.common;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;