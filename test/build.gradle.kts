plugins {
    id("buildlogic.java-conventions")
    id("org.somda.sdc.xjc")
}

dependencies {
    api(libs.org.jvnet.jaxb.jaxb.plugins.runtime)
    api(libs.org.bouncycastle.bcprov.jdk18on)
    api(libs.org.bouncycastle.bcpkix.jdk18on)
    api(libs.jakarta.xml.bind.jakarta.xml.bind.api)
    api(libs.org.glassfish.jaxb.jaxb.core)
    api(libs.org.glassfish.jaxb.jaxb.runtime)
    api(libs.org.apache.logging.log4j.log4j.core)
    api(libs.org.junit.jupiter.junit.jupiter.api)
}

description = "test"

// generate data model used for unit test
val schemaFile: File = layout.projectDirectory.dir("src/main/resources/it/org/somda/sdc/dpws").file("TestServiceSchema.xsd").asFile

xjc {
    schemaLocation = listOf(schemaFile)
}