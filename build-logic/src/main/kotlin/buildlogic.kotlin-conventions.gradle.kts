import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    id("org.jetbrains.kotlin.jvm")
    id("io.gitlab.arturbosch.detekt")
    id("buildlogic.java-conventions")
    id("buildlogic.dokka")
}

kotlin {
    compilerOptions {
        jvmTarget = JvmTarget.JVM_17
        freeCompilerArgs.addAll(
            "-Xjvm-default=all",
            // enforce jspecify annotation meaning
            "-Xjspecify-annotations=strict",
            "-Xtype-enhancement-improvements-strict-mode",
            // make sure the jspecify annotations are available to guice
            "-Xemit-jvm-type-annotations",
        )
    }
}

dependencies {
    api(libs.org.jetbrains.kotlin.kotlin.stdlib)
    api(libs.org.apache.logging.log4j.log4j.api.kotlin)
}
