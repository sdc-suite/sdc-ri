
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.attributes.DocsType
import org.gradle.api.component.AdhocComponentWithVariants
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.plugins.internal.JavaConfigurationVariantMapping
import org.gradle.api.plugins.internal.JvmPluginsHelper
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.TaskProvider
import org.gradle.kotlin.dsl.getByName

// taken from https://github.com/TWiStErRob/net.twisterrob.gradle/blob/57d100cfbe87141614b9921caf8962c64307523c/gradle/plugins/src/main/kotlin/net/twisterrob/gradle/build/publishing/withDokkaJar.kt

/**
 * @param project [Project] this extension belongs to
 * @param artifactTask that produces the Documentation output files
 *
 * @see org.gradle.api.plugins.internal.DefaultJavaPluginExtension.withJavadocJar
 */
fun JavaPluginExtension.withDokkaJar(project: Project, artifactTask: TaskProvider<out Task>) {
    val kdocVariant = JvmPluginsHelper.createDocumentationVariantWithArtifact(
        JavaPlugin.JAVADOC_ELEMENTS_CONFIGURATION_NAME,
        null,
        DocsType.JAVADOC,
        emptySet(),
        this.sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).javadocJarTaskName,
        artifactTask,
        project as ProjectInternal,
    )
    val java = project.components.getByName<AdhocComponentWithVariants>("java")
    java.addVariantsFromConfiguration(kdocVariant, JavaConfigurationVariantMapping("runtime", true))
}