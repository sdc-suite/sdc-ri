import com.github.spotbugs.snom.Confidence

plugins {
    `java-library`
    `maven-publish`
    id("com.github.spotbugs")
    id("buildlogic.dokka")
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    api(libs.org.apache.logging.log4j.log4j.api)
    api(libs.com.google.guava.guava)
    implementation(libs.com.google.inject.guice)
    api(libs.com.google.inject.extensions.guice.assistedinject)
    api(libs.org.checkerframework.checker.qual)
    api(libs.jspecify)
    testImplementation(libs.org.apache.logging.log4j.log4j.jul)
    testImplementation(libs.org.apache.logging.log4j.log4j.slf4j2.impl)
    testImplementation(libs.org.junit.platform.junit.platform.engine)
    testImplementation(libs.org.junit.jupiter.junit.jupiter.api)
    testImplementation(libs.org.junit.jupiter.junit.jupiter.engine)
    testImplementation(libs.org.junit.jupiter.junit.jupiter.params)
    testImplementation(libs.org.mockito.mockito.core)
    testImplementation(libs.org.hamcrest.hamcrest)
}

group = "org.somda.sdc"
version = "6.2.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withDokkaJar(project, tasks.named("dokkaJavadoc"))
}

tasks.withType<Jar> {
    manifest {
        attributes["Implementation-Title"] = project.name
        attributes["Implementation-Version"] = project.version
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks.withType<Test>().configureEach {
    maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).coerceAtLeast(1)
}

spotbugs {
    excludeFilter = rootProject.file("spotbugs_exclude.xml")
    reportLevel = Confidence.HIGH
}

tasks.register("releaseCheck") {
    group = "publishing"
    description = "Checks if the project only depends on non-SNAPSHOT versions."
    project.configurations.forEach { configuration ->
        configuration.allDependencies.forEach {
            logger.debug("releaseCheck (${configuration.name}): Checking ${project.name}: ${it.name} in ${it.version}")
            // accepts dependencies without versions, which at least aren't SNAPSHOTs
            check(!(it.version?.contains("-SNAPSHOT") ?: false)) {
                "Dependencies with SNAPSHOT versions are not allowed." +
                    " Violating: ${it.name} in version ${it.version}, found in configuration ${configuration.name}"
            }
        }
    }
}
