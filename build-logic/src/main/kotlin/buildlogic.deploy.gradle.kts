plugins {
    `java-library`
    `maven-publish`
    signing
}

// we need to run this after the project has been evaluated
// otherwise the project properties (such as description)
// are not available yet and will be missing from the publication
afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("maven") {
                groupId = project.group.toString()
                artifactId = project.name
                version = project.version.toString()

                from(components["java"])
                pom {
                    name = project.name
                    url = "http://www.somda.org"
                    description = checkNotNull(project.description) {
                        "Project description not available for ${project.name}"
                    }

                    scm {
                        connection = "scm:git:git://gitlab.com/sdc-suite/sdc-ri.git"
                        developerConnection = "scm:git:git://gitlab.com/sdc-suite/sdc-ri.git"
                        url = "https://gitlab.com/sdc-suite/sdc-ri/-/tree/main"
                    }

                    developers {
                        developer {
                            name = "David Gregorczyk"
                            email = "david.gregorczyk@web.de"
                            organizationUrl = "https://gitlab.com/d.gregorczyk"
                        }

                        developer {
                            name = "Lukas Deichmann"
                            email = "lukasdeichmann@gmail.com"
                            organizationUrl = "https://gitlab.com/ldeichmann"
                        }

                        developer {
                            name = "Malte Grebe-Lüth"
                            email = "malte.grebe@gmx.de"
                            organizationUrl = "https://gitlab.com/grebem"
                        }
                    }

                    licenses {
                        license {
                            name = "MIT License"
                            url = "https://opensource.org/licenses/MIT"
                        }
                    }
                }
            }
        }

        repositories {
            maven {
                name = "SonatypeSnapshots"

                url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
                credentials {
                    username = System.getenv("MAVEN_REPO_USER")
                    password = System.getenv("MAVEN_REPO_PASS")
                }
            }

            maven {
                name = "SonatypeStaging"

                url = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2")
                credentials {
                    username = System.getenv("MAVEN_REPO_USER")
                    password = System.getenv("MAVEN_REPO_PASS")
                }
            }
        }
    }

    signing {
        // replace \n literal with newlines
        // for some reason, System.getenv only retrieves the first line of the environment variable when
        // it is set in gitlab with multiple lines
        val signingKey: String = System.getenv("GPG_PRIVATE_KEY_ESCAPED")?.replace("\\n", "\n") ?: ""
        val signingPassword: String = System.getenv("GPG_PRIVATE_KEY_PASSWORD") ?: ""

        useInMemoryPgpKeys(signingKey, signingPassword)
        sign(publishing.publications["maven"])
    }
}