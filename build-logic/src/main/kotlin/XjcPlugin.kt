import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.api.file.FileCollection
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.jvm.tasks.Jar
import java.io.File

interface CompileXjc {
    val jaxbClasspath: Property<FileCollection>
    val args: ListProperty<String>
    val bindingLocation: Property<Collection<File>>
    val schemaLocation: Property<Collection<File>>
}

const val EXTENSION_NAME = "xjc"
const val CREATE_DIRECTORY_TASK_NAME = "createXjcOutputDir"
const val MAIN_SOURCE_SET = "main"
const val XJC_MAIN_CLASS = "com.sun.tools.xjc.XJCFacade"
const val XJC_TASK_NAME = "xmlSchemaToJava"
const val CONFIG_NAME = "xjc"
const val EPISODE_FILE_NAME = "sun-jaxb.episode"

const val GENERATED_SOURCES_BASE = "generated/sources/xjc"
const val GENERATED_SOURCES_FOLDER_PATH = "$GENERATED_SOURCES_BASE/main"
const val GENERATED_SOURCES_EPISODE_PATH = "$GENERATED_SOURCES_BASE/$EPISODE_FILE_NAME"

class XjcPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create(
            EXTENSION_NAME,
            CompileXjc::class.java
        )

        // retrieve the version catalog from the toml, sadly not in a type safe manner here
        val libs = project
            .extensions
            .getByType(VersionCatalogsExtension::class.java)
            .named("libs")

        val xjcOutputDir = project.layout.buildDirectory.dir(GENERATED_SOURCES_FOLDER_PATH).get()
        val episodeOutputFile = project.layout.buildDirectory.file(GENERATED_SOURCES_EPISODE_PATH).get()

        val createXjcOutputDir = project.tasks.register(CREATE_DIRECTORY_TASK_NAME) {
            doLast {
                project.layout.buildDirectory.dir(GENERATED_SOURCES_FOLDER_PATH).get().asFile.mkdirs()
            }
        }

        val xjcTask = project.tasks.register(XJC_TASK_NAME, JavaExec::class.java) {
            // always add the base dependencies needed for our models
            val jaxb = project.configurations.create(CONFIG_NAME)
            project.dependencies.add(CONFIG_NAME, libs.findLibrary("org.glassfish.jaxb.jaxb.core").get().get())
            project.dependencies.add(CONFIG_NAME, libs.findLibrary("org.glassfish.jaxb.jaxb.runtime").get().get())
            project.dependencies.add(CONFIG_NAME, libs.findLibrary("org.glassfish.jaxb.jaxb.xjc").get().get())
            project.dependencies.add(CONFIG_NAME, libs.findLibrary("org.jvnet.jaxb.jaxb.plugins").get().get())

            // append additional user-specified arguments
            val newClassPath = jaxb + extension.jaxbClasspath.getOrElse(project.objects.fileCollection())

            dependsOn(createXjcOutputDir)
            classpath = newClassPath
            mainClass.set(XJC_MAIN_CLASS)

            project.logger.debug("episodeOutputFile = {}", episodeOutputFile)
            project.logger.debug("newClassPath = {}", newClassPath)
            project.logger.debug("currentJaxbClasspath = {}", extension.jaxbClasspath.orNull)
            project.logger.debug("args = {}", extension.args.orNull)
            project.logger.debug("schemaLocation = {}", extension.schemaLocation.orNull)

            // force generated javadoc to english
            jvmArgs = listOf("-Duser.language=en-US")

            val extensionArgs = extension.args.orElse(emptyList()).get()
            val bindingArgs =
                extension.bindingLocation.orNull?.map { listOf("-b", it.invariantSeparatorsPath) }?.flatten() ?: emptyList()
            val schemaLocationArgs =
                extension.schemaLocation.orNull?.map { it.invariantSeparatorsPath } ?: emptyList()

            val argsList = mutableListOf(
                "-d",
                xjcOutputDir.toString(),
                "-episode",
                episodeOutputFile.toString(),
                "-encoding",
                "UTF-8",
                "-quiet",
                "-extension",
                "-npa",
                "-no-header",
                "-Xsetters",
                "-Xsetters-mode=accessor",
                "-XsimpleEquals",
                "-XsimpleHashCode",
                "-XtoString",
                "-Xcopyable",
            ) + extensionArgs + bindingArgs + schemaLocationArgs

            project.logger.debug("Calling xjc with args: {}", argsList)

            args = argsList
        }

        // ensure xjc as run before any java compilation
        project.tasks.withType(JavaCompile::class.java) {
            dependsOn(xjcTask)
        }


        // claim the generated sources as ours
        val sourceSets: SourceSetContainer = project.extensions.getByType(SourceSetContainer::class.java)
        val mainSourceSet = sourceSets.findByName(MAIN_SOURCE_SET)!!

        // add episode file to jars
        project.tasks.withType(Jar::class.java) {
            metaInf {
                from(episodeOutputFile)
            }
        }

        // attach main source set to generated code
        mainSourceSet.java {
            srcDirs(
                project.files(xjcOutputDir) {
                    builtBy(xjcTask)
                }
            )
        }

    }
}
