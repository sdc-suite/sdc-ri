plugins {
    checkstyle
    id("buildlogic.java-conventions")
}

checkstyle {
    toolVersion = "10.15.0"
    configFile = rootProject.file("checkstyle.xml")
}