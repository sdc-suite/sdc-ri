plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    implementation(files(libs.javaClass.superclass.protectionDomain.codeSource.location))
    implementation(libs.gradleplugins.kotlin.jvm)
    implementation(libs.gradleplugins.detekt)
    implementation(libs.gradleplugins.spotbugs)
    implementation(libs.gradleplugins.dokka)
}

gradlePlugin {
    plugins {
        create("XjcPlugin") {
            id = "org.somda.sdc.xjc"
            implementationClass = "XjcPlugin"
        }
    }
}
