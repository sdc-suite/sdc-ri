@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package com.example.provider2_extension;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
