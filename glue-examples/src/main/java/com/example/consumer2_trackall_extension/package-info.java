@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package com.example.consumer2_trackall_extension;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
