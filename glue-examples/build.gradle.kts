plugins {
    id("buildlogic.java-conventions")
    id("org.somda.sdc.xjc")
    id("buildlogic.deploy")
}

dependencies {
    api(projects.glue)
    api(projects.dpws)
    api(projects.biceps)
    api(libs.jakarta.xml.bind.jakarta.xml.bind.api)
    api(libs.org.glassfish.jaxb.jaxb.core)
    api(libs.org.glassfish.jaxb.jaxb.runtime)
    api(libs.org.apache.logging.log4j.log4j.core)
    api(libs.org.apache.logging.log4j.log4j.slf4j.impl)
    api(libs.commons.cli.commons.cli)
    api(libs.org.junit.platform.junit.platform.launcher)
    api(libs.org.bouncycastle.bcprov.jdk18on)
    api(libs.org.bouncycastle.bcpkix.jdk18on)
}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming" +
    " with the IEEE 11073 SDC specifications. This project implements examples for using the glue package."

//
// generate data model used for example 2
//
val schemaFilePath: File = layout.projectDirectory.dir("src/main/resources/provider2_extension").asFile

// determine path to the jar, putting it as a schema location automatically reads the episode
val bicepsModelArtifactId = "biceps-model"
val bicepsModuleVersion: String = libs.versions.sdc.ri.model.get()
val bicepsModuleJarName = "$bicepsModelArtifactId-$bicepsModuleVersion.jar"

val resolvedLib: FileCollection = project.configurations.getByName("compileClasspath").filter {
    it.name.startsWith(bicepsModuleJarName)
}

// determine path to participant model and extension point in the jar
val extensionSchemaPath = File("jar:file:${resolvedLib.asPath}!/ExtensionPoint.xsd")
val participantSchemaPath = File("jar:file:${resolvedLib.asPath}!/BICEPS_ParticipantModel.xsd")

xjc {
    args = emptyList()
    bindingLocation = listOf(schemaFilePath)
    schemaLocation = listOf(extensionSchemaPath, participantSchemaPath, resolvedLib.singleFile, schemaFilePath)
}
