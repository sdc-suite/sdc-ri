plugins {
    id("buildlogic.java-conventions")
    id("buildlogic.kotlin-conventions")
    id("buildlogic.deploy")
}

dependencies {
    api(projects.dpws)
    api(projects.biceps)
    api(libs.net.sourceforge.jregex.jregex)
    api(libs.org.bouncycastle.bcpkix.jdk18on)
    implementation(libs.org.jetbrains.kotlin.kotlin.reflect)
    testImplementation(testFixtures(projects.dpws))
    testImplementation(testFixtures(projects.biceps))
    testImplementation(projects.test)
    testImplementation(libs.org.mockito.mockito.kotlin)
    testImplementation(libs.org.apache.logging.log4j.log4j.core.test)
}

description = "SDCri is a set of Java libraries that implements a network communication framework conforming" +
    " with the IEEE 11073 SDC specifications. This project implements the 11073-20702 SDC glue binding."
