@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package it.org.somda.glue;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
