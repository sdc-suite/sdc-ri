package org.somda.sdc.glue.provider.localization.helper

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.math.BigInteger

import org.junit.jupiter.api.Assertions.assertTrue

class HeapBasedLocalizationStorageTest {

    @Test
    @DisplayName("Regression test for #354 'NPE when getting localized texts from empty HeapBasedLocalizationStorage'")
    fun testGetlocalizedTextOnEmptyStorage() {
        // GIVEN an empty HeapBasedLocalizationStorage
        val storage = HeapBasedLocalizationStorage()

        // WHEN getting any localized text
        val result = storage.getLocalizedText(
            listOf("foo"),
            BigInteger.ONE,
            listOf("en-US")
        )

        // THEN no exception is thrown and an empty list is returned
        assertTrue(result.isEmpty())
    }
}
