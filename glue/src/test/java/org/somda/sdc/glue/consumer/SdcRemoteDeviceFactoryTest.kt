package org.somda.sdc.glue.consumer

import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.mockito.kotlin.mock
import org.somda.sdc.biceps.consumer.access.RemoteMdibAccess
import org.somda.sdc.dpws.service.HostingServiceProxy
import org.somda.sdc.glue.UnitTestUtil
import org.somda.sdc.glue.consumer.factory.SdcRemoteDeviceFactory
import org.somda.sdc.glue.consumer.report.ReportProcessor

class SdcRemoteDeviceFactoryTest {

    /**
     * Test that the factory can create a [SdcRemoteDevice] where all nullable fields are set to null.
     *
     * This is basically a test that the [KotlinSupportImpl][com.google.inject.KotlinSupportImpl] is working correctly.
     */
    @Test
    fun testCallWithNull() {
        val injector = UT.injector
        val factory = injector.getInstance(SdcRemoteDeviceFactory::class.java)

        assertDoesNotThrow {
            factory.createSdcRemoteDevice(
                mock<HostingServiceProxy>(),
                mock<RemoteMdibAccess>(),
                mock<ReportProcessor>(),
                null,
                mock<SdcRemoteDeviceWatchdog>(),
                null
            )
        }
    }

    companion object: Logging {
        val UT: UnitTestUtil = UnitTestUtil()
    }
}
