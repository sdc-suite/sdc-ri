package org.somda.sdc.glue.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.model.participant.ClockDescriptor;
import org.somda.sdc.biceps.model.participant.ClockState;
import org.somda.sdc.biceps.model.participant.MdDescription;
import org.somda.sdc.biceps.model.participant.Mdib;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.VmdDescriptor;
import org.somda.sdc.biceps.testutil.Handles;
import org.somda.sdc.biceps.testutil.MockModelFactory;
import org.somda.sdc.glue.UnitTestUtil;
import org.somda.sdc.glue.common.factory.ModificationsBuilderFactory;
import test.org.somda.common.LoggingTestWatcher;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(LoggingTestWatcher.class)
class ModificationsBuilderTest {
    private static final UnitTestUtil UT = new UnitTestUtil();

    @Test
    void createSingleStateIfNotExists() {
        var builderFactory = UT.getInjector().getInstance(ModificationsBuilderFactory.class);

        var mdib = new Mdib();
        var mdDescription = new MdDescription();
        mdib.setMdDescription(mdDescription);

        var mds = MockModelFactory.createDescriptor(Handles.MDS_0, MdsDescriptor.class);
        mdDescription.getMds().add(mds);

        var vmd = MockModelFactory.createDescriptor(Handles.VMD_0, VmdDescriptor.class);
        mds.getVmd().add(vmd);

        assertThrows(RuntimeException.class, () ->
                builderFactory.createModificationsBuilder((Mdib) mdib.clone(), false));

        assertDoesNotThrow(() -> builderFactory.createModificationsBuilder((Mdib) mdib.clone(), true));

        var modifications = builderFactory.createModificationsBuilder((Mdib) mdib.clone(), true).get();
        assertEquals(2, modifications.asList().size());

        var firstModification = Assertions.assertInstanceOf(MdibDescriptionModification.Insert.class, modifications.asList().get(0));
        var secondModification = Assertions.assertInstanceOf(MdibDescriptionModification.Insert.class, modifications.asList().get(1));

        assertEquals(1, firstModification.getPair().getStates().size());
        assertEquals(1, secondModification.getPair().getStates().size());
    }

    @Test
    void applyDefaultStates() {
        var builderFactory = UT.getInjector().getInstance(ModificationsBuilderFactory.class);

        var mdib = new Mdib();
        var mdDescription = new MdDescription();
        mdib.setMdDescription(mdDescription);
        var mds = MockModelFactory.createDescriptor(Handles.MDS_0, MdsDescriptor.class);
        mdDescription.getMds().add(mds);
        var clock = MockModelFactory.createDescriptor(Handles.CLOCK_0, ClockDescriptor.class);
        mds.setClock(clock);

        ModificationsBuilder modificationsBuilder = builderFactory.createModificationsBuilder(mdib, true);
        var modifications = modificationsBuilder.get();

        assertEquals(2, modifications.asList().size());
        var secondModification = Assertions.assertInstanceOf(MdibDescriptionModification.Insert.class, modifications.asList().get(1));


        assertEquals(1,  secondModification.getPair().getStates().size());
        var state = secondModification.getPair().getStates().get(0);
        assertTrue(state instanceof ClockState);
        assertFalse(((ClockState)state).isRemoteSync());
    }
}