package org.somda.sdc.glue.consumer.report.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.somda.sdc.biceps.common.MdibDescriptionModification;
import org.somda.sdc.biceps.common.MdibDescriptionModificationType;
import org.somda.sdc.biceps.common.MdibDescriptionModifications;
import org.somda.sdc.biceps.common.MdibStateModifications;
import org.somda.sdc.biceps.common.MdibTypeValidator;
import org.somda.sdc.biceps.common.Pair;
import org.somda.sdc.biceps.common.storage.PreprocessingException;
import org.somda.sdc.biceps.consumer.access.RemoteMdibAccess;
import org.somda.sdc.biceps.model.message.DescriptionModificationReport;
import org.somda.sdc.biceps.model.message.DescriptionModificationType;
import org.somda.sdc.biceps.model.message.EpisodicAlertReport;
import org.somda.sdc.biceps.model.message.EpisodicComponentReport;
import org.somda.sdc.biceps.model.message.EpisodicContextReport;
import org.somda.sdc.biceps.model.message.EpisodicMetricReport;
import org.somda.sdc.biceps.model.message.EpisodicOperationalStateReport;
import org.somda.sdc.biceps.model.message.ObjectFactory;
import org.somda.sdc.biceps.model.message.WaveformStream;
import org.somda.sdc.biceps.model.participant.AbstractAlertState;
import org.somda.sdc.biceps.model.participant.AbstractContextDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractContextState;
import org.somda.sdc.biceps.model.participant.AbstractDescriptor;
import org.somda.sdc.biceps.model.participant.AbstractDeviceComponentState;
import org.somda.sdc.biceps.model.participant.AbstractMetricState;
import org.somda.sdc.biceps.model.participant.AbstractMultiState;
import org.somda.sdc.biceps.model.participant.AbstractOperationState;
import org.somda.sdc.biceps.model.participant.AbstractState;
import org.somda.sdc.biceps.model.participant.ActivateOperationState;
import org.somda.sdc.biceps.model.participant.AlertConditionState;
import org.somda.sdc.biceps.model.participant.AlertSystemState;
import org.somda.sdc.biceps.model.participant.ChannelState;
import org.somda.sdc.biceps.model.participant.EnsembleContextDescriptor;
import org.somda.sdc.biceps.model.participant.EnsembleContextState;
import org.somda.sdc.biceps.model.participant.EnumStringMetricDescriptor;
import org.somda.sdc.biceps.model.participant.EnumStringMetricState;
import org.somda.sdc.biceps.model.participant.LimitAlertConditionState;
import org.somda.sdc.biceps.model.participant.LocationContextDescriptor;
import org.somda.sdc.biceps.model.participant.LocationContextState;
import org.somda.sdc.biceps.model.participant.MdibVersion;
import org.somda.sdc.biceps.model.participant.MdsDescriptor;
import org.somda.sdc.biceps.model.participant.MdsState;
import org.somda.sdc.biceps.model.participant.NumericMetricDescriptor;
import org.somda.sdc.biceps.model.participant.NumericMetricState;
import org.somda.sdc.biceps.model.participant.PatientContextDescriptor;
import org.somda.sdc.biceps.model.participant.PatientContextState;
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricDescriptor;
import org.somda.sdc.biceps.model.participant.RealTimeSampleArrayMetricState;
import org.somda.sdc.biceps.model.participant.SetContextStateOperationState;
import org.somda.sdc.biceps.model.participant.SetStringOperationState;
import org.somda.sdc.biceps.model.participant.StringMetricDescriptor;
import org.somda.sdc.biceps.model.participant.VmdState;
import org.somda.sdc.biceps.testutil.Handles;
import org.somda.sdc.biceps.testutil.MockEntryFactory;
import org.somda.sdc.glue.UnitTestUtil;
import org.somda.sdc.glue.common.MdibVersionUtil;
import org.somda.sdc.glue.consumer.report.ReportProcessingException;
import test.org.somda.common.LoggingTestWatcher;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(LoggingTestWatcher.class)
class ReportWriterTest {
    private static final UnitTestUtil UT = new UnitTestUtil();
    private ReportWriter reportWriter;
    private RemoteMdibAccess mdibAccess;

    private ArgumentCaptor<MdibStateModifications> stateCaptor;
    private ArgumentCaptor<MdibDescriptionModifications> descriptionCaptor;
    private ObjectFactory messageFactory;
    private MockEntryFactory mockEntryFactory;
    private ArgumentCaptor<MdibVersion> mdibVersionCaptor;
    private MdibVersionUtil mdibVersionUtil;
    private ArgumentCaptor<BigInteger> nullCaptor;
    private MdibTypeValidator typeValidator;

    @BeforeEach
    void beforeEach() {
        reportWriter = UT.getInjector().getInstance(ReportWriter.class);
        messageFactory = new ObjectFactory();
        mdibAccess = mock(RemoteMdibAccess.class);
        stateCaptor = ArgumentCaptor.forClass(MdibStateModifications.class);
        mdibVersionCaptor = ArgumentCaptor.forClass(MdibVersion.class);
        descriptionCaptor = ArgumentCaptor.forClass(MdibDescriptionModifications.class);
        nullCaptor = ArgumentCaptor.forClass(BigInteger.class);
        mockEntryFactory = new MockEntryFactory(UT.getInjector().getInstance(MdibTypeValidator.class));
        mdibVersionUtil = UT.getInjector().getInstance(MdibVersionUtil.class);
        typeValidator = UT.getInjector().getInstance(MdibTypeValidator.class);
    }

    @Test
    void writeDescription() throws Exception {
        final DescriptionModificationReport report = messageFactory.createDescriptionModificationReport();
        final DescriptionModificationReport.ReportPart reportPartInsert = messageFactory.createDescriptionModificationReportReportPart();
        final DescriptionModificationReport.ReportPart reportPartUpdate = messageFactory.createDescriptionModificationReportReportPart();
        final DescriptionModificationReport.ReportPart reportPartDelete = messageFactory.createDescriptionModificationReportReportPart();

        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);

        reportPartInsert.setModificationType(DescriptionModificationType.CRT);
        addEntry(reportPartInsert, Handles.MDS_0, MdsDescriptor.class, false);
        addEntry(reportPartInsert, Handles.MDS_1, MdsDescriptor.class, false);
        addEntry(reportPartInsert, Handles.MDS_2, MdsDescriptor.class, false);

        final String expectedUpdateParent = "update-parent";
        reportPartUpdate.setParentDescriptor(expectedUpdateParent);
        addContextEntry(reportPartUpdate, Handles.CONTEXTDESCRIPTOR_0, Arrays.asList(Handles.CONTEXT_0, Handles.CONTEXT_1), LocationContextDescriptor.class);
        addContextEntry(reportPartUpdate, Handles.CONTEXTDESCRIPTOR_1, Arrays.asList(Handles.CONTEXT_2, Handles.CONTEXT_3), PatientContextDescriptor.class);
        addContextEntry(reportPartUpdate, Handles.CONTEXTDESCRIPTOR_2, Arrays.asList(Handles.CONTEXT_4, Handles.CONTEXT_5), EnsembleContextDescriptor.class);

        final String expectedDeleteParent = "delete-parent";
        reportPartDelete.setModificationType(DescriptionModificationType.DEL);
        reportPartDelete.setParentDescriptor(expectedDeleteParent);
        addEntryDescriptor(reportPartDelete, Handles.METRIC_0, NumericMetricDescriptor.class);
        addEntryDescriptor(reportPartDelete, Handles.METRIC_1, StringMetricDescriptor.class);
        addEntryDescriptor(reportPartDelete, Handles.METRIC_2, EnumStringMetricDescriptor.class);
        addEntryDescriptor(reportPartDelete, Handles.METRIC_3, RealTimeSampleArrayMetricDescriptor.class);

        report.getReportPart().addAll(Arrays.asList(reportPartInsert, reportPartUpdate, reportPartDelete));

        reportWriter.write(EpisodicReport.from(report), mdibAccess);

        verify(mdibAccess).writeDescription(mdibVersionCaptor.capture(), nullCaptor.capture(), nullCaptor.capture(), descriptionCaptor.capture());

        assertEquals(expectedMdibVersion, mdibVersionCaptor.getValue());
        nullCaptor.getAllValues().forEach(Assertions::assertNull);

        final MdibDescriptionModifications modifications = descriptionCaptor.getValue();
        final List<MdibDescriptionModification> modificationsList = modifications.asList();
        assertEquals(10, modificationsList.size());

        testSingleState(modificationsList.get(0), Handles.MDS_0, MdibDescriptionModificationType.INSERT, Optional.empty());
        testSingleState(modificationsList.get(1), Handles.MDS_1, MdibDescriptionModificationType.INSERT, Optional.empty());
        testSingleState(modificationsList.get(2), Handles.MDS_2, MdibDescriptionModificationType.INSERT, Optional.empty());

        testMultiStateUpdate(modificationsList.get(3), Handles.CONTEXTDESCRIPTOR_0, Arrays.asList(Handles.CONTEXT_0, Handles.CONTEXT_1));
        testMultiStateUpdate(modificationsList.get(4), Handles.CONTEXTDESCRIPTOR_1, Arrays.asList(Handles.CONTEXT_2, Handles.CONTEXT_3));
        testMultiStateUpdate(modificationsList.get(5), Handles.CONTEXTDESCRIPTOR_2, Arrays.asList(Handles.CONTEXT_4, Handles.CONTEXT_5));

        testSingleState(modificationsList.get(6), Handles.METRIC_0, MdibDescriptionModificationType.DELETE, Optional.empty());
        testSingleState(modificationsList.get(7), Handles.METRIC_1, MdibDescriptionModificationType.DELETE, Optional.empty());
        testSingleState(modificationsList.get(8), Handles.METRIC_2, MdibDescriptionModificationType.DELETE, Optional.empty());
        testSingleState(modificationsList.get(9), Handles.METRIC_3, MdibDescriptionModificationType.DELETE, Optional.empty());
    }

    @Test
    void writeDescriptionStateWithoutDescriptor() throws Exception {
        final DescriptionModificationReport report = messageFactory.createDescriptionModificationReport();
        final DescriptionModificationReport.ReportPart reportPartInsert = messageFactory.createDescriptionModificationReportReportPart();

        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);

        reportPartInsert.setModificationType(DescriptionModificationType.CRT);
        addEntry(reportPartInsert, Handles.MDS_0, MdsDescriptor.class, false);
        addEntry(reportPartInsert, Handles.MDS_1, MdsDescriptor.class, true);
        addEntry(reportPartInsert, Handles.MDS_2, MdsDescriptor.class, false);

        report.getReportPart().add(reportPartInsert);

        assertThrows(ReportProcessingException.class, () -> reportWriter.write(EpisodicReport.from(report), mdibAccess));
    }

    private void testSingleState(MdibDescriptionModification modification, String handle, MdibDescriptionModificationType type, Optional<String> parentHandle) {
        assertEquals(handle, modification.getDescriptorHandle());

        if (type == MdibDescriptionModificationType.DELETE) {
            var typed = Assertions.assertInstanceOf(MdibDescriptionModification.Delete.class, modification);
            testSingleState(typed, handle);
        } else if (type == MdibDescriptionModificationType.INSERT) {
            assertEquals(parentHandle.orElse(null), modification.getParent());
            var typed = Assertions.assertInstanceOf(MdibDescriptionModification.Insert.class, modification);
            testSingleState(typed, handle);
        } else {
            testSingleState(modification);
        }
    }

    private void testSingleState(MdibDescriptionModification.Delete modification, String handle) {
        assertNotNull(modification.getHandle());
        assertEquals(handle, modification.getHandle());
    }

    private void testSingleState(MdibDescriptionModification.Insert modification, String handle) {
        Assertions.assertInstanceOf(Pair.SingleStatePair.class, modification.getPair());
        assertEquals(handle, modification.getPair().getStates().get(0).getDescriptorHandle());
    }

    private void testSingleState(MdibDescriptionModification modification) {
        Assertions.fail("No update expected");
    }

    private void testMultiStateUpdate(MdibDescriptionModification modification, String handle, List<String> stateHandles) {
        var typed = Assertions.assertInstanceOf(MdibDescriptionModification.Update.class, modification);

        assertEquals(handle, typed.getPair().getDescriptor().getHandle());
        assertEquals(stateHandles.size(), typed.getPair().getStates().size());

        for (int i = 0; i < stateHandles.size(); ++i) {
            assertEquals(handle, typed.getPair().getStates().get(i).getDescriptorHandle());
            assertTrue(typed.getPair().getStates().get(i) instanceof AbstractMultiState);
            assertEquals(stateHandles.get(i), ((AbstractMultiState) typed.getPair().getStates().get(i)).getHandle());
        }
    }

    private void addEntryDescriptor(DescriptionModificationReport.ReportPart reportPart, String handle, Class<? extends AbstractDescriptor> type) throws Exception {
        final var pair = mockEntryFactory.entry(handle, type, typeValidator.resolveStateType(type));
        reportPart.getDescriptor().add(pair.getDescriptor());
    }

    private void addEntry(DescriptionModificationReport.ReportPart reportPart, String handle, Class<? extends AbstractDescriptor> type, boolean addStateWithoutDescriptor) throws Exception {
        final var pair = mockEntryFactory.entry(handle, type, typeValidator.resolveStateType(type));
        if (!addStateWithoutDescriptor) {
            reportPart.getDescriptor().add(pair.getDescriptor());
        }
        reportPart.getState().addAll(pair.getStates());
    }

    private void addContextEntry(DescriptionModificationReport.ReportPart reportPart, String handle, List<String> stateHandles, Class<? extends AbstractContextDescriptor> type) throws Exception {
        final var entry = mockEntryFactory.contextEntry(handle, stateHandles, type, typeValidator.resolveStateType(type));
        reportPart.getDescriptor().add(entry.getDescriptor());
        reportPart.getState().addAll(entry.getStates());
    }

    @Test
    void writeComponent() throws Exception {
        final EpisodicComponentReport report = messageFactory.createEpisodicComponentReport();
        final EpisodicComponentReport.ReportPart reportPart = messageFactory.createAbstractComponentReportReportPart();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);
        report.getReportPart().add(reportPart);

        final List<AbstractDeviceComponentState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", MdsState.class),
                mockEntryFactory.state("2", VmdState.class),
                mockEntryFactory.state("3", ChannelState.class));

        reportPart.getComponentState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Component.class);
    }

    @Test
    void writeAlert() throws Exception {
        final EpisodicAlertReport report = messageFactory.createEpisodicAlertReport();
        final EpisodicAlertReport.ReportPart reportPart = messageFactory.createAbstractAlertReportReportPart();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);
        report.getReportPart().add(reportPart);

        final List<AbstractAlertState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", AlertSystemState.class),
                mockEntryFactory.state("2", AlertConditionState.class),
                mockEntryFactory.state("3", LimitAlertConditionState.class));

        reportPart.getAlertState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Alert.class);
    }

    @Test
    void writeMetric() throws Exception {
        final EpisodicMetricReport report = messageFactory.createEpisodicMetricReport();
        final EpisodicMetricReport.ReportPart reportPart = messageFactory.createAbstractMetricReportReportPart();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);
        report.getReportPart().add(reportPart);

        final List<AbstractMetricState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", NumericMetricState.class),
                mockEntryFactory.state("2", EnumStringMetricState.class),
                mockEntryFactory.state("3", RealTimeSampleArrayMetricState.class));

        reportPart.getMetricState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Metric.class);
    }

    @Test
    void writeOperation() throws Exception {
        final EpisodicOperationalStateReport report = messageFactory.createEpisodicOperationalStateReport();
        final EpisodicOperationalStateReport.ReportPart reportPart = messageFactory.createAbstractOperationalStateReportReportPart();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);
        report.getReportPart().add(reportPart);

        final List<AbstractOperationState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", ActivateOperationState.class),
                mockEntryFactory.state("2", SetStringOperationState.class),
                mockEntryFactory.state("3", SetContextStateOperationState.class));

        reportPart.getOperationState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Operation.class);
    }

    @Test
    void writeContext() throws Exception {
        final EpisodicContextReport report = messageFactory.createEpisodicContextReport();
        final EpisodicContextReport.ReportPart reportPart = messageFactory.createAbstractContextReportReportPart();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);
        report.getReportPart().add(reportPart);

        final List<AbstractContextState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", LocationContextState.class),
                mockEntryFactory.state("2", PatientContextState.class),
                mockEntryFactory.state("3", EnsembleContextState.class));

        reportPart.getContextState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Context.class);
    }

    @Test
    void writeWaveform() throws Exception {
        final WaveformStream report = messageFactory.createWaveformStream();
        final MdibVersion expectedMdibVersion = MdibVersion.create();
        mdibVersionUtil.setMdibVersion(expectedMdibVersion, report);

        final List<RealTimeSampleArrayMetricState> expectedStates = Arrays.asList(
                mockEntryFactory.state("1", RealTimeSampleArrayMetricState.class),
                mockEntryFactory.state("2", RealTimeSampleArrayMetricState.class),
                mockEntryFactory.state("3", RealTimeSampleArrayMetricState.class));

        report.getState().addAll(expectedStates);

        reportWriter.write(EpisodicReport.from(report), mdibAccess);
        testStateModifications(expectedMdibVersion, expectedStates, MdibStateModifications.Waveform.class);
    }

    void testStateModifications(MdibVersion expectedMdibVersion, List<? extends AbstractState> expectedStates, Class<? extends MdibStateModifications> expectedModificationClass) throws PreprocessingException {
        verify(mdibAccess).writeStates(mdibVersionCaptor.capture(), stateCaptor.capture());

        assertEquals(expectedMdibVersion, mdibVersionCaptor.getValue());

        final var modifications = stateCaptor.getValue();

        Assertions.assertInstanceOf(expectedModificationClass, modifications);
        assertEquals(expectedStates.size(), modifications.getStates().size());
        for (int i = 0; i < expectedStates.size(); ++i) {
            assertEquals(expectedStates.get(i).getDescriptorHandle(), modifications.getStates().get(i).getDescriptorHandle());
        }
    }
}