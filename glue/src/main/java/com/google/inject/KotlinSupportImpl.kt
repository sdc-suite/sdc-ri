package com.google.inject

import com.google.inject.internal.Errors
import com.google.inject.internal.KotlinSupportInterface
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.Method
import java.util.function.Predicate
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.jvmErasure

/**
 * A Kotlin support implementation for Guice.
 */
class KotlinSupportImpl : KotlinSupportInterface {
    override fun getAnnotations(field: Field?): Array<Annotation> {
        return field?.let { f ->
            f.declaringClass.kotlin.declaredMemberProperties.forEach {
                if (it.name == field.name) {
                    return@let it.annotations.toTypedArray()
                }
            }
            emptyArray()
        } ?: emptyArray()
    }

    override fun isNullable(field: Field?): Boolean {
        return field?.let { f ->
            f.declaringClass.kotlin.declaredMemberProperties.forEach {
                if (it.name == field.name) {
                    return@let it.returnType.isMarkedNullable
                }
            }
            false
        } ?: false
    }

    override fun getIsParameterKotlinNullablePredicate(constructor: Constructor<*>?): Predicate<Int> {
        return constructor?.let { c ->
            c.declaringClass.kotlin.constructors.forEach {
                val isNullable = it.parameters.zip(constructor.parameters)
                    .all { (kotlinParam, javaParam) -> kotlinParam.type.jvmErasure.java == javaParam.type }
                if (isNullable) {
                    return@let Predicate { index -> it.parameters[index].type.isMarkedNullable }
                }
            }
            Predicate { false }
        } ?: Predicate { false }
    }

    override fun getIsParameterKotlinNullablePredicate(method: Method?): Predicate<Int> {
        return method?.let { m ->
            m.declaringClass.kotlin.declaredFunctions.forEach {
                val isNullable = it.name == method.name &&
                    it.parameters.zip(method.parameters)
                        .all { (kotlinParam, javaParam) -> kotlinParam.type.jvmErasure.java == javaParam.type }
                if (isNullable) {
                    return@let Predicate { _ -> it.returnType.isMarkedNullable }
                }
            }
            Predicate { false }
        } ?: Predicate { false }
    }

    override fun checkConstructorParameterAnnotations(constructor: Constructor<*>?, errors: Errors?) {
        // we currently don't know of any errors we want to handle
        return
    }

    override fun isLocalClass(clazz: Class<*>?): Boolean {
        return clazz != null && clazz.kotlin.isInner
    }

    companion object {
        // this is used by guice to load the class via reflection
        // not cool, but necessary
        @Suppress("unused")
        @JvmField
        val INSTANCE = KotlinSupportImpl()
    }
}
