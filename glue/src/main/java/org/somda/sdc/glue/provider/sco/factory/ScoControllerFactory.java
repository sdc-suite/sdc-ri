package org.somda.sdc.glue.provider.sco.factory;

import com.google.inject.assistedinject.Assisted;
import org.jspecify.annotations.Nullable;
import org.somda.sdc.biceps.provider.access.LocalMdibAccess;
import org.somda.sdc.dpws.device.EventSourceAccess;
import org.somda.sdc.glue.provider.sco.OperationInvocationReceiver;
import org.somda.sdc.glue.provider.sco.ScoController;


/**
 * Factory to create {@linkplain ScoControllerFactory} instances.
 */
public interface ScoControllerFactory {
    /**
     * Creates a new {@linkplain ScoController} instance.
     *
     * @param eventSourceAccess             the event source access to send report notifications.
     * @param mdibAccess                    required to fetch MDIB versions and pass to invocation contexts.
     * @param operationInvocationReceiver   to process incoming set service requests.
     * @return the new instance.
     */
    ScoController createScoController(@Assisted EventSourceAccess eventSourceAccess,
                                      @Assisted LocalMdibAccess mdibAccess,
                                      @Assisted @Nullable OperationInvocationReceiver operationInvocationReceiver);
}
