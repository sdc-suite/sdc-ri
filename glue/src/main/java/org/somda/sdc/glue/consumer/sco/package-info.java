/**
 * Implementation of the SDC provider service control object (SCO).
 */
@DefaultQualifier(value = NonNull.class, locations = TypeUseLocation.PARAMETER)
package org.somda.sdc.glue.consumer.sco;

import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
import org.jspecify.annotations.NonNull;
