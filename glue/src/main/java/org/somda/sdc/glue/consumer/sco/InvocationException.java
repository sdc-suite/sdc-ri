package org.somda.sdc.glue.consumer.sco;

import org.jspecify.annotations.Nullable;

/**
 * Error that can occur during SCO processing.
 */
public class InvocationException extends Exception {
    public InvocationException() {
    }

    public InvocationException(@Nullable String message) {
        super(message);
    }

    public InvocationException(@Nullable String message, Throwable cause) {
        super(message, cause);
    }

    public InvocationException(Throwable cause) {
        super(cause);
    }

    public InvocationException(@Nullable String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
